return 
{
  anchor = {
    x = 17,
    y = 14
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "underground-belt",
      position = {
        x = -23,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -20,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "underground-belt",
      position = {
        x = -21,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 4,
      name = "underground-belt",
      position = {
        x = -19,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 5,
      name = "underground-belt",
      position = {
        x = -15,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 6,
      name = "underground-belt",
      position = {
        x = -14,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 7,
      name = "underground-belt",
      position = {
        x = -12,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -11,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "underground-belt",
      position = {
        x = -10,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 10,
      name = "underground-belt",
      position = {
        x = -6,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 11,
      name = "underground-belt",
      position = {
        x = -5,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -2,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "underground-belt",
      position = {
        x = -3,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 14,
      name = "underground-belt",
      position = {
        x = -1,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 15,
      name = "underground-belt",
      position = {
        x = 3,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 16,
      name = "underground-belt",
      position = {
        x = 4,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 17,
      name = "underground-belt",
      position = {
        x = 6,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 7,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "underground-belt",
      position = {
        x = 8,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 20,
      name = "underground-belt",
      position = {
        x = 12,
        y = -14
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 13,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 14,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 14,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 14,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 14,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 14,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 14,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 14,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 14,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 14,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 14,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 14,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 14,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 14,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 14,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 14,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 14,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 14,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 14,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 14,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 14,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 14,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 14,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 14,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 14,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 14,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 14,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 14,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 14,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 50,
      name = "underground-belt",
      position = {
        x = 15,
        y = 13
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 52,
      name = "underground-belt",
      position = {
        x = 19,
        y = 13
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 53,
      name = "splitter",
      position = {
        x = 20,
        y = 13.5
      }
    },
    {
      entity_number = 54,
      name = "splitter",
      position = {
        x = 22.5,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 21,
        y = 14
      }
    },
    {
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 22,
        y = 14
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "underground-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_iron_coal-1-furnace_connection"
}