return {
  anchor = {
    x = -1,
    y = 7
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -8
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "inserter",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 4
      }
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      entity_number = 7,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 4.5
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "inserter",
      position = {
        x = 1,
        y = 6
      }
    },
    {
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      entity_number = 12,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 6.5
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "inserter",
      position = {
        x = 1,
        y = 9
      }
    },
    {
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      entity_number = 17,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 8.5
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "inserter",
      position = {
        x = 1,
        y = 10
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 11
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = 11
      }
    },
    {
      entity_number = 23,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 10.5
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 0,
        y = 12
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap-1"
}