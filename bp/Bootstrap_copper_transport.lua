return 
{
  anchor = {
    x = 2,
    y = -39
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 3,
        y = -44
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 4,
        y = -44
      }
    },
    {
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 3,
        y = -43
      }
    },
    {
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 3,
        y = -42
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 4,
        y = -43
      }
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 4,
        y = -42
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 3,
        y = -41
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 3,
        y = -40
      }
    },
    {
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 4,
        y = -41
      }
    },
    {
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 4,
        y = -40
      }
    },
    {
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 3,
        y = -39
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 3,
        y = -38
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 4,
        y = -39
      }
    },
    {
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 4,
        y = -38
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 3,
        y = -37
      }
    },
    {
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 3,
        y = -36
      }
    },
    {
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 4,
        y = -37
      }
    },
    {
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 4,
        y = -36
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 3,
        y = -35
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 3,
        y = -34
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 4,
        y = -35
      }
    },
    {
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 4,
        y = -34
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 3,
        y = -33
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 3,
        y = -32
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 4,
        y = -33
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 4,
        y = -32
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 3,
        y = -31
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 3,
        y = -30
      }
    },
    {
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 4,
        y = -31
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 4,
        y = -30
      }
    },
    {
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 3,
        y = -29
      }
    },
    {
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 3,
        y = -28
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 4,
        y = -29
      }
    },
    {
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 4,
        y = -28
      }
    },
    {
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 3,
        y = -27
      }
    },
    {
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 3,
        y = -26
      }
    },
    {
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 4,
        y = -27
      }
    },
    {
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 4,
        y = -26
      }
    },
    {
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 3,
        y = -25
      }
    },
    {
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 3,
        y = -24
      }
    },
    {
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 4,
        y = -25
      }
    },
    {
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 4,
        y = -24
      }
    },
    {
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 3,
        y = -23
      }
    },
    {
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 3,
        y = -22
      }
    },
    {
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 4,
        y = -23
      }
    },
    {
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 4,
        y = -22
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 3,
        y = -21
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 3,
        y = -20
      }
    },
    {
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 4,
        y = -21
      }
    },
    {
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 4,
        y = -20
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 3,
        y = -19
      }
    },
    {
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 3,
        y = -18
      }
    },
    {
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 4,
        y = -18
      }
    },
    {
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 3,
        y = -17
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 3,
        y = -16
      }
    },
    {
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 4,
        y = -17
      }
    },
    {
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 4,
        y = -16
      }
    },
    {
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = 3,
        y = -15
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 3,
        y = -14
      }
    },
    {
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 4,
        y = -15
      }
    },
    {
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = 3,
        y = -13
      }
    },
    {
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 3,
        y = -12
      }
    },
    {
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 4,
        y = -13
      }
    },
    {
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 4,
        y = -12
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 3,
        y = -11
      }
    },
    {
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 3,
        y = -10
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 4,
        y = -11
      }
    },
    {
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 4,
        y = -10
      }
    },
    {
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 3,
        y = -9
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 3,
        y = -8
      }
    },
    {
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 4,
        y = -9
      }
    },
    {
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 3,
        y = -7
      }
    },
    {
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 3,
        y = -6
      }
    },
    {
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 4,
        y = -7
      }
    },
    {
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 3,
        y = -5
      }
    },
    {
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 3,
        y = -4
      }
    },
    {
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 4,
        y = -5
      }
    },
    {
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 4,
        y = -4
      }
    },
    {
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 3,
        y = -3
      }
    },
    {
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = 3,
        y = -2
      }
    },
    {
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = 4,
        y = -3
      }
    },
    {
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = 4,
        y = -2
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 3,
        y = 1
      }
    },
    {
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 3,
        y = 2
      }
    },
    {
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 4,
        y = 1
      }
    },
    {
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      entity_number = 97,
      name = "transport-belt",
      position = {
        x = 3,
        y = 4
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 4,
        y = 3
      }
    },
    {
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = 3,
        y = 5
      }
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = 3,
        y = 6
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      entity_number = 104,
      name = "transport-belt",
      position = {
        x = 3,
        y = 7
      }
    },
    {
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = 3,
        y = 8
      }
    },
    {
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = 4,
        y = 7
      }
    },
    {
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = 3,
        y = 9
      }
    },
    {
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = 3,
        y = 10
      }
    },
    {
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = 4,
        y = 10
      }
    },
    {
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 3,
        y = 11
      }
    },
    {
      entity_number = 113,
      name = "transport-belt",
      position = {
        x = 3,
        y = 12
      }
    },
    {
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 4,
        y = 12
      }
    },
    {
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 3,
        y = 13
      }
    },
    {
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = 3,
        y = 14
      }
    },
    {
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      entity_number = 120,
      name = "transport-belt",
      position = {
        x = 3,
        y = 15
      }
    },
    {
      entity_number = 121,
      name = "transport-belt",
      position = {
        x = 3,
        y = 16
      }
    },
    {
      entity_number = 122,
      name = "transport-belt",
      position = {
        x = 4,
        y = 15
      }
    },
    {
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 4,
        y = 16
      }
    },
    {
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 3,
        y = 17
      }
    },
    {
      entity_number = 125,
      name = "transport-belt",
      position = {
        x = 3,
        y = 18
      }
    },
    {
      entity_number = 126,
      name = "transport-belt",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      entity_number = 127,
      name = "transport-belt",
      position = {
        x = 4,
        y = 18
      }
    },
    {
      entity_number = 128,
      name = "transport-belt",
      position = {
        x = 3,
        y = 19
      }
    },
    {
      entity_number = 129,
      name = "transport-belt",
      position = {
        x = 3,
        y = 20
      }
    },
    {
      entity_number = 130,
      name = "transport-belt",
      position = {
        x = 4,
        y = 19
      }
    },
    {
      entity_number = 131,
      name = "transport-belt",
      position = {
        x = 4,
        y = 20
      }
    },
    {
      entity_number = 132,
      name = "transport-belt",
      position = {
        x = 3,
        y = 21
      }
    },
    {
      entity_number = 133,
      name = "transport-belt",
      position = {
        x = 3,
        y = 22
      }
    },
    {
      entity_number = 134,
      name = "transport-belt",
      position = {
        x = 4,
        y = 21
      }
    },
    {
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = 4,
        y = 22
      }
    },
    {
      entity_number = 136,
      name = "transport-belt",
      position = {
        x = 3,
        y = 23
      }
    },
    {
      entity_number = 137,
      name = "transport-belt",
      position = {
        x = 3,
        y = 24
      }
    },
    {
      entity_number = 138,
      name = "transport-belt",
      position = {
        x = 4,
        y = 23
      }
    },
    {
      entity_number = 139,
      name = "transport-belt",
      position = {
        x = 4,
        y = 24
      }
    },
    {
      entity_number = 140,
      name = "transport-belt",
      position = {
        x = 3,
        y = 25
      }
    },
    {
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = 3,
        y = 26
      }
    },
    {
      entity_number = 142,
      name = "transport-belt",
      position = {
        x = 4,
        y = 25
      }
    },
    {
      entity_number = 143,
      name = "transport-belt",
      position = {
        x = 4,
        y = 26
      }
    },
    {
      entity_number = 144,
      name = "transport-belt",
      position = {
        x = 3,
        y = 27
      }
    },
    {
      entity_number = 145,
      name = "transport-belt",
      position = {
        x = 3,
        y = 28
      }
    },
    {
      entity_number = 146,
      name = "transport-belt",
      position = {
        x = 4,
        y = 27
      }
    },
    {
      entity_number = 147,
      name = "transport-belt",
      position = {
        x = 4,
        y = 28
      }
    },
    {
      direction = 2,
      entity_number = 148,
      name = "transport-belt",
      position = {
        x = 0,
        y = 30
      }
    },
    {
      direction = 2,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = 1,
        y = 30
      }
    },
    {
      entity_number = 150,
      name = "transport-belt",
      position = {
        x = 3,
        y = 29
      }
    },
    {
      direction = 2,
      entity_number = 151,
      name = "transport-belt",
      position = {
        x = 2,
        y = 30
      }
    },
    {
      entity_number = 152,
      name = "transport-belt",
      position = {
        x = 3,
        y = 30
      }
    },
    {
      entity_number = 153,
      name = "transport-belt",
      position = {
        x = 4,
        y = 29
      }
    },
    {
      entity_number = 154,
      name = "transport-belt",
      position = {
        x = 4,
        y = 30
      }
    },
    {
      entity_number = 155,
      name = "transport-belt",
      position = {
        x = 0,
        y = 31
      }
    },
    {
      direction = 2,
      entity_number = 156,
      name = "transport-belt",
      position = {
        x = 1,
        y = 31
      }
    },
    {
      entity_number = 157,
      name = "transport-belt",
      position = {
        x = 0,
        y = 32
      }
    },
    {
      entity_number = 158,
      name = "transport-belt",
      position = {
        x = 1,
        y = 32
      }
    },
    {
      direction = 2,
      entity_number = 159,
      name = "transport-belt",
      position = {
        x = 2,
        y = 31
      }
    },
    {
      direction = 2,
      entity_number = 160,
      name = "transport-belt",
      position = {
        x = 3,
        y = 31
      }
    },
    {
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = 4,
        y = 31
      }
    },
    {
      entity_number = 162,
      name = "transport-belt",
      position = {
        x = 0,
        y = 33
      }
    },
    {
      entity_number = 163,
      name = "transport-belt",
      position = {
        x = 1,
        y = 33
      }
    },
    {
      entity_number = 164,
      name = "transport-belt",
      position = {
        x = 0,
        y = 34
      }
    },
    {
      entity_number = 165,
      name = "transport-belt",
      position = {
        x = 1,
        y = 34
      }
    },
    {
      entity_number = 166,
      name = "transport-belt",
      position = {
        x = 0,
        y = 35
      }
    },
    {
      entity_number = 167,
      name = "transport-belt",
      position = {
        x = 1,
        y = 35
      }
    },
    {
      entity_number = 168,
      name = "transport-belt",
      position = {
        x = 0,
        y = 36
      }
    },
    {
      entity_number = 169,
      name = "transport-belt",
      position = {
        x = 1,
        y = 36
      }
    },
    {
      entity_number = 170,
      name = "transport-belt",
      position = {
        x = 0,
        y = 37
      }
    },
    {
      entity_number = 171,
      name = "transport-belt",
      position = {
        x = 1,
        y = 37
      }
    },
    {
      entity_number = 172,
      name = "transport-belt",
      position = {
        x = 0,
        y = 38
      }
    },
    {
      entity_number = 173,
      name = "transport-belt",
      position = {
        x = 1,
        y = 38
      }
    },
    {
      entity_number = 174,
      name = "transport-belt",
      position = {
        x = 0,
        y = 39
      }
    },
    {
      entity_number = 175,
      name = "transport-belt",
      position = {
        x = 1,
        y = 39
      }
    },
    {
      entity_number = 176,
      name = "transport-belt",
      position = {
        x = 0,
        y = 40
      }
    },
    {
      entity_number = 177,
      name = "transport-belt",
      position = {
        x = 1,
        y = 40
      }
    },
    {
      direction = 2,
      entity_number = 178,
      name = "transport-belt",
      position = {
        x = -5,
        y = 42
      }
    },
    {
      direction = 2,
      entity_number = 179,
      name = "transport-belt",
      position = {
        x = -4,
        y = 42
      }
    },
    {
      direction = 2,
      entity_number = 180,
      name = "transport-belt",
      position = {
        x = -3,
        y = 42
      }
    },
    {
      direction = 2,
      entity_number = 181,
      name = "transport-belt",
      position = {
        x = -2,
        y = 42
      }
    },
    {
      direction = 2,
      entity_number = 182,
      name = "transport-belt",
      position = {
        x = -1,
        y = 42
      }
    },
    {
      entity_number = 183,
      name = "transport-belt",
      position = {
        x = 0,
        y = 41
      }
    },
    {
      entity_number = 184,
      name = "transport-belt",
      position = {
        x = 1,
        y = 41
      }
    },
    {
      entity_number = 185,
      name = "transport-belt",
      position = {
        x = 0,
        y = 42
      }
    },
    {
      entity_number = 186,
      name = "transport-belt",
      position = {
        x = 1,
        y = 42
      }
    },
    {
      direction = 2,
      entity_number = 187,
      name = "transport-belt",
      position = {
        x = -5,
        y = 43
      }
    },
    {
      direction = 2,
      entity_number = 188,
      name = "transport-belt",
      position = {
        x = -4,
        y = 43
      }
    },
    {
      direction = 2,
      entity_number = 189,
      name = "transport-belt",
      position = {
        x = -3,
        y = 43
      }
    },
    {
      direction = 2,
      entity_number = 190,
      name = "transport-belt",
      position = {
        x = -2,
        y = 43
      }
    },
    {
      direction = 2,
      entity_number = 191,
      name = "transport-belt",
      position = {
        x = -1,
        y = 43
      }
    },
    {
      direction = 2,
      entity_number = 192,
      name = "transport-belt",
      position = {
        x = 0,
        y = 43
      }
    },
    {
      entity_number = 193,
      name = "transport-belt",
      position = {
        x = 1,
        y = 43
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap-copper-transport"
}