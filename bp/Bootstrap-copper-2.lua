return 
{
  anchor = {
    x = 8,
    y = 2
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = -12,
        y = -7
      }
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = -8,
        y = -7
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = -2,
        y = -5
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      entity_number = 5,
      name = "inserter",
      position = {
        x = 7,
        y = 2
      }
    },
    {
      entity_number = 6,
      name = "inserter",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      entity_number = 7,
      name = "assembling-machine-1",
      position = {
        x = 7,
        y = 4
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 8,
      name = "assembling-machine-1",
      position = {
        x = 10,
        y = 4
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 10,
      name = "inserter",
      position = {
        x = 11,
        y = 2
      }
    },
    {
      entity_number = 11,
      name = "inserter",
      position = {
        x = 10,
        y = 2
      }
    },
    {
      entity_number = 12,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 4
      }
    },
    {
      entity_number = 13,
      name = "small-electric-pole",
      position = {
        x = 12,
        y = 4
      }
    },
    {
      entity_number = 14,
      name = "iron-chest",
      position = {
        x = 7,
        y = 7
      }
    },
    {
      entity_number = 15,
      name = "inserter",
      position = {
        x = 7,
        y = 6
      }
    },
    {
      entity_number = 16,
      name = "iron-chest",
      position = {
        x = 11,
        y = 7
      }
    },
    {
      entity_number = 17,
      name = "inserter",
      position = {
        x = 11,
        y = 6
      }
    }
  },
  icons = {
    {
      index = 2,
      signal = {
        name = "assembling-machine-1",
        type = "item"
      }
    }
  },
  name = "Bootstrap-copper-2"
}