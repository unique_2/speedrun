return 
{
  anchor = {
    x = -9,
    y = -4
  },
  entities = {
    {
      entity_number = 2,
      name = "assembling-machine-1",
      position = {
        x = -3,
        y = -3
      },
      recipe = "stone-furnace"
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "fast-inserter",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      entity_number = 6,
      name = "iron-chest",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = -5,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 8,
      name = "fast-inserter",
      position = {
        x = -3,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -4,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 10,
      name = "inserter",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -3,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "fast-inserter",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      entity_number = 13,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -2,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 5,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 7,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 8,
        y = 0
      }
    },
    {
      entity_number = 26,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      entity_number = 27,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 2
      }
    },
    {
      entity_number = 28,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      entity_number = 29,
      name = "electric-mining-drill",
      position = {
        x = 8,
        y = 2
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Furnace-assembler-1"
}