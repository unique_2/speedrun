return {
  anchor = {
    x = 8,
    y = 7
  },
  entities = {
    {
      entity_number = 1,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -9.5
      }
    },
    {
      entity_number = 2,
      name = "stone-furnace",
      position = {
        x = -0.5,
        y = -9.5
      }
    },
    {
      entity_number = 3,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = -9.5
      }
    },
    {
      entity_number = 4,
      name = "stone-furnace",
      position = {
        x = 3.5,
        y = -9.5
      }
    },
    {
      entity_number = 5,
      name = "stone-furnace",
      position = {
        x = 5.5,
        y = -9.5
      }
    },
    {
      entity_number = 6,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -9.5
      }
    },
    {
      entity_number = 7,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = -6.5
      }
    },
    {
      entity_number = 8,
      name = "stone-furnace",
      position = {
        x = 3.5,
        y = -7.5
      }
    },
    {
      entity_number = 9,
      name = "stone-furnace",
      position = {
        x = 5.5,
        y = -7.5
      }
    },
    {
      entity_number = 10,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -7.5
      }
    },
    {
      entity_number = 11,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = -4.5
      }
    },
    {
      entity_number = 12,
      name = "stone-furnace",
      position = {
        x = 5.5,
        y = -5.5
      }
    },
    {
      entity_number = 13,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -5.5
      }
    },
    {
      entity_number = 14,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = -2.5
      }
    },
    {
      entity_number = 15,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = -0.5
      }
    },
    {
      entity_number = 16,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = 1.5
      }
    },
    {
      entity_number = 17,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = 3.5
      }
    },
    {
      entity_number = 18,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = 5.5
      }
    },
    {
      entity_number = 19,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = 7.5
      }
    },
    {
      entity_number = 20,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 6.5
      }
    },
    {
      entity_number = 21,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 6.5
      }
    },
    {
      entity_number = 23,
      name = "stone-furnace",
      position = {
        x = -7.5,
        y = 9.5
      }
    },
    {
      entity_number = 24,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 8.5
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    }
  },
  name = "Furnaces"
}