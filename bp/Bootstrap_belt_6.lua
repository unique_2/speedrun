return 
{
  anchor = {
    x = -24,
    y = -3
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 16,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 17,
        y = -18
      }
    },
    {
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 16,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 18,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 19,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 20,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 21,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 22,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 23,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 24,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 25,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 26,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 27,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 12,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 13,
        y = -16
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 12,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 14,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 15,
        y = -16
      }
    },
    {
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 16,
        y = -16
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 12,
        y = -14
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 12,
        y = -13
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 12,
        y = -12
      }
    },
    {
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 12,
        y = -11
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 12,
        y = -10
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 12,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 1,
        y = -8
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 2,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 3,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 5,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 6,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 7,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 8,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 9,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 10,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 11,
        y = -8
      }
    },
    {
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 12,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "splitter",
      position = {
        x = -25,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = -24,
        y = -5
      }
    },
    {
      entity_number = 42,
      name = "splitter",
      position = {
        x = -22.5,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = -22,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = -21,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = -20,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -19,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -18,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -17,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -17,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = -16,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = -15,
        y = -5
      }
    },
    {
      entity_number = 52,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -14,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -13,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "splitter",
      position = {
        x = -12,
        y = -4.5
      }
    },
    {
      direction = 2,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = -11,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -10,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -9,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -8,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = -7,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = -6,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = -5,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = -4,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = -3,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = -2,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = -27,
        y = -4
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = -27,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = -26,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = -24,
        y = -4
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = -23,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 75,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = -2
      }
    },
    {
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = -22,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = -21,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = -20,
        y = -4
      }
    },
    {
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = -20,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = -13,
        y = -4
      }
    },
    {
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = -13,
        y = -3
      }
    },
    {
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = -27,
        y = -2
      }
    },
    {
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = -27,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 84,
      name = "electric-mining-drill",
      position = {
        x = -25,
        y = 0
      }
    },
    {
      entity_number = 85,
      name = "small-electric-pole",
      position = {
        x = -24,
        y = -2
      }
    },
    {
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = -20,
        y = -2
      }
    },
    {
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = -20,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 88,
      name = "electric-mining-drill",
      position = {
        x = -15,
        y = -1
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -13,
        y = -2
      }
    },
    {
      entity_number = 90,
      name = "underground-belt",
      position = {
        x = -13,
        y = -1
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 91,
      name = "electric-mining-drill",
      position = {
        x = -11,
        y = -1
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = -27,
        y = 0
      }
    },
    {
      entity_number = 93,
      name = "small-electric-pole",
      position = {
        x = -28,
        y = 1
      }
    },
    {
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = -27,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 95,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 1
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = -20,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 97,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 1
      }
    },
    {
      entity_number = 98,
      name = "underground-belt",
      position = {
        x = -20,
        y = 1
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 99,
      name = "electric-mining-drill",
      position = {
        x = -15,
        y = 2
      }
    },
    {
      entity_number = 100,
      name = "small-electric-pole",
      position = {
        x = -13,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 101,
      name = "electric-mining-drill",
      position = {
        x = -11,
        y = 2
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = -27,
        y = 2
      }
    },
    {
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = -27,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 104,
      name = "electric-mining-drill",
      position = {
        x = -25,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 105,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 4
      }
    },
    {
      entity_number = 106,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 107,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 4
      }
    },
    {
      entity_number = 108,
      name = "underground-belt",
      position = {
        x = -13,
        y = 2
      },
      type = "input"
    },
    {
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = -13,
        y = 3
      }
    },
    {
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = -27,
        y = 4
      }
    },
    {
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = -27,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 112,
      name = "electric-mining-drill",
      position = {
        x = -25,
        y = 6
      }
    },
    {
      entity_number = 113,
      name = "underground-belt",
      position = {
        x = -20,
        y = 4
      },
      type = "input"
    },
    {
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = -20,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 115,
      name = "electric-mining-drill",
      position = {
        x = -15,
        y = 5
      }
    },
    {
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = -13,
        y = 4
      }
    },
    {
      entity_number = 117,
      name = "underground-belt",
      position = {
        x = -13,
        y = 5
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 118,
      name = "electric-mining-drill",
      position = {
        x = -11,
        y = 5
      }
    },
    {
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = -27,
        y = 6
      }
    },
    {
      entity_number = 120,
      name = "small-electric-pole",
      position = {
        x = -28,
        y = 7
      }
    },
    {
      entity_number = 121,
      name = "transport-belt",
      position = {
        x = -27,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 122,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 7
      }
    },
    {
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = -20,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 124,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 7
      }
    },
    {
      entity_number = 125,
      name = "underground-belt",
      position = {
        x = -20,
        y = 7
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 126,
      name = "electric-mining-drill",
      position = {
        x = -15,
        y = 8
      }
    },
    {
      entity_number = 127,
      name = "small-electric-pole",
      position = {
        x = -13,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 128,
      name = "electric-mining-drill",
      position = {
        x = -11,
        y = 8
      }
    },
    {
      entity_number = 129,
      name = "transport-belt",
      position = {
        x = -27,
        y = 8
      }
    },
    {
      entity_number = 130,
      name = "transport-belt",
      position = {
        x = -27,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 131,
      name = "electric-mining-drill",
      position = {
        x = -25,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 132,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 10
      }
    },
    {
      entity_number = 133,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 134,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 10
      }
    },
    {
      entity_number = 135,
      name = "underground-belt",
      position = {
        x = -13,
        y = 8
      },
      type = "input"
    },
    {
      entity_number = 136,
      name = "transport-belt",
      position = {
        x = -27,
        y = 10
      }
    },
    {
      entity_number = 137,
      name = "transport-belt",
      position = {
        x = -27,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 138,
      name = "electric-mining-drill",
      position = {
        x = -25,
        y = 12
      }
    },
    {
      entity_number = 139,
      name = "underground-belt",
      position = {
        x = -20,
        y = 10
      },
      type = "input"
    },
    {
      entity_number = 140,
      name = "transport-belt",
      position = {
        x = -20,
        y = 11
      }
    },
    {
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = -27,
        y = 12
      }
    },
    {
      entity_number = 142,
      name = "small-electric-pole",
      position = {
        x = -28,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 143,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 13
      }
    },
    {
      entity_number = 144,
      name = "transport-belt",
      position = {
        x = -20,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 145,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 13
      }
    },
    {
      entity_number = 146,
      name = "underground-belt",
      position = {
        x = -20,
        y = 13
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 147,
      name = "electric-mining-drill",
      position = {
        x = -22,
        y = 16
      }
    },
    {
      entity_number = 148,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 149,
      name = "electric-mining-drill",
      position = {
        x = -18,
        y = 16
      }
    },
    {
      entity_number = 150,
      name = "underground-belt",
      position = {
        x = -20,
        y = 16
      },
      type = "input"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_6"
}