return {
  anchor = {
    x = 3,
    y = -5
  },
  entities = {
    {
      direction = 6,
      entity_number = 1,
      name = "inserter",
      position = {
        x = 5,
        y = -26
      }
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -25
      }
    },
    {
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 4,
        y = -26
      }
    },
    {
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 4,
        y = -25
      }
    },
    {
      entity_number = 5,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -25.5
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "inserter",
      position = {
        x = 5,
        y = -23
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 4,
        y = -23
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 4,
        y = -24
      }
    },
    {
      entity_number = 9,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -23.5
      }
    },
    {
      direction = 6,
      entity_number = 10,
      name = "inserter",
      position = {
        x = 5,
        y = -22
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 4,
        y = -21
      }
    },
    {
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 4,
        y = -22
      }
    },
    {
      entity_number = 13,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -21.5
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "inserter",
      position = {
        x = 5,
        y = -19
      }
    },
    {
      entity_number = 15,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -20
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 4,
        y = -20
      }
    },
    {
      entity_number = 18,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -19.5
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "inserter",
      position = {
        x = 5,
        y = -18
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 4,
        y = -17
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 4,
        y = -18
      }
    },
    {
      entity_number = 22,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -17.5
      }
    },
    {
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 4,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 24,
      name = "inserter",
      position = {
        x = 5,
        y = -15
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 4,
        y = -15
      }
    },
    {
      entity_number = 26,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -15.5
      }
    },
    {
      direction = 6,
      entity_number = 27,
      name = "inserter",
      position = {
        x = 5,
        y = -14
      }
    },
    {
      entity_number = 28,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -13
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 4,
        y = -13
      }
    },
    {
      entity_number = 31,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -13.5
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "inserter",
      position = {
        x = 5,
        y = -11
      }
    },
    {
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 4,
        y = -11
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 4,
        y = -12
      }
    },
    {
      entity_number = 35,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -11.5
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "inserter",
      position = {
        x = 5,
        y = -10
      }
    },
    {
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 4,
        y = -9
      }
    },
    {
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 4,
        y = -10
      }
    },
    {
      entity_number = 39,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -9.5
      }
    },
    {
      direction = 6,
      entity_number = 40,
      name = "inserter",
      position = {
        x = 5,
        y = -7
      }
    },
    {
      entity_number = 41,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -8
      }
    },
    {
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 4,
        y = -7
      }
    },
    {
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      entity_number = 44,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -7.5
      }
    },
    {
      direction = 6,
      entity_number = 46,
      name = "inserter",
      position = {
        x = 5,
        y = -6
      }
    },
    {
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 4,
        y = -5
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      entity_number = 49,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -5.5
      }
    },
    {
      direction = 2,
      entity_number = 50,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 51,
      name = "inserter",
      position = {
        x = 5,
        y = -3
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 4,
        y = -4
      }
    },
    {
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 4,
        y = -3
      }
    },
    {
      entity_number = 54,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -3.5
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "inserter",
      position = {
        x = 5,
        y = -2
      }
    },
    {
      entity_number = 57,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -1
      }
    },
    {
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 4,
        y = -2
      }
    },
    {
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      entity_number = 60,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = -1.5
      }
    },
    {
      direction = 6,
      entity_number = 61,
      name = "inserter",
      position = {
        x = 5,
        y = 1
      }
    },
    {
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 4,
        y = 1
      }
    },
    {
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      entity_number = 64,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 0.5
      }
    },
    {
      direction = 2,
      entity_number = 65,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 66,
      name = "inserter",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 4,
        y = 3
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      entity_number = 69,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 2.5
      }
    },
    {
      direction = 2,
      entity_number = 70,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 71,
      name = "inserter",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      entity_number = 72,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 4
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      entity_number = 75,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 4.5
      }
    },
    {
      direction = 6,
      entity_number = 76,
      name = "inserter",
      position = {
        x = 5,
        y = 6
      }
    },
    {
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 4,
        y = 7
      }
    },
    {
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      entity_number = 79,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 6.5
      }
    },
    {
      direction = 2,
      entity_number = 80,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 81,
      name = "inserter",
      position = {
        x = 5,
        y = 9
      }
    },
    {
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      entity_number = 84,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 8.5
      }
    },
    {
      direction = 2,
      entity_number = 85,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 86,
      name = "inserter",
      position = {
        x = 5,
        y = 10
      }
    },
    {
      entity_number = 87,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 11
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 4,
        y = 10
      }
    },
    {
      entity_number = 90,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 10.5
      }
    },
    {
      direction = 6,
      entity_number = 91,
      name = "inserter",
      position = {
        x = 5,
        y = 13
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 4,
        y = 12
      }
    },
    {
      entity_number = 94,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 12.5
      }
    },
    {
      direction = 2,
      entity_number = 95,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 15
      }
    },
    {
      entity_number = 96,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 97,
      name = "inserter",
      position = {
        x = 5,
        y = 14
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 4,
        y = 15
      }
    },
    {
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      entity_number = 100,
      name = "stone-furnace",
      position = {
        x = 6.5,
        y = 14.5
      }
    },
    {
      direction = 2,
      entity_number = 101,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 18
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = 4,
        y = 16
      }
    },
    {
      entity_number = 104,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 18
      }
    },
    {
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = 4,
        y = 19
      }
    },
    {
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = 4,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 107,
      name = "electric-mining-drill",
      position = {
        x = -5,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = -3,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 109,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = 21
      }
    },
    {
      direction = 2,
      entity_number = 110,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = 21
      }
    },
    {
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = 4,
        y = 21
      }
    },
    {
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 4,
        y = 20
      }
    },
    {
      entity_number = 113,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = -3,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = -3,
        y = 22
      }
    },
    {
      direction = 2,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = -1,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = -2,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = 1,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = 0,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 120,
      name = "transport-belt",
      position = {
        x = 3,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 121,
      name = "transport-belt",
      position = {
        x = 2,
        y = 23
      }
    },
    {
      entity_number = 122,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 5,
        y = 23
      }
    },
    {
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 4,
        y = 23
      }
    },
    {
      entity_number = 125,
      name = "transport-belt",
      position = {
        x = 4,
        y = 22
      }
    },
    {
      entity_number = 126,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 27
      }
    },
    {
      entity_number = 127,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 27
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap-6"
}