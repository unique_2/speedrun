return 
{
  anchor = {
    x = -10,
    y = -15
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -8,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -9,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -8,
        y = -19
      }
    },
    {
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -9,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -6,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -7,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -7,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -6,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -4,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -5,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -5,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -4,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -2,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -3,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -3,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -1,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 2,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 1,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 4,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 3,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 6,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 6,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 5,
        y = -20
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -8,
        y = -18
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -8,
        y = -17
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = -9,
        y = -18
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = -9,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = -7,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = -6,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -5,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = -4,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "splitter",
      position = {
        x = -2.5,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -3,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = -2,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = -1,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 3,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 4,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 5,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "splitter",
      position = {
        x = 6.5,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 6,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 7,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 8,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "underground-belt",
      position = {
        x = -10,
        y = -16
      },
      type = "input"
    },
    {
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -8,
        y = -16
      }
    },
    {
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -8,
        y = -15
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -9,
        y = -16
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -9,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = -7,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = -7,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 52,
      name = "underground-belt",
      position = {
        x = -6,
        y = -16
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 53,
      name = "splitter",
      position = {
        x = -5,
        y = -15.5
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -4,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "underground-belt",
      position = {
        x = -4,
        y = -16
      },
      type = "input"
    },
    {
      entity_number = 56,
      name = "splitter",
      position = {
        x = -2.5,
        y = -16
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -3,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -1,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -1,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 60,
      name = "underground-belt",
      position = {
        x = 0,
        y = -16
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 61,
      name = "underground-belt",
      position = {
        x = 1,
        y = -16
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 3,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 3,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = 4,
        y = -16
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 5,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 5,
        y = -16
      }
    },
    {
      entity_number = 67,
      name = "splitter",
      position = {
        x = 6.5,
        y = -16
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 6,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 8,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 8,
        y = -16
      }
    },
    {
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = -9,
        y = -14
      }
    },
    {
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = -9,
        y = -13
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = -8,
        y = -14
      }
    },
    {
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = -8,
        y = -13
      }
    },
    {
      entity_number = 75,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -12.5
      }
    },
    {
      direction = 2,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = -7,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = -6,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 78,
      name = "inserter",
      position = {
        x = -5,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = -5,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = -4,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = -4,
        y = -14
      }
    },
    {
      entity_number = 82,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = -2,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = -2,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 85,
      name = "inserter",
      position = {
        x = -1,
        y = -13
      }
    },
    {
      direction = 6,
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = -1,
        y = -14
      }
    },
    {
      entity_number = 87,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -12.5
      }
    },
    {
      entity_number = 88,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -12.5
      }
    },
    {
      direction = 2,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 3,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 90,
      name = "inserter",
      position = {
        x = 4,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 5,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 5,
        y = -14
      }
    },
    {
      entity_number = 94,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 7,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = 7,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 97,
      name = "inserter",
      position = {
        x = 8,
        y = -13
      }
    },
    {
      direction = 6,
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 8,
        y = -14
      }
    },
    {
      entity_number = 99,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -12.5
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = -9,
        y = -12
      }
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = -9,
        y = -11
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = -8,
        y = -12
      }
    },
    {
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = -8,
        y = -11
      }
    },
    {
      entity_number = 104,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -10.5
      }
    },
    {
      direction = 6,
      entity_number = 105,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 106,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = -4,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = -4,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 109,
      name = "underground-belt",
      position = {
        x = -3,
        y = -11
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = -3,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = -2,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = -2,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 113,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -11
      }
    },
    {
      direction = 2,
      entity_number = 114,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -12
      }
    },
    {
      entity_number = 115,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -10.5
      }
    },
    {
      entity_number = 116,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -10.5
      }
    },
    {
      direction = 6,
      entity_number = 117,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 118,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = 5,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 120,
      name = "transport-belt",
      position = {
        x = 5,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 121,
      name = "underground-belt",
      position = {
        x = 6,
        y = -11
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 122,
      name = "transport-belt",
      position = {
        x = 6,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 7,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 7,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 125,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -11
      }
    },
    {
      direction = 2,
      entity_number = 126,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -12
      }
    },
    {
      entity_number = 127,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -10.5
      }
    },
    {
      entity_number = 128,
      name = "transport-belt",
      position = {
        x = -9,
        y = -9
      }
    },
    {
      entity_number = 129,
      name = "transport-belt",
      position = {
        x = -9,
        y = -10
      }
    },
    {
      entity_number = 130,
      name = "transport-belt",
      position = {
        x = -8,
        y = -10
      }
    },
    {
      entity_number = 131,
      name = "transport-belt",
      position = {
        x = -8,
        y = -9
      }
    },
    {
      entity_number = 132,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -8.5
      }
    },
    {
      direction = 2,
      entity_number = 133,
      name = "inserter",
      position = {
        x = -5,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 134,
      name = "inserter",
      position = {
        x = -5,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = -4,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 136,
      name = "transport-belt",
      position = {
        x = -4,
        y = -10
      }
    },
    {
      entity_number = 137,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 138,
      name = "transport-belt",
      position = {
        x = -2,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 139,
      name = "transport-belt",
      position = {
        x = -2,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 140,
      name = "inserter",
      position = {
        x = -1,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 141,
      name = "inserter",
      position = {
        x = -1,
        y = -10
      }
    },
    {
      entity_number = 142,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -8.5
      }
    },
    {
      entity_number = 143,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -8.5
      }
    },
    {
      direction = 2,
      entity_number = 144,
      name = "inserter",
      position = {
        x = 4,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 145,
      name = "inserter",
      position = {
        x = 4,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 146,
      name = "transport-belt",
      position = {
        x = 5,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 147,
      name = "transport-belt",
      position = {
        x = 5,
        y = -10
      }
    },
    {
      entity_number = 148,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = 7,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 150,
      name = "transport-belt",
      position = {
        x = 7,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 151,
      name = "inserter",
      position = {
        x = 8,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 152,
      name = "inserter",
      position = {
        x = 8,
        y = -10
      }
    },
    {
      entity_number = 153,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -8.5
      }
    },
    {
      entity_number = 154,
      name = "transport-belt",
      position = {
        x = -9,
        y = -7
      }
    },
    {
      entity_number = 155,
      name = "transport-belt",
      position = {
        x = -9,
        y = -8
      }
    },
    {
      entity_number = 156,
      name = "transport-belt",
      position = {
        x = -8,
        y = -8
      }
    },
    {
      entity_number = 157,
      name = "transport-belt",
      position = {
        x = -8,
        y = -7
      }
    },
    {
      entity_number = 158,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -6.5
      }
    },
    {
      direction = 6,
      entity_number = 159,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 160,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = -4,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 162,
      name = "transport-belt",
      position = {
        x = -4,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 163,
      name = "underground-belt",
      position = {
        x = -3,
        y = -7
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 164,
      name = "underground-belt",
      position = {
        x = -3,
        y = -8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 165,
      name = "transport-belt",
      position = {
        x = -2,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 166,
      name = "transport-belt",
      position = {
        x = -2,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 167,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 168,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -8
      }
    },
    {
      entity_number = 169,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -6.5
      }
    },
    {
      entity_number = 170,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -6.5
      }
    },
    {
      direction = 6,
      entity_number = 171,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 172,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 173,
      name = "transport-belt",
      position = {
        x = 5,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 174,
      name = "transport-belt",
      position = {
        x = 5,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 175,
      name = "underground-belt",
      position = {
        x = 6,
        y = -7
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 176,
      name = "underground-belt",
      position = {
        x = 6,
        y = -8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 177,
      name = "transport-belt",
      position = {
        x = 7,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 178,
      name = "transport-belt",
      position = {
        x = 7,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 179,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 180,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -8
      }
    },
    {
      entity_number = 181,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -6.5
      }
    },
    {
      entity_number = 182,
      name = "transport-belt",
      position = {
        x = -9,
        y = -5
      }
    },
    {
      entity_number = 183,
      name = "transport-belt",
      position = {
        x = -9,
        y = -6
      }
    },
    {
      entity_number = 184,
      name = "transport-belt",
      position = {
        x = -8,
        y = -6
      }
    },
    {
      entity_number = 185,
      name = "transport-belt",
      position = {
        x = -8,
        y = -5
      }
    },
    {
      entity_number = 186,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -4.5
      }
    },
    {
      direction = 2,
      entity_number = 187,
      name = "inserter",
      position = {
        x = -5,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 188,
      name = "inserter",
      position = {
        x = -5,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 189,
      name = "transport-belt",
      position = {
        x = -4,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 190,
      name = "transport-belt",
      position = {
        x = -4,
        y = -6
      }
    },
    {
      entity_number = 191,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 192,
      name = "transport-belt",
      position = {
        x = -2,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 193,
      name = "transport-belt",
      position = {
        x = -2,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 194,
      name = "inserter",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 195,
      name = "inserter",
      position = {
        x = -1,
        y = -6
      }
    },
    {
      entity_number = 196,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -4.5
      }
    },
    {
      entity_number = 197,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -4.5
      }
    },
    {
      direction = 2,
      entity_number = 198,
      name = "inserter",
      position = {
        x = 4,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 199,
      name = "inserter",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 200,
      name = "transport-belt",
      position = {
        x = 5,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 201,
      name = "transport-belt",
      position = {
        x = 5,
        y = -6
      }
    },
    {
      entity_number = 202,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 203,
      name = "transport-belt",
      position = {
        x = 7,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 204,
      name = "transport-belt",
      position = {
        x = 7,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 205,
      name = "inserter",
      position = {
        x = 8,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 206,
      name = "inserter",
      position = {
        x = 8,
        y = -6
      }
    },
    {
      entity_number = 207,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -4.5
      }
    },
    {
      entity_number = 208,
      name = "transport-belt",
      position = {
        x = -9,
        y = -3
      }
    },
    {
      entity_number = 209,
      name = "transport-belt",
      position = {
        x = -9,
        y = -4
      }
    },
    {
      entity_number = 210,
      name = "transport-belt",
      position = {
        x = -8,
        y = -4
      }
    },
    {
      entity_number = 211,
      name = "transport-belt",
      position = {
        x = -8,
        y = -3
      }
    },
    {
      entity_number = 212,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -2.5
      }
    },
    {
      direction = 6,
      entity_number = 213,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 214,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 215,
      name = "transport-belt",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 216,
      name = "transport-belt",
      position = {
        x = -4,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 217,
      name = "underground-belt",
      position = {
        x = -3,
        y = -3
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 218,
      name = "underground-belt",
      position = {
        x = -3,
        y = -4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 219,
      name = "transport-belt",
      position = {
        x = -2,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 220,
      name = "transport-belt",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 221,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 222,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      entity_number = 223,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -2.5
      }
    },
    {
      entity_number = 224,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -2.5
      }
    },
    {
      direction = 6,
      entity_number = 225,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 226,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 227,
      name = "transport-belt",
      position = {
        x = 5,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 228,
      name = "transport-belt",
      position = {
        x = 5,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 229,
      name = "underground-belt",
      position = {
        x = 6,
        y = -3
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 230,
      name = "underground-belt",
      position = {
        x = 6,
        y = -4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 231,
      name = "transport-belt",
      position = {
        x = 7,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 232,
      name = "transport-belt",
      position = {
        x = 7,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 233,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 234,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -4
      }
    },
    {
      entity_number = 235,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -2.5
      }
    },
    {
      entity_number = 236,
      name = "small-electric-pole",
      position = {
        x = -10,
        y = -2
      }
    },
    {
      entity_number = 237,
      name = "transport-belt",
      position = {
        x = -9,
        y = -1
      }
    },
    {
      entity_number = 238,
      name = "transport-belt",
      position = {
        x = -9,
        y = -2
      }
    },
    {
      entity_number = 239,
      name = "transport-belt",
      position = {
        x = -8,
        y = -2
      }
    },
    {
      entity_number = 240,
      name = "transport-belt",
      position = {
        x = -8,
        y = -1
      }
    },
    {
      entity_number = 241,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = -0.5
      }
    },
    {
      direction = 2,
      entity_number = 242,
      name = "inserter",
      position = {
        x = -5,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 243,
      name = "inserter",
      position = {
        x = -5,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 244,
      name = "transport-belt",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 245,
      name = "transport-belt",
      position = {
        x = -4,
        y = -2
      }
    },
    {
      entity_number = 246,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 247,
      name = "transport-belt",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 248,
      name = "transport-belt",
      position = {
        x = -2,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 249,
      name = "inserter",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 250,
      name = "inserter",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      entity_number = 251,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -0.5
      }
    },
    {
      entity_number = 252,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -0.5
      }
    },
    {
      direction = 2,
      entity_number = 253,
      name = "inserter",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 254,
      name = "inserter",
      position = {
        x = 4,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 255,
      name = "transport-belt",
      position = {
        x = 5,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 256,
      name = "transport-belt",
      position = {
        x = 5,
        y = -2
      }
    },
    {
      entity_number = 257,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 258,
      name = "transport-belt",
      position = {
        x = 7,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 259,
      name = "transport-belt",
      position = {
        x = 7,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 260,
      name = "inserter",
      position = {
        x = 8,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 261,
      name = "inserter",
      position = {
        x = 8,
        y = -2
      }
    },
    {
      entity_number = 262,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -0.5
      }
    },
    {
      entity_number = 263,
      name = "transport-belt",
      position = {
        x = -9,
        y = 1
      }
    },
    {
      entity_number = 264,
      name = "transport-belt",
      position = {
        x = -9,
        y = 0
      }
    },
    {
      entity_number = 265,
      name = "transport-belt",
      position = {
        x = -8,
        y = 0
      }
    },
    {
      entity_number = 266,
      name = "transport-belt",
      position = {
        x = -8,
        y = 1
      }
    },
    {
      entity_number = 267,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 1.5
      }
    },
    {
      direction = 6,
      entity_number = 268,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 269,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 270,
      name = "transport-belt",
      position = {
        x = -4,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 271,
      name = "transport-belt",
      position = {
        x = -4,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 272,
      name = "underground-belt",
      position = {
        x = -3,
        y = 1
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 273,
      name = "underground-belt",
      position = {
        x = -3,
        y = 0
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 274,
      name = "transport-belt",
      position = {
        x = -2,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 275,
      name = "transport-belt",
      position = {
        x = -2,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 276,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 277,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      entity_number = 278,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 1.5
      }
    },
    {
      entity_number = 279,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 1.5
      }
    },
    {
      direction = 6,
      entity_number = 280,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 281,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 282,
      name = "transport-belt",
      position = {
        x = 5,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 283,
      name = "transport-belt",
      position = {
        x = 5,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 284,
      name = "underground-belt",
      position = {
        x = 6,
        y = 1
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 285,
      name = "underground-belt",
      position = {
        x = 6,
        y = 0
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 286,
      name = "transport-belt",
      position = {
        x = 7,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 287,
      name = "transport-belt",
      position = {
        x = 7,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 288,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 289,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 0
      }
    },
    {
      entity_number = 290,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 1.5
      }
    },
    {
      entity_number = 291,
      name = "transport-belt",
      position = {
        x = -9,
        y = 3
      }
    },
    {
      entity_number = 292,
      name = "transport-belt",
      position = {
        x = -9,
        y = 2
      }
    },
    {
      entity_number = 293,
      name = "transport-belt",
      position = {
        x = -8,
        y = 2
      }
    },
    {
      entity_number = 294,
      name = "transport-belt",
      position = {
        x = -8,
        y = 3
      }
    },
    {
      entity_number = 295,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 3.5
      }
    },
    {
      direction = 2,
      entity_number = 296,
      name = "inserter",
      position = {
        x = -5,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 297,
      name = "inserter",
      position = {
        x = -5,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 298,
      name = "transport-belt",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 299,
      name = "transport-belt",
      position = {
        x = -4,
        y = 2
      }
    },
    {
      entity_number = 300,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 301,
      name = "transport-belt",
      position = {
        x = -2,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 302,
      name = "transport-belt",
      position = {
        x = -2,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 303,
      name = "inserter",
      position = {
        x = -1,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 304,
      name = "inserter",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      entity_number = 305,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 3.5
      }
    },
    {
      entity_number = 306,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 3.5
      }
    },
    {
      direction = 2,
      entity_number = 307,
      name = "inserter",
      position = {
        x = 4,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 308,
      name = "inserter",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 309,
      name = "transport-belt",
      position = {
        x = 5,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 310,
      name = "transport-belt",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      entity_number = 311,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 312,
      name = "transport-belt",
      position = {
        x = 7,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 313,
      name = "transport-belt",
      position = {
        x = 7,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 314,
      name = "inserter",
      position = {
        x = 8,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 315,
      name = "inserter",
      position = {
        x = 8,
        y = 2
      }
    },
    {
      entity_number = 316,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 3.5
      }
    },
    {
      entity_number = 317,
      name = "transport-belt",
      position = {
        x = -9,
        y = 5
      }
    },
    {
      entity_number = 318,
      name = "transport-belt",
      position = {
        x = -9,
        y = 4
      }
    },
    {
      entity_number = 319,
      name = "transport-belt",
      position = {
        x = -8,
        y = 4
      }
    },
    {
      entity_number = 320,
      name = "transport-belt",
      position = {
        x = -8,
        y = 5
      }
    },
    {
      entity_number = 321,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 5.5
      }
    },
    {
      direction = 6,
      entity_number = 322,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 323,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 324,
      name = "transport-belt",
      position = {
        x = -4,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 325,
      name = "transport-belt",
      position = {
        x = -4,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 326,
      name = "underground-belt",
      position = {
        x = -3,
        y = 5
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 327,
      name = "underground-belt",
      position = {
        x = -3,
        y = 4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 328,
      name = "transport-belt",
      position = {
        x = -2,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 329,
      name = "transport-belt",
      position = {
        x = -2,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 330,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 331,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 4
      }
    },
    {
      entity_number = 332,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 5.5
      }
    },
    {
      entity_number = 333,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 5.5
      }
    },
    {
      direction = 6,
      entity_number = 334,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 335,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 336,
      name = "transport-belt",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 337,
      name = "transport-belt",
      position = {
        x = 5,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 338,
      name = "underground-belt",
      position = {
        x = 6,
        y = 5
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 339,
      name = "underground-belt",
      position = {
        x = 6,
        y = 4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 340,
      name = "transport-belt",
      position = {
        x = 7,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 341,
      name = "transport-belt",
      position = {
        x = 7,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 342,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 343,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 4
      }
    },
    {
      entity_number = 344,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 5.5
      }
    },
    {
      entity_number = 345,
      name = "transport-belt",
      position = {
        x = -9,
        y = 7
      }
    },
    {
      entity_number = 346,
      name = "transport-belt",
      position = {
        x = -9,
        y = 6
      }
    },
    {
      entity_number = 347,
      name = "transport-belt",
      position = {
        x = -8,
        y = 7
      }
    },
    {
      entity_number = 348,
      name = "transport-belt",
      position = {
        x = -8,
        y = 6
      }
    },
    {
      entity_number = 349,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 7.5
      }
    },
    {
      direction = 2,
      entity_number = 350,
      name = "inserter",
      position = {
        x = -5,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 351,
      name = "inserter",
      position = {
        x = -5,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 352,
      name = "transport-belt",
      position = {
        x = -4,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 353,
      name = "transport-belt",
      position = {
        x = -4,
        y = 6
      }
    },
    {
      entity_number = 354,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 355,
      name = "transport-belt",
      position = {
        x = -2,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 356,
      name = "transport-belt",
      position = {
        x = -2,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 357,
      name = "inserter",
      position = {
        x = -1,
        y = 7
      }
    },
    {
      direction = 6,
      entity_number = 358,
      name = "inserter",
      position = {
        x = -1,
        y = 6
      }
    },
    {
      entity_number = 359,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 7.5
      }
    },
    {
      entity_number = 360,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 7.5
      }
    },
    {
      direction = 2,
      entity_number = 361,
      name = "inserter",
      position = {
        x = 4,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 362,
      name = "inserter",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 363,
      name = "transport-belt",
      position = {
        x = 5,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 364,
      name = "transport-belt",
      position = {
        x = 5,
        y = 6
      }
    },
    {
      entity_number = 365,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 366,
      name = "transport-belt",
      position = {
        x = 7,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 367,
      name = "transport-belt",
      position = {
        x = 7,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 368,
      name = "inserter",
      position = {
        x = 8,
        y = 7
      }
    },
    {
      direction = 6,
      entity_number = 369,
      name = "inserter",
      position = {
        x = 8,
        y = 6
      }
    },
    {
      entity_number = 370,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 7.5
      }
    },
    {
      entity_number = 371,
      name = "transport-belt",
      position = {
        x = -9,
        y = 9
      }
    },
    {
      entity_number = 372,
      name = "transport-belt",
      position = {
        x = -9,
        y = 8
      }
    },
    {
      entity_number = 373,
      name = "transport-belt",
      position = {
        x = -8,
        y = 8
      }
    },
    {
      entity_number = 374,
      name = "transport-belt",
      position = {
        x = -8,
        y = 9
      }
    },
    {
      entity_number = 375,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 9.5
      }
    },
    {
      direction = 6,
      entity_number = 376,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 377,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 378,
      name = "transport-belt",
      position = {
        x = -4,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 379,
      name = "transport-belt",
      position = {
        x = -4,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 380,
      name = "underground-belt",
      position = {
        x = -3,
        y = 9
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 381,
      name = "underground-belt",
      position = {
        x = -3,
        y = 8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 382,
      name = "transport-belt",
      position = {
        x = -2,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 383,
      name = "transport-belt",
      position = {
        x = -2,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 384,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 385,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 8
      }
    },
    {
      entity_number = 386,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 9.5
      }
    },
    {
      entity_number = 387,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 9.5
      }
    },
    {
      direction = 6,
      entity_number = 388,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 389,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 390,
      name = "transport-belt",
      position = {
        x = 5,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 391,
      name = "transport-belt",
      position = {
        x = 5,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 392,
      name = "underground-belt",
      position = {
        x = 6,
        y = 9
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 393,
      name = "underground-belt",
      position = {
        x = 6,
        y = 8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 394,
      name = "transport-belt",
      position = {
        x = 7,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 395,
      name = "transport-belt",
      position = {
        x = 7,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 396,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 397,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 8
      }
    },
    {
      entity_number = 398,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 9.5
      }
    },
    {
      entity_number = 399,
      name = "transport-belt",
      position = {
        x = -9,
        y = 11
      }
    },
    {
      entity_number = 400,
      name = "transport-belt",
      position = {
        x = -9,
        y = 10
      }
    },
    {
      entity_number = 401,
      name = "transport-belt",
      position = {
        x = -8,
        y = 10
      }
    },
    {
      entity_number = 402,
      name = "transport-belt",
      position = {
        x = -8,
        y = 11
      }
    },
    {
      entity_number = 403,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 11.5
      }
    },
    {
      direction = 2,
      entity_number = 404,
      name = "inserter",
      position = {
        x = -5,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 405,
      name = "inserter",
      position = {
        x = -5,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 406,
      name = "transport-belt",
      position = {
        x = -4,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 407,
      name = "transport-belt",
      position = {
        x = -4,
        y = 10
      }
    },
    {
      entity_number = 408,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 409,
      name = "transport-belt",
      position = {
        x = -2,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 410,
      name = "transport-belt",
      position = {
        x = -2,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 411,
      name = "inserter",
      position = {
        x = -1,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 412,
      name = "inserter",
      position = {
        x = -1,
        y = 10
      }
    },
    {
      entity_number = 413,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 11.5
      }
    },
    {
      entity_number = 414,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 11.5
      }
    },
    {
      direction = 2,
      entity_number = 415,
      name = "inserter",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 416,
      name = "inserter",
      position = {
        x = 4,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 417,
      name = "transport-belt",
      position = {
        x = 5,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 418,
      name = "transport-belt",
      position = {
        x = 5,
        y = 10
      }
    },
    {
      entity_number = 419,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 420,
      name = "transport-belt",
      position = {
        x = 7,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 421,
      name = "transport-belt",
      position = {
        x = 7,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 422,
      name = "inserter",
      position = {
        x = 8,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 423,
      name = "inserter",
      position = {
        x = 8,
        y = 10
      }
    },
    {
      entity_number = 424,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 11.5
      }
    },
    {
      entity_number = 425,
      name = "transport-belt",
      position = {
        x = -9,
        y = 13
      }
    },
    {
      entity_number = 426,
      name = "transport-belt",
      position = {
        x = -9,
        y = 12
      }
    },
    {
      entity_number = 427,
      name = "transport-belt",
      position = {
        x = -8,
        y = 12
      }
    },
    {
      entity_number = 428,
      name = "transport-belt",
      position = {
        x = -8,
        y = 13
      }
    },
    {
      entity_number = 429,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 13.5
      }
    },
    {
      direction = 6,
      entity_number = 430,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 431,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 432,
      name = "transport-belt",
      position = {
        x = -4,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 433,
      name = "transport-belt",
      position = {
        x = -4,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 434,
      name = "underground-belt",
      position = {
        x = -3,
        y = 13
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 435,
      name = "underground-belt",
      position = {
        x = -3,
        y = 12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 436,
      name = "transport-belt",
      position = {
        x = -2,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 437,
      name = "transport-belt",
      position = {
        x = -2,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 438,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 439,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 12
      }
    },
    {
      entity_number = 440,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 13.5
      }
    },
    {
      entity_number = 441,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 13.5
      }
    },
    {
      direction = 6,
      entity_number = 442,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 443,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 444,
      name = "transport-belt",
      position = {
        x = 5,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 445,
      name = "transport-belt",
      position = {
        x = 5,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 446,
      name = "underground-belt",
      position = {
        x = 6,
        y = 13
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 447,
      name = "underground-belt",
      position = {
        x = 6,
        y = 12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 448,
      name = "transport-belt",
      position = {
        x = 7,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 449,
      name = "transport-belt",
      position = {
        x = 7,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 450,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 451,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 12
      }
    },
    {
      entity_number = 452,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 13.5
      }
    },
    {
      entity_number = 453,
      name = "transport-belt",
      position = {
        x = -9,
        y = 15
      }
    },
    {
      entity_number = 454,
      name = "transport-belt",
      position = {
        x = -9,
        y = 14
      }
    },
    {
      entity_number = 455,
      name = "transport-belt",
      position = {
        x = -8,
        y = 15
      }
    },
    {
      entity_number = 456,
      name = "transport-belt",
      position = {
        x = -8,
        y = 14
      }
    },
    {
      entity_number = 457,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 15.5
      }
    },
    {
      direction = 2,
      entity_number = 458,
      name = "inserter",
      position = {
        x = -5,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 459,
      name = "inserter",
      position = {
        x = -5,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 460,
      name = "transport-belt",
      position = {
        x = -4,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 461,
      name = "transport-belt",
      position = {
        x = -4,
        y = 14
      }
    },
    {
      entity_number = 462,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 463,
      name = "transport-belt",
      position = {
        x = -2,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 464,
      name = "transport-belt",
      position = {
        x = -2,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 465,
      name = "inserter",
      position = {
        x = -1,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 466,
      name = "inserter",
      position = {
        x = -1,
        y = 14
      }
    },
    {
      entity_number = 467,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 15.5
      }
    },
    {
      entity_number = 468,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 15.5
      }
    },
    {
      direction = 2,
      entity_number = 469,
      name = "inserter",
      position = {
        x = 4,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 470,
      name = "inserter",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 471,
      name = "transport-belt",
      position = {
        x = 5,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 472,
      name = "transport-belt",
      position = {
        x = 5,
        y = 14
      }
    },
    {
      entity_number = 473,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 474,
      name = "transport-belt",
      position = {
        x = 7,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 475,
      name = "transport-belt",
      position = {
        x = 7,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 476,
      name = "inserter",
      position = {
        x = 8,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 477,
      name = "inserter",
      position = {
        x = 8,
        y = 14
      }
    },
    {
      entity_number = 478,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 15.5
      }
    },
    {
      entity_number = 479,
      name = "transport-belt",
      position = {
        x = -9,
        y = 17
      }
    },
    {
      entity_number = 480,
      name = "transport-belt",
      position = {
        x = -9,
        y = 16
      }
    },
    {
      entity_number = 481,
      name = "transport-belt",
      position = {
        x = -8,
        y = 16
      }
    },
    {
      entity_number = 482,
      name = "transport-belt",
      position = {
        x = -8,
        y = 17
      }
    },
    {
      entity_number = 483,
      name = "stone-furnace",
      position = {
        x = -6.5,
        y = 17.5
      }
    },
    {
      direction = 6,
      entity_number = 484,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 485,
      name = "long-handed-inserter",
      position = {
        x = -5,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 486,
      name = "transport-belt",
      position = {
        x = -4,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 487,
      name = "transport-belt",
      position = {
        x = -4,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 488,
      name = "underground-belt",
      position = {
        x = -3,
        y = 17
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 489,
      name = "underground-belt",
      position = {
        x = -3,
        y = 16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 490,
      name = "transport-belt",
      position = {
        x = -2,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 491,
      name = "transport-belt",
      position = {
        x = -2,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 492,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 17
      }
    },
    {
      direction = 2,
      entity_number = 493,
      name = "long-handed-inserter",
      position = {
        x = -1,
        y = 16
      }
    },
    {
      entity_number = 494,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = 17.5
      }
    },
    {
      direction = 6,
      entity_number = 495,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 496,
      name = "transport-belt",
      position = {
        x = 6,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 497,
      name = "transport-belt",
      position = {
        x = 5,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 498,
      name = "underground-belt",
      position = {
        x = 6,
        y = 16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 499,
      name = "transport-belt",
      position = {
        x = 7,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 500,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 16
      }
    },
    {
      entity_number = 501,
      name = "transport-belt",
      position = {
        x = -9,
        y = 19
      }
    },
    {
      entity_number = 502,
      name = "transport-belt",
      position = {
        x = -9,
        y = 18
      }
    },
    {
      entity_number = 503,
      name = "transport-belt",
      position = {
        x = -8,
        y = 18
      }
    },
    {
      entity_number = 504,
      name = "transport-belt",
      position = {
        x = -8,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 505,
      name = "transport-belt",
      position = {
        x = -6,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 506,
      name = "transport-belt",
      position = {
        x = -5,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 507,
      name = "transport-belt",
      position = {
        x = -4,
        y = 19
      }
    },
    {
      direction = 2,
      entity_number = 508,
      name = "inserter",
      position = {
        x = -5,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 509,
      name = "transport-belt",
      position = {
        x = -4,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 510,
      name = "transport-belt",
      position = {
        x = -3,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 511,
      name = "transport-belt",
      position = {
        x = -2,
        y = 19
      }
    },
    {
      entity_number = 512,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 513,
      name = "transport-belt",
      position = {
        x = -2,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 514,
      name = "transport-belt",
      position = {
        x = -1,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 515,
      name = "transport-belt",
      position = {
        x = 0,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 516,
      name = "inserter",
      position = {
        x = -1,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 517,
      name = "transport-belt",
      position = {
        x = 1,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 518,
      name = "transport-belt",
      position = {
        x = 2,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 519,
      name = "transport-belt",
      position = {
        x = 3,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 520,
      name = "transport-belt",
      position = {
        x = 4,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 521,
      name = "transport-belt",
      position = {
        x = 5,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 522,
      name = "transport-belt",
      position = {
        x = 6,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 523,
      name = "transport-belt",
      position = {
        x = 6,
        y = 18
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_belts-furnaces-3"
}