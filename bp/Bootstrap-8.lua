return {
  anchor = {
    x = -5,
    y = -11
  },
  entities = {
    {
      direction = 4,
      entity_number = 2,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = 8
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -3,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -2,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -1,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 1,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 3,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 2,
        y = 10
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap-8"
}