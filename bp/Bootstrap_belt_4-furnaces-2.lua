return 
{
  anchor = {
    x = 3,
    y = -25
  },
  entities = {
    {
      direction = 6,
      entity_number = 2,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -23
      }
    },
    {
      direction = 6,
      entity_number = 3,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -1,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 5,
      name = "underground-belt",
      position = {
        x = -1,
        y = -22
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 6,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -22
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 10,
      name = "underground-belt",
      position = {
        x = -1,
        y = -19
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 11,
      name = "underground-belt",
      position = {
        x = -1,
        y = -18
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 12,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "underground-belt",
      position = {
        x = -1,
        y = -15
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 17,
      name = "underground-belt",
      position = {
        x = -1,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 18,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 21,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "underground-belt",
      position = {
        x = -1,
        y = -11
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 23,
      name = "underground-belt",
      position = {
        x = -1,
        y = -10
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 24,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -11
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 27,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 28,
      name = "underground-belt",
      position = {
        x = -1,
        y = -7
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 29,
      name = "underground-belt",
      position = {
        x = -1,
        y = -6
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 30,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "underground-belt",
      position = {
        x = -1,
        y = -3
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 35,
      name = "underground-belt",
      position = {
        x = -1,
        y = -2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 36,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 39,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "underground-belt",
      position = {
        x = -1,
        y = 1
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 41,
      name = "underground-belt",
      position = {
        x = -1,
        y = 2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 42,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 44,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "underground-belt",
      position = {
        x = -1,
        y = 5
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 47,
      name = "underground-belt",
      position = {
        x = -1,
        y = 6
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 48,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 49,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 51,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 52,
      name = "underground-belt",
      position = {
        x = -1,
        y = 9
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 53,
      name = "underground-belt",
      position = {
        x = -1,
        y = 10
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 54,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "underground-belt",
      position = {
        x = -1,
        y = 13
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 59,
      name = "underground-belt",
      position = {
        x = -1,
        y = 14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 60,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 61,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = -1,
        y = 17
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 65,
      name = "underground-belt",
      position = {
        x = -1,
        y = 18
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 66,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 17
      }
    },
    {
      direction = 2,
      entity_number = 67,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 68,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 69,
      name = "long-handed-inserter",
      position = {
        x = -3,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "underground-belt",
      position = {
        x = -1,
        y = 21
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 71,
      name = "underground-belt",
      position = {
        x = -1,
        y = 22
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 72,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 21
      }
    },
    {
      direction = 2,
      entity_number = 73,
      name = "long-handed-inserter",
      position = {
        x = 1,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 74,
      name = "underground-belt",
      position = {
        x = -1,
        y = 25
      },
      type = "output"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "underground-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_4-furnaces-2"
}