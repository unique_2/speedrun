return 
{
  anchor = {
    x = -3,
    y = -26
  },
  entities = {
    {
      direction = 6,
      entity_number = 2,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 2,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -23
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "underground-belt",
      position = {
        x = 2,
        y = -22
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 7,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -22
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 9,
      name = "underground-belt",
      position = {
        x = 2,
        y = -19
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 10,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "underground-belt",
      position = {
        x = 2,
        y = -18
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 13,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 15,
      name = "underground-belt",
      position = {
        x = 2,
        y = -15
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 16,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "underground-belt",
      position = {
        x = 2,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 19,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 21,
      name = "underground-belt",
      position = {
        x = 2,
        y = -11
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 22,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 23,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "underground-belt",
      position = {
        x = 2,
        y = -10
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 25,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "underground-belt",
      position = {
        x = 2,
        y = -7
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 28,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 29,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "underground-belt",
      position = {
        x = 2,
        y = -6
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 31,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "underground-belt",
      position = {
        x = 2,
        y = -3
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 34,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 35,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "underground-belt",
      position = {
        x = 2,
        y = -2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 37,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "underground-belt",
      position = {
        x = 2,
        y = 1
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 40,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "underground-belt",
      position = {
        x = 2,
        y = 2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 43,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 44,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "underground-belt",
      position = {
        x = 2,
        y = 5
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 46,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 47,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "underground-belt",
      position = {
        x = 2,
        y = 6
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 49,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "underground-belt",
      position = {
        x = 2,
        y = 9
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 52,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "underground-belt",
      position = {
        x = 2,
        y = 10
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 55,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "underground-belt",
      position = {
        x = 2,
        y = 13
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 58,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 60,
      name = "underground-belt",
      position = {
        x = 2,
        y = 14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 61,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 63,
      name = "underground-belt",
      position = {
        x = 2,
        y = 17
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 64,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 65,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "underground-belt",
      position = {
        x = 2,
        y = 18
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 67,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 68,
      name = "long-handed-inserter",
      position = {
        x = 0,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "underground-belt",
      position = {
        x = 2,
        y = 21
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 70,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 2,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 2,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 2,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 2,
        y = 24
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "underground-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_1-furnaces-2"
}