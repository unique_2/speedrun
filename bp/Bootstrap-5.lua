return {
  anchor = {
    x = -2,
    y = 5
  },
  entities = {
    {
      direction = 6,
      entity_number = 1,
      name = "inserter",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      entity_number = 3,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = -5.5
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "inserter",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      entity_number = 8,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = -3.5
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "inserter",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      entity_number = 12,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = -1.5
      }
    },
    {
      direction = 6,
      entity_number = 13,
      name = "inserter",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      entity_number = 16,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = 0.5
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "inserter",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      entity_number = 18,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -1,
        y = 3
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      entity_number = 21,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = 2.5
      }
    },
    {
      direction = 6,
      entity_number = 23,
      name = "inserter",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = -1,
        y = 5
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -1,
        y = 4
      }
    },
    {
      entity_number = 26,
      name = "stone-furnace",
      position = {
        x = 1.5,
        y = 4.5
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap-5"
}