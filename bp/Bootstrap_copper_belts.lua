return 
{
  anchor = {
    x = -2,
    y = -12
  },
  entities = {
    {
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -6,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -6,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -5,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -4,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -3,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -2,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -1,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 0,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 1,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 2,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 3,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 3,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 2,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 5,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 5,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 4,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 7,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 6,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 7,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 6,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 9,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "splitter",
      position = {
        x = 9,
        y = -12.5
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 8,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 8,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "splitter",
      position = {
        x = 10,
        y = -13.5
      }
    },
    {
      direction = 2,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 11,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 11,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 12,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 12,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = -10
      }
    },
    {
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = -6,
        y = -12
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -6,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 35,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = -10
      }
    },
    {
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 1,
        y = -12
      }
    },
    {
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 1,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 40,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 8,
        y = -12
      }
    },
    {
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 8,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 44,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = -10
      }
    },
    {
      entity_number = 45,
      name = "underground-belt",
      position = {
        x = -6,
        y = -10
      },
      type = "output"
    },
    {
      entity_number = 46,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = -9
      }
    },
    {
      entity_number = 47,
      name = "underground-belt",
      position = {
        x = 1,
        y = -10
      },
      type = "output"
    },
    {
      entity_number = 48,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      entity_number = 49,
      name = "underground-belt",
      position = {
        x = 8,
        y = -10
      },
      type = "output"
    },
    {
      entity_number = 50,
      name = "small-electric-pole",
      position = {
        x = 8,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 51,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = -7
      }
    },
    {
      entity_number = 52,
      name = "underground-belt",
      position = {
        x = -6,
        y = -7
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 53,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = -7
      }
    },
    {
      entity_number = 55,
      name = "underground-belt",
      position = {
        x = 1,
        y = -7
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 56,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 57,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = -7
      }
    },
    {
      entity_number = 58,
      name = "underground-belt",
      position = {
        x = 8,
        y = -7
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 59,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 60,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = -4
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = -6,
        y = -6
      }
    },
    {
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = -6,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 1,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 68,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = -4
      }
    },
    {
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 8,
        y = -6
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 8,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 71,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = -4
      }
    },
    {
      entity_number = 72,
      name = "underground-belt",
      position = {
        x = -6,
        y = -4
      },
      type = "output"
    },
    {
      entity_number = 73,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = -3
      }
    },
    {
      entity_number = 74,
      name = "underground-belt",
      position = {
        x = 1,
        y = -4
      },
      type = "output"
    },
    {
      entity_number = 75,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      entity_number = 76,
      name = "underground-belt",
      position = {
        x = 8,
        y = -4
      },
      type = "output"
    },
    {
      entity_number = 77,
      name = "small-electric-pole",
      position = {
        x = 8,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 78,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = -1
      }
    },
    {
      entity_number = 79,
      name = "underground-belt",
      position = {
        x = -6,
        y = -1
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 80,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 81,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      entity_number = 82,
      name = "underground-belt",
      position = {
        x = 1,
        y = -1
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 83,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 84,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = -1
      }
    },
    {
      entity_number = 85,
      name = "underground-belt",
      position = {
        x = 8,
        y = -1
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 86,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 87,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = 2
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = -6,
        y = 0
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -6,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 90,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 1,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 94,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 95,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = 8,
        y = 0
      }
    },
    {
      entity_number = 97,
      name = "transport-belt",
      position = {
        x = 8,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 98,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = 2
      }
    },
    {
      entity_number = 99,
      name = "underground-belt",
      position = {
        x = -6,
        y = 2
      },
      type = "output"
    },
    {
      entity_number = 100,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = 3
      }
    },
    {
      entity_number = 101,
      name = "underground-belt",
      position = {
        x = 1,
        y = 2
      },
      type = "output"
    },
    {
      entity_number = 102,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      entity_number = 103,
      name = "underground-belt",
      position = {
        x = 8,
        y = 2
      },
      type = "output"
    },
    {
      entity_number = 104,
      name = "small-electric-pole",
      position = {
        x = 8,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 105,
      name = "electric-mining-drill",
      position = {
        x = -8,
        y = 5
      }
    },
    {
      entity_number = 106,
      name = "underground-belt",
      position = {
        x = -6,
        y = 5
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 107,
      name = "electric-mining-drill",
      position = {
        x = -4,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 108,
      name = "electric-mining-drill",
      position = {
        x = -1,
        y = 5
      }
    },
    {
      entity_number = 109,
      name = "underground-belt",
      position = {
        x = 1,
        y = 5
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 110,
      name = "electric-mining-drill",
      position = {
        x = 3,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 111,
      name = "electric-mining-drill",
      position = {
        x = 6,
        y = 5
      }
    },
    {
      entity_number = 112,
      name = "underground-belt",
      position = {
        x = 8,
        y = 5
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 113,
      name = "electric-mining-drill",
      position = {
        x = 10,
        y = 5
      }
    },
    {
      entity_number = 114,
      name = "small-electric-pole",
      position = {
        x = -10,
        y = 9
      }
    },
    {
      entity_number = 115,
      name = "small-electric-pole",
      position = {
        x = -13,
        y = 15
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_belts"
}