return 
{
  anchor = {
    x = -3,
    y = -2
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 2,
      name = "underground-belt",
      position = {
        x = 7,
        y = -18
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 3,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -15
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 7,
      name = "underground-belt",
      position = {
        x = 7,
        y = -15
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 7,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 7,
        y = -13
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "underground-belt",
      position = {
        x = 7,
        y = -12
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 13,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -9
      }
    },
    {
      entity_number = 14,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -6,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -5,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -4,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -3,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -2,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -1,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "underground-belt",
      position = {
        x = 7,
        y = -9
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 7,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -6,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "splitter",
      position = {
        x = -6.5,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -6
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 30,
      name = "electric-mining-drill",
      position = {
        x = 2,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 7,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "underground-belt",
      position = {
        x = 7,
        y = -6
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = -11,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = -10,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = -11,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "underground-belt",
      position = {
        x = -10,
        y = -4
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = -9,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = -8,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "underground-belt",
      position = {
        x = -8,
        y = -4
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = -7,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = -6,
        y = -5
      }
    },
    {
      entity_number = 44,
      name = "splitter",
      position = {
        x = -6.5,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = -5,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -5,
        y = -4
      }
    },
    {
      entity_number = 47,
      name = "underground-belt",
      position = {
        x = 0,
        y = -5
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 48,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -3
      }
    },
    {
      entity_number = 49,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = -11,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = -11,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -10,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -9,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = -8,
        y = -2
      }
    },
    {
      entity_number = 56,
      name = "small-electric-pole",
      position = {
        x = -7,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -6,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -5,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -5,
        y = -2
      }
    },
    {
      entity_number = 61,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 62,
      name = "underground-belt",
      position = {
        x = 7,
        y = -3
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 7,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = -8,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = -6,
        y = -1
      }
    },
    {
      entity_number = 66,
      name = "underground-belt",
      position = {
        x = 0,
        y = 0
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 67,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 7,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 69,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "underground-belt",
      position = {
        x = 7,
        y = 0
      },
      type = "input"
    },
    {
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 73,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 3
      }
    },
    {
      entity_number = 74,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 75,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 3
      }
    },
    {
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 78,
      name = "underground-belt",
      position = {
        x = 7,
        y = 3
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 7,
        y = 4
      }
    },
    {
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 82,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 7,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 84,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 85,
      name = "underground-belt",
      position = {
        x = 7,
        y = 6
      },
      type = "input"
    },
    {
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 88,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 9
      }
    },
    {
      entity_number = 89,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 90,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 9
      }
    },
    {
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "underground-belt",
      position = {
        x = 7,
        y = 9
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 7,
        y = 10
      }
    },
    {
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 0,
        y = 11
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = 0,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 97,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 7,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 99,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 100,
      name = "underground-belt",
      position = {
        x = 7,
        y = 12
      },
      type = "input"
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = 0,
        y = 13
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 103,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 15
      }
    },
    {
      entity_number = 104,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 105,
      name = "electric-mining-drill",
      position = {
        x = 9,
        y = 15
      }
    },
    {
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = 0,
        y = 15
      }
    },
    {
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 0,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 108,
      name = "underground-belt",
      position = {
        x = 7,
        y = 15
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = 7,
        y = 16
      }
    },
    {
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = 0,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = 1,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 2,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "transport-belt",
      position = {
        x = 3,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 5,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 6,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = 7,
        y = 17
      }
    },
    {
      entity_number = 118,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 20
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_4"
}