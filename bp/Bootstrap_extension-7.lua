return 
{
  anchor = {
    x = -7,
    y = 4
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -8
      }
    },
    {
      entity_number = 2,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -7
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 3,
      name = "assembling-machine-2",
      position = {
        x = -1,
        y = -6
      },
      recipe = "electronic-circuit"
    },
    {
      direction = 2,
      entity_number = 4,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 6,
        y = -7
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 6,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -7
      }
    },
    {
      entity_number = 9,
      name = "iron-chest",
      position = {
        x = -4,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "fast-inserter",
      position = {
        x = -3,
        y = -5
      }
    },
    {
      entity_number = 11,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -5
      }
    },
    {
      entity_number = 13,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -4
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 6,
        y = -5
      }
    },
    {
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 6,
        y = -4
      }
    },
    {
      entity_number = 16,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -4
      }
    },
    {
      entity_number = 18,
      name = "iron-chest",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "fast-inserter",
      position = {
        x = -3,
        y = -3
      }
    },
    {
      entity_number = 20,
      name = "assembling-machine-2",
      position = {
        x = -1,
        y = -2
      },
      recipe = "electronic-circuit"
    },
    {
      direction = 2,
      entity_number = 21,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -2
      }
    },
    {
      entity_number = 23,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -1
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 6,
        y = -3
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      entity_number = 26,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 6,
        y = -1
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -1
      }
    },
    {
      entity_number = 31,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 6,
        y = 1
      }
    },
    {
      entity_number = 33,
      name = "underground-belt",
      position = {
        x = 6,
        y = 2
      },
      type = "output"
    },
    {
      entity_number = 35,
      name = "underground-belt",
      position = {
        x = 6,
        y = 7
      },
      type = "input"
    },
    {
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 6,
        y = 8
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-7"
}