return 
{
  anchor = {
    x = -8,
    y = -23
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -8,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -9,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -6,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -7,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -4,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -5,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -2,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -3,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 0,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -1,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 2,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 1,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 4,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 3,
        y = -28
      }
    },
    {
      direction = 4,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 6,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 5,
        y = -28
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 6,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "splitter",
      position = {
        x = 6.5,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "underground-belt",
      position = {
        x = -4,
        y = -24
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 20,
      name = "underground-belt",
      position = {
        x = 0,
        y = -24
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 21,
      name = "underground-belt",
      position = {
        x = 1,
        y = -24
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 2,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 2,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "splitter",
      position = {
        x = 4,
        y = -23.5
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "underground-belt",
      position = {
        x = 3,
        y = -24
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 3,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 4,
        y = -25
      }
    },
    {
      entity_number = 28,
      name = "splitter",
      position = {
        x = 6.5,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "underground-belt",
      position = {
        x = 5,
        y = -24
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 5,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 6,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 7,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 8,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 8,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 35,
      name = "underground-belt",
      position = {
        x = 9,
        y = -24
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 2,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 2,
        y = -23
      }
    },
    {
      entity_number = 39,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 3,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 4,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 5,
        y = -23
      }
    },
    {
      entity_number = 43,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -22
      }
    },
    {
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 6,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 5,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 7,
        y = -22
      }
    },
    {
      direction = 6,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 8,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 8,
        y = -23
      }
    },
    {
      entity_number = 49,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -20.5
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 51,
      name = "inserter",
      position = {
        x = 4,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 6,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 5,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 5,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -20
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "inserter",
      position = {
        x = 8,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 7,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 7,
        y = -21
      }
    },
    {
      entity_number = 59,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -20.5
      }
    },
    {
      entity_number = 60,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -18.5
      }
    },
    {
      direction = 2,
      entity_number = 61,
      name = "inserter",
      position = {
        x = 4,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      entity_number = 63,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = 6,
        y = -19
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 5,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 5,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "inserter",
      position = {
        x = 8,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 68,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 7,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 7,
        y = -19
      }
    },
    {
      entity_number = 71,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -18.5
      }
    },
    {
      entity_number = 72,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -16.5
      }
    },
    {
      direction = 6,
      entity_number = 73,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 74,
      name = "inserter",
      position = {
        x = 4,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 75,
      name = "underground-belt",
      position = {
        x = 6,
        y = -16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 5,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 5,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 78,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 79,
      name = "inserter",
      position = {
        x = 8,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 7,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 7,
        y = -17
      }
    },
    {
      entity_number = 82,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -16.5
      }
    },
    {
      entity_number = 83,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -14.5
      }
    },
    {
      direction = 2,
      entity_number = 84,
      name = "inserter",
      position = {
        x = 4,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 85,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -15
      }
    },
    {
      entity_number = 86,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 87,
      name = "underground-belt",
      position = {
        x = 6,
        y = -15
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 5,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 5,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 90,
      name = "inserter",
      position = {
        x = 8,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 7,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 7,
        y = -15
      }
    },
    {
      entity_number = 94,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -14.5
      }
    },
    {
      entity_number = 95,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -12.5
      }
    },
    {
      direction = 6,
      entity_number = 96,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 97,
      name = "inserter",
      position = {
        x = 4,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 98,
      name = "underground-belt",
      position = {
        x = 6,
        y = -12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = 5,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = 5,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 101,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 102,
      name = "inserter",
      position = {
        x = 8,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = 7,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 104,
      name = "transport-belt",
      position = {
        x = 7,
        y = -13
      }
    },
    {
      entity_number = 105,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -12.5
      }
    },
    {
      entity_number = 106,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -10.5
      }
    },
    {
      direction = 2,
      entity_number = 107,
      name = "inserter",
      position = {
        x = 4,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 108,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -11
      }
    },
    {
      entity_number = 109,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 110,
      name = "underground-belt",
      position = {
        x = 6,
        y = -11
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = 5,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 5,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "inserter",
      position = {
        x = 8,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 114,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 7,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 7,
        y = -11
      }
    },
    {
      entity_number = 117,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -10.5
      }
    },
    {
      entity_number = 118,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -8.5
      }
    },
    {
      direction = 6,
      entity_number = 119,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 120,
      name = "inserter",
      position = {
        x = 4,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 121,
      name = "underground-belt",
      position = {
        x = 6,
        y = -8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 122,
      name = "transport-belt",
      position = {
        x = 5,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 5,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 124,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 125,
      name = "inserter",
      position = {
        x = 8,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 126,
      name = "transport-belt",
      position = {
        x = 7,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 127,
      name = "transport-belt",
      position = {
        x = 7,
        y = -9
      }
    },
    {
      entity_number = 128,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -8.5
      }
    },
    {
      entity_number = 129,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -6.5
      }
    },
    {
      direction = 2,
      entity_number = 130,
      name = "inserter",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 131,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -7
      }
    },
    {
      entity_number = 132,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 133,
      name = "underground-belt",
      position = {
        x = 6,
        y = -7
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 134,
      name = "transport-belt",
      position = {
        x = 5,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = 5,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 136,
      name = "inserter",
      position = {
        x = 8,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 137,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 138,
      name = "transport-belt",
      position = {
        x = 7,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 139,
      name = "transport-belt",
      position = {
        x = 7,
        y = -7
      }
    },
    {
      entity_number = 140,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -6.5
      }
    },
    {
      entity_number = 141,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -4.5
      }
    },
    {
      direction = 6,
      entity_number = 142,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 143,
      name = "inserter",
      position = {
        x = 4,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 144,
      name = "underground-belt",
      position = {
        x = 6,
        y = -4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 145,
      name = "transport-belt",
      position = {
        x = 5,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 146,
      name = "transport-belt",
      position = {
        x = 5,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 147,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 148,
      name = "inserter",
      position = {
        x = 8,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = 7,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 150,
      name = "transport-belt",
      position = {
        x = 7,
        y = -5
      }
    },
    {
      entity_number = 151,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -4.5
      }
    },
    {
      entity_number = 152,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -2.5
      }
    },
    {
      direction = 2,
      entity_number = 153,
      name = "inserter",
      position = {
        x = 4,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 154,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = -3
      }
    },
    {
      entity_number = 155,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 156,
      name = "underground-belt",
      position = {
        x = 6,
        y = -3
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 157,
      name = "transport-belt",
      position = {
        x = 5,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 158,
      name = "transport-belt",
      position = {
        x = 5,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 159,
      name = "inserter",
      position = {
        x = 8,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 160,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = 7,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 162,
      name = "transport-belt",
      position = {
        x = 7,
        y = -3
      }
    },
    {
      entity_number = 163,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -2.5
      }
    },
    {
      entity_number = 164,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -0.5
      }
    },
    {
      direction = 6,
      entity_number = 165,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 166,
      name = "inserter",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 167,
      name = "underground-belt",
      position = {
        x = 6,
        y = 0
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 168,
      name = "transport-belt",
      position = {
        x = 5,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 169,
      name = "transport-belt",
      position = {
        x = 5,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 170,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 171,
      name = "inserter",
      position = {
        x = 8,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 172,
      name = "transport-belt",
      position = {
        x = 7,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 173,
      name = "transport-belt",
      position = {
        x = 7,
        y = -1
      }
    },
    {
      entity_number = 174,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = -0.5
      }
    },
    {
      entity_number = 175,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 1.5
      }
    },
    {
      direction = 2,
      entity_number = 176,
      name = "inserter",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 177,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 1
      }
    },
    {
      entity_number = 178,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 179,
      name = "underground-belt",
      position = {
        x = 6,
        y = 1
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 180,
      name = "transport-belt",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 181,
      name = "transport-belt",
      position = {
        x = 5,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 182,
      name = "inserter",
      position = {
        x = 8,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 183,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 184,
      name = "transport-belt",
      position = {
        x = 7,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 185,
      name = "transport-belt",
      position = {
        x = 7,
        y = 1
      }
    },
    {
      entity_number = 186,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 1.5
      }
    },
    {
      entity_number = 187,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 3.5
      }
    },
    {
      direction = 6,
      entity_number = 188,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 189,
      name = "inserter",
      position = {
        x = 4,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 190,
      name = "underground-belt",
      position = {
        x = 6,
        y = 4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 191,
      name = "transport-belt",
      position = {
        x = 5,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 192,
      name = "transport-belt",
      position = {
        x = 5,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 193,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 194,
      name = "inserter",
      position = {
        x = 8,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 195,
      name = "transport-belt",
      position = {
        x = 7,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 196,
      name = "transport-belt",
      position = {
        x = 7,
        y = 3
      }
    },
    {
      entity_number = 197,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 3.5
      }
    },
    {
      entity_number = 198,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 5.5
      }
    },
    {
      direction = 2,
      entity_number = 199,
      name = "inserter",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 200,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      entity_number = 201,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 202,
      name = "underground-belt",
      position = {
        x = 6,
        y = 5
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 203,
      name = "transport-belt",
      position = {
        x = 5,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 204,
      name = "transport-belt",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 205,
      name = "inserter",
      position = {
        x = 8,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 206,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 207,
      name = "transport-belt",
      position = {
        x = 7,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 208,
      name = "transport-belt",
      position = {
        x = 7,
        y = 5
      }
    },
    {
      entity_number = 209,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 5.5
      }
    },
    {
      entity_number = 210,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 7.5
      }
    },
    {
      direction = 6,
      entity_number = 211,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 212,
      name = "inserter",
      position = {
        x = 4,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 213,
      name = "underground-belt",
      position = {
        x = 6,
        y = 8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 214,
      name = "transport-belt",
      position = {
        x = 5,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 215,
      name = "transport-belt",
      position = {
        x = 5,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 216,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 217,
      name = "inserter",
      position = {
        x = 8,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 218,
      name = "transport-belt",
      position = {
        x = 7,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 219,
      name = "transport-belt",
      position = {
        x = 7,
        y = 7
      }
    },
    {
      entity_number = 220,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 7.5
      }
    },
    {
      entity_number = 221,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 9.5
      }
    },
    {
      direction = 2,
      entity_number = 222,
      name = "inserter",
      position = {
        x = 4,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 223,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      entity_number = 224,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 225,
      name = "underground-belt",
      position = {
        x = 6,
        y = 9
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 226,
      name = "transport-belt",
      position = {
        x = 5,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 227,
      name = "transport-belt",
      position = {
        x = 5,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 228,
      name = "inserter",
      position = {
        x = 8,
        y = 10
      }
    },
    {
      direction = 2,
      entity_number = 229,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 230,
      name = "transport-belt",
      position = {
        x = 7,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 231,
      name = "transport-belt",
      position = {
        x = 7,
        y = 9
      }
    },
    {
      entity_number = 232,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 9.5
      }
    },
    {
      entity_number = 233,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 11.5
      }
    },
    {
      direction = 6,
      entity_number = 234,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 235,
      name = "inserter",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 236,
      name = "underground-belt",
      position = {
        x = 6,
        y = 12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 237,
      name = "transport-belt",
      position = {
        x = 5,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 238,
      name = "transport-belt",
      position = {
        x = 5,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 239,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 240,
      name = "inserter",
      position = {
        x = 8,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 241,
      name = "transport-belt",
      position = {
        x = 7,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 242,
      name = "transport-belt",
      position = {
        x = 7,
        y = 11
      }
    },
    {
      entity_number = 243,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 11.5
      }
    },
    {
      entity_number = 244,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 13.5
      }
    },
    {
      direction = 2,
      entity_number = 245,
      name = "inserter",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 246,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      entity_number = 247,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 248,
      name = "underground-belt",
      position = {
        x = 6,
        y = 13
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 249,
      name = "transport-belt",
      position = {
        x = 5,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 250,
      name = "transport-belt",
      position = {
        x = 5,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 251,
      name = "inserter",
      position = {
        x = 8,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 252,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 253,
      name = "transport-belt",
      position = {
        x = 7,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 254,
      name = "transport-belt",
      position = {
        x = 7,
        y = 13
      }
    },
    {
      entity_number = 255,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 13.5
      }
    },
    {
      entity_number = 256,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 15.5
      }
    },
    {
      direction = 6,
      entity_number = 257,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 258,
      name = "inserter",
      position = {
        x = 4,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 259,
      name = "underground-belt",
      position = {
        x = 6,
        y = 16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 260,
      name = "transport-belt",
      position = {
        x = 5,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 261,
      name = "transport-belt",
      position = {
        x = 5,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 262,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 16
      }
    },
    {
      direction = 6,
      entity_number = 263,
      name = "inserter",
      position = {
        x = 8,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 264,
      name = "transport-belt",
      position = {
        x = 7,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 265,
      name = "transport-belt",
      position = {
        x = 7,
        y = 15
      }
    },
    {
      entity_number = 266,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 15.5
      }
    },
    {
      entity_number = 267,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 17.5
      }
    },
    {
      direction = 2,
      entity_number = 268,
      name = "inserter",
      position = {
        x = 4,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 269,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      entity_number = 270,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 271,
      name = "underground-belt",
      position = {
        x = 6,
        y = 17
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 272,
      name = "transport-belt",
      position = {
        x = 5,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 273,
      name = "transport-belt",
      position = {
        x = 5,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 274,
      name = "inserter",
      position = {
        x = 8,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 275,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 276,
      name = "transport-belt",
      position = {
        x = 7,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 277,
      name = "transport-belt",
      position = {
        x = 7,
        y = 17
      }
    },
    {
      entity_number = 278,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 17.5
      }
    },
    {
      entity_number = 279,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 19.5
      }
    },
    {
      direction = 6,
      entity_number = 280,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 20
      }
    },
    {
      direction = 2,
      entity_number = 281,
      name = "inserter",
      position = {
        x = 4,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 282,
      name = "underground-belt",
      position = {
        x = 6,
        y = 20
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 283,
      name = "transport-belt",
      position = {
        x = 5,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 284,
      name = "transport-belt",
      position = {
        x = 5,
        y = 19
      }
    },
    {
      direction = 2,
      entity_number = 285,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 20
      }
    },
    {
      direction = 6,
      entity_number = 286,
      name = "inserter",
      position = {
        x = 8,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 287,
      name = "transport-belt",
      position = {
        x = 7,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 288,
      name = "transport-belt",
      position = {
        x = 7,
        y = 19
      }
    },
    {
      entity_number = 289,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 19.5
      }
    },
    {
      entity_number = 290,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 21.5
      }
    },
    {
      direction = 2,
      entity_number = 291,
      name = "inserter",
      position = {
        x = 4,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 292,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 21
      }
    },
    {
      entity_number = 293,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 294,
      name = "underground-belt",
      position = {
        x = 6,
        y = 21
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 295,
      name = "transport-belt",
      position = {
        x = 5,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 296,
      name = "transport-belt",
      position = {
        x = 5,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 297,
      name = "inserter",
      position = {
        x = 8,
        y = 22
      }
    },
    {
      direction = 2,
      entity_number = 298,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 299,
      name = "transport-belt",
      position = {
        x = 7,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 300,
      name = "transport-belt",
      position = {
        x = 7,
        y = 21
      }
    },
    {
      entity_number = 301,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 21.5
      }
    },
    {
      entity_number = 302,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 23.5
      }
    },
    {
      direction = 6,
      entity_number = 303,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 304,
      name = "inserter",
      position = {
        x = 4,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 305,
      name = "underground-belt",
      position = {
        x = 6,
        y = 24
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 306,
      name = "transport-belt",
      position = {
        x = 5,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 307,
      name = "transport-belt",
      position = {
        x = 5,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 308,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 24
      }
    },
    {
      direction = 6,
      entity_number = 309,
      name = "inserter",
      position = {
        x = 8,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 310,
      name = "transport-belt",
      position = {
        x = 7,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 311,
      name = "transport-belt",
      position = {
        x = 7,
        y = 23
      }
    },
    {
      entity_number = 312,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 23.5
      }
    },
    {
      entity_number = 313,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 25.5
      }
    },
    {
      direction = 2,
      entity_number = 314,
      name = "inserter",
      position = {
        x = 4,
        y = 26
      }
    },
    {
      direction = 6,
      entity_number = 315,
      name = "long-handed-inserter",
      position = {
        x = 4,
        y = 25
      }
    },
    {
      entity_number = 316,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 317,
      name = "underground-belt",
      position = {
        x = 6,
        y = 25
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 318,
      name = "transport-belt",
      position = {
        x = 5,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 319,
      name = "transport-belt",
      position = {
        x = 5,
        y = 25
      }
    },
    {
      direction = 6,
      entity_number = 320,
      name = "inserter",
      position = {
        x = 8,
        y = 26
      }
    },
    {
      direction = 2,
      entity_number = 321,
      name = "long-handed-inserter",
      position = {
        x = 8,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 322,
      name = "transport-belt",
      position = {
        x = 7,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 323,
      name = "transport-belt",
      position = {
        x = 7,
        y = 25
      }
    },
    {
      entity_number = 324,
      name = "stone-furnace",
      position = {
        x = 9.5,
        y = 25.5
      }
    },
    {
      direction = 4,
      entity_number = 325,
      name = "underground-belt",
      position = {
        x = 6,
        y = 28
      },
      type = "output"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_5-furnaces-2"
}