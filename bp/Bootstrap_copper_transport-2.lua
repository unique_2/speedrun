return 
{
  anchor = {
    x = 28,
    y = -21
  },
  entities = {
    {
      direction = 4,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 27,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 27,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "underground-belt",
      position = {
        x = 30,
        y = -21
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 27,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 28,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 27,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 28,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 29,
        y = -20
      }
    },
    {
      direction = 6,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 30,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 27,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 28,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 27,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 28,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 27,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 28,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 27,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 28,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 27,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 28,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 27,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 28,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 27,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 28,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 27,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 28,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 27,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 28,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 27,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 28,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 27,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 28,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 27,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 28,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 27,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 28,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 27,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 28,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 27,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 28,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 27,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 28,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 43,
      name = "underground-belt",
      position = {
        x = -30,
        y = -1
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 44,
      name = "underground-belt",
      position = {
        x = -25,
        y = -1
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = -24,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -23,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 47,
      name = "underground-belt",
      position = {
        x = -22,
        y = -1
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 48,
      name = "underground-belt",
      position = {
        x = -17,
        y = -1
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -16,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 27,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 28,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 27,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 28,
        y = -1
      }
    },
    {
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -16,
        y = 0
      }
    },
    {
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = -16,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 27,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 28,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 27,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 28,
        y = 1
      }
    },
    {
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = -16,
        y = 2
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = -16,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 27,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 28,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = 27,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 28,
        y = 3
      }
    },
    {
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = -16,
        y = 4
      }
    },
    {
      entity_number = 67,
      name = "underground-belt",
      position = {
        x = -16,
        y = 5
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 27,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 28,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 27,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 28,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 27,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 28,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 27,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 28,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 16,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 17,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 18,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 19,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 20,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 81,
      name = "underground-belt",
      position = {
        x = 21,
        y = 9
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 82,
      name = "underground-belt",
      position = {
        x = 26,
        y = 9
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 27,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 28,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = 27,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = 28,
        y = 9
      }
    },
    {
      entity_number = 87,
      name = "underground-belt",
      position = {
        x = -16,
        y = 10
      },
      type = "input"
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = -16,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 16,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = 16,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 17,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 18,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 17,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 19,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 20,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 96,
      name = "underground-belt",
      position = {
        x = 21,
        y = 10
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 97,
      name = "underground-belt",
      position = {
        x = 26,
        y = 10
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 27,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = 28,
        y = 10
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = -16,
        y = 12
      }
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = -16,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 102,
      name = "splitter",
      position = {
        x = -15,
        y = 13.5
      }
    },
    {
      direction = 6,
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = -14,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 104,
      name = "transport-belt",
      position = {
        x = -13,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = -12,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = -11,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = -10,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = -9,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = -8,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = -7,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = -6,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = -5,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "transport-belt",
      position = {
        x = -4,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = -3,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = -2,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = -1,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = 0,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = 1,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = 2,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 120,
      name = "transport-belt",
      position = {
        x = 3,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 121,
      name = "transport-belt",
      position = {
        x = 4,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 122,
      name = "transport-belt",
      position = {
        x = 5,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 6,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 7,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 125,
      name = "transport-belt",
      position = {
        x = 8,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 126,
      name = "transport-belt",
      position = {
        x = 9,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 127,
      name = "transport-belt",
      position = {
        x = 10,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 128,
      name = "transport-belt",
      position = {
        x = 11,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 129,
      name = "transport-belt",
      position = {
        x = 12,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 130,
      name = "transport-belt",
      position = {
        x = 13,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 131,
      name = "transport-belt",
      position = {
        x = 14,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 132,
      name = "transport-belt",
      position = {
        x = 16,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 133,
      name = "transport-belt",
      position = {
        x = 15,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 134,
      name = "transport-belt",
      position = {
        x = 16,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = 17,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 136,
      name = "transport-belt",
      position = {
        x = 17,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 137,
      name = "transport-belt",
      position = {
        x = -16,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 138,
      name = "transport-belt",
      position = {
        x = -16,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 139,
      name = "transport-belt",
      position = {
        x = -14,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 140,
      name = "transport-belt",
      position = {
        x = -13,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = -12,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 142,
      name = "transport-belt",
      position = {
        x = -11,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 143,
      name = "transport-belt",
      position = {
        x = -10,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 144,
      name = "transport-belt",
      position = {
        x = -9,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 145,
      name = "transport-belt",
      position = {
        x = -8,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 146,
      name = "transport-belt",
      position = {
        x = -7,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 147,
      name = "transport-belt",
      position = {
        x = -6,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 148,
      name = "transport-belt",
      position = {
        x = -5,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = -4,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 150,
      name = "transport-belt",
      position = {
        x = -3,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 151,
      name = "transport-belt",
      position = {
        x = -2,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 152,
      name = "transport-belt",
      position = {
        x = -1,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 153,
      name = "transport-belt",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 154,
      name = "transport-belt",
      position = {
        x = 1,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 155,
      name = "transport-belt",
      position = {
        x = 2,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 156,
      name = "transport-belt",
      position = {
        x = 3,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 157,
      name = "transport-belt",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 158,
      name = "transport-belt",
      position = {
        x = 5,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 159,
      name = "transport-belt",
      position = {
        x = 6,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 160,
      name = "transport-belt",
      position = {
        x = 7,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = 8,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 162,
      name = "transport-belt",
      position = {
        x = 9,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 163,
      name = "transport-belt",
      position = {
        x = 10,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 164,
      name = "transport-belt",
      position = {
        x = 11,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 165,
      name = "transport-belt",
      position = {
        x = 12,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 166,
      name = "transport-belt",
      position = {
        x = 13,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 167,
      name = "transport-belt",
      position = {
        x = 14,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 168,
      name = "transport-belt",
      position = {
        x = 15,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 169,
      name = "transport-belt",
      position = {
        x = 16,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 170,
      name = "transport-belt",
      position = {
        x = 17,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 171,
      name = "transport-belt",
      position = {
        x = -16,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 172,
      name = "transport-belt",
      position = {
        x = -16,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 173,
      name = "transport-belt",
      position = {
        x = -18,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 174,
      name = "transport-belt",
      position = {
        x = -16,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 175,
      name = "transport-belt",
      position = {
        x = -17,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 176,
      name = "transport-belt",
      position = {
        x = -16,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 177,
      name = "transport-belt",
      position = {
        x = -18,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 178,
      name = "transport-belt",
      position = {
        x = -18,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 179,
      name = "splitter",
      position = {
        x = -30,
        y = 22.5
      }
    },
    {
      direction = 6,
      entity_number = 180,
      name = "underground-belt",
      position = {
        x = -29,
        y = 22
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 181,
      name = "underground-belt",
      position = {
        x = -24,
        y = 22
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 182,
      name = "transport-belt",
      position = {
        x = -23,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 183,
      name = "transport-belt",
      position = {
        x = -22,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 184,
      name = "transport-belt",
      position = {
        x = -21,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 185,
      name = "transport-belt",
      position = {
        x = -20,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 186,
      name = "transport-belt",
      position = {
        x = -19,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 187,
      name = "transport-belt",
      position = {
        x = -18,
        y = 22
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_transport-2"
}