return 
{
  anchor = {
    x = -6,
    y = 11
  },
  entities = {
    {
      entity_number = 1,
      name = "assembling-machine-1",
      position = {
        x = 3,
        y = -11
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -5,
        y = -10
      }
    },
    {
      entity_number = 3,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -9.5
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "inserter",
      position = {
        x = -4,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "inserter",
      position = {
        x = 5,
        y = -11
      }
    },
    {
      bar = 1,
      entity_number = 6,
      name = "iron-chest",
      position = {
        x = 6,
        y = -11
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -5,
        y = -8
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -5,
        y = -9
      }
    },
    {
      entity_number = 9,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -7.5
      }
    },
    {
      entity_number = 10,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -9
      }
    },
    {
      entity_number = 11,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      entity_number = 12,
      name = "assembling-machine-1",
      position = {
        x = 3,
        y = -8
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 6,
      entity_number = 13,
      name = "inserter",
      position = {
        x = 5,
        y = -8
      }
    },
    {
      entity_number = 14,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -9
      }
    },
    {
      bar = 1,
      entity_number = 15,
      name = "iron-chest",
      position = {
        x = 6,
        y = -8
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -5,
        y = -6
      }
    },
    {
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -5,
        y = -7
      }
    },
    {
      entity_number = 18,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -5.5
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "inserter",
      position = {
        x = -4,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "inserter",
      position = {
        x = -4,
        y = -7
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -5,
        y = -4
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = -5,
        y = -5
      }
    },
    {
      entity_number = 23,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -3.5
      }
    },
    {
      entity_number = 24,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -4
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -5,
        y = -2
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -5,
        y = -3
      }
    },
    {
      entity_number = 27,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -1.5
      }
    },
    {
      direction = 6,
      entity_number = 28,
      name = "inserter",
      position = {
        x = -4,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 29,
      name = "inserter",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = -5,
        y = 0
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -5,
        y = -1
      }
    },
    {
      entity_number = 32,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 0.5
      }
    },
    {
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = -5,
        y = 2
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -5,
        y = 1
      }
    },
    {
      entity_number = 35,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 2.5
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "inserter",
      position = {
        x = -4,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 37,
      name = "inserter",
      position = {
        x = -4,
        y = 1
      }
    },
    {
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = -5,
        y = 4
      }
    },
    {
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = -5,
        y = 3
      }
    },
    {
      entity_number = 40,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 4.5
      }
    },
    {
      entity_number = 41,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = -5,
        y = 6
      }
    },
    {
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = -5,
        y = 5
      }
    },
    {
      entity_number = 44,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 6.5
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "inserter",
      position = {
        x = -4,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 46,
      name = "inserter",
      position = {
        x = -4,
        y = 5
      }
    },
    {
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -5,
        y = 8
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -5,
        y = 7
      }
    },
    {
      entity_number = 49,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 8.5
      }
    },
    {
      entity_number = 50,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 8
      }
    },
    {
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = -5,
        y = 10
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = -5,
        y = 9
      }
    },
    {
      entity_number = 53,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 10.5
      }
    },
    {
      direction = 6,
      entity_number = 54,
      name = "inserter",
      position = {
        x = -4,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 55,
      name = "inserter",
      position = {
        x = -4,
        y = 9
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -5,
        y = 11
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap-7"
}