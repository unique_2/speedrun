return 
{
  anchor = {
    x = 2,
    y = -9
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -4,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -2,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -3,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -1,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 2,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "splitter",
      position = {
        x = 4,
        y = -9.5
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 3,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -7
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -4,
        y = -9
      }
    },
    {
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -4,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 13,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 16,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 3,
        y = -9
      }
    },
    {
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 3,
        y = -8
      }
    },
    {
      entity_number = 19,
      name = "underground-belt",
      position = {
        x = -4,
        y = -7
      },
      type = "output"
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -6
      }
    },
    {
      entity_number = 21,
      name = "underground-belt",
      position = {
        x = 3,
        y = -7
      },
      type = "output"
    },
    {
      entity_number = 22,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -4
      }
    },
    {
      entity_number = 24,
      name = "underground-belt",
      position = {
        x = -4,
        y = -4
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 25,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 27,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -4
      }
    },
    {
      entity_number = 28,
      name = "underground-belt",
      position = {
        x = 3,
        y = -4
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 29,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -1
      }
    },
    {
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -4,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 33,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 34,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = -1
      }
    },
    {
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 3,
        y = -3
      }
    },
    {
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 3,
        y = -2
      }
    },
    {
      entity_number = 37,
      name = "underground-belt",
      position = {
        x = -4,
        y = -1
      },
      type = "output"
    },
    {
      entity_number = 38,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 0
      }
    },
    {
      entity_number = 39,
      name = "underground-belt",
      position = {
        x = 3,
        y = -1
      },
      type = "output"
    },
    {
      entity_number = 40,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 2
      }
    },
    {
      entity_number = 42,
      name = "underground-belt",
      position = {
        x = -4,
        y = 2
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 43,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 44,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      entity_number = 46,
      name = "underground-belt",
      position = {
        x = 3,
        y = 2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 47,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 5
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -4,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 51,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 52,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 3,
        y = 4
      }
    },
    {
      entity_number = 55,
      name = "underground-belt",
      position = {
        x = -4,
        y = 5
      },
      type = "output"
    },
    {
      entity_number = 56,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 6
      }
    },
    {
      entity_number = 57,
      name = "underground-belt",
      position = {
        x = 3,
        y = 5
      },
      type = "output"
    },
    {
      entity_number = 58,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 59,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 8
      }
    },
    {
      entity_number = 60,
      name = "underground-belt",
      position = {
        x = -4,
        y = 8
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 61,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 62,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 8
      }
    },
    {
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = 3,
        y = 8
      },
      type = "input"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_belts-2"
}