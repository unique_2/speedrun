return 
{
  anchor = {
    x = -3,
    y = -1
  },
  entities = {
    {
      direction = 4,
      entity_number = 1,
      name = "burner-mining-drill",
      position = {
        x = -5.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 2,
      name = "burner-mining-drill",
      position = {
        x = -3.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 3,
      name = "burner-mining-drill",
      position = {
        x = -1.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "burner-mining-drill",
      position = {
        x = 0.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 5,
      name = "burner-mining-drill",
      position = {
        x = 2.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "burner-mining-drill",
      position = {
        x = 4.5,
        y = -4.5
      }
    },
    {
      entity_number = 7,
      name = "stone-furnace",
      position = {
        x = -5.5,
        y = -2.5
      }
    },
    {
      entity_number = 8,
      name = "stone-furnace",
      position = {
        x = -3.5,
        y = -2.5
      }
    },
    {
      entity_number = 9,
      name = "stone-furnace",
      position = {
        x = -1.5,
        y = -2.5
      }
    },
    {
      entity_number = 10,
      name = "stone-furnace",
      position = {
        x = 0.5,
        y = -2.5
      }
    },
    {
      entity_number = 11,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -2.5
      }
    },
    {
      entity_number = 12,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -2.5
      }
    },
    {
      entity_number = 13,
      name = "inserter",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      entity_number = 14,
      name = "inserter",
      position = {
        x = -5,
        y = -1
      }
    },
    {
      entity_number = 15,
      name = "assembling-machine-1",
      position = {
        x = -4,
        y = 1
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 16,
      name = "assembling-machine-1",
      position = {
        x = -1,
        y = 1
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 18,
      name = "inserter",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      entity_number = 19,
      name = "inserter",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = -1
      }
    },
    {
      entity_number = 21,
      name = "assembling-machine-1",
      position = {
        x = 3,
        y = 1
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 22,
      name = "inserter",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      entity_number = 23,
      name = "inserter",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      entity_number = 24,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = 1
      }
    },
    {
      entity_number = 25,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 1
      }
    },
    {
      entity_number = 26,
      name = "iron-chest",
      position = {
        x = -4,
        y = 4
      }
    },
    {
      entity_number = 27,
      name = "inserter",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      entity_number = 28,
      name = "iron-chest",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      entity_number = 29,
      name = "inserter",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      entity_number = 30,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      entity_number = 31,
      name = "inserter",
      position = {
        x = 4,
        y = 3
      }
    },
    {
      entity_number = 32,
      name = "iron-chest",
      position = {
        x = 4,
        y = 4
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-1",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "burner-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap-copper-3"
}