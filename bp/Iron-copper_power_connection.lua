return 
{
  anchor = {
    x = 11,
    y = -7
  },
  entities = {
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = -12,
        y = 7
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = 6
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 7
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 6
      }
    },
    {
      entity_number = 6,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 7
      }
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 6
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "small-electric-pole",
        type = "item"
      }
    }
  },
  name = "Iron-copper_power_connection"
}