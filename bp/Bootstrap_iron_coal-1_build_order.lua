return {
  ["-10.5_-90.5"] = 1,
  ["-10.5_-91.5"] = 1,
  ["-10.5_-92.5"] = 1,
  ["-10.5_-93.5"] = 1,
  ["-12.5_-88.5"] = 2,
  ["-12.5_-90.5"] = 2,
  ["-12.5_-92.5"] = 3,
  ["-12.5_-97.5"] = 2,
  ["-12.5_-98.5"] = 1,
  ["-13.5_-90.5"] = 1,
  ["-13.5_-97.5"] = 2,
  ["-14.5_-97.5"] = 2,
  ["-15.5_-88.5"] = 2,
  ["-15.5_-90.5"] = 2,
  ["-15.5_-92.5"] = 3,
  ["-15.5_-95.5"] = 4,
  ["-15.5_-97.5"] = 2,
  ["-15.5_-99.5"] = 3,
  ["-16.5_-90.5"] = 2,
  ["-16.5_-97.5"] = 2,
  ["-17.5_-90.5"] = 2,
  ["-17.5_-97.5"] = 2,
  ["-18.5_-88.5"] = 2,
  ["-18.5_-90.5"] = 2,
  ["-18.5_-92.5"] = 2,
  ["-18.5_-95.5"] = 2,
  ["-18.5_-97.5"] = 2,
  ["-18.5_-99.5"] = 2,
  ["-19.5_-90.5"] = 1,
  ["-19.5_-97.5"] = 1,
  ["-21.5_-88.5"] = 2,
  ["-21.5_-90.5"] = 2,
  ["-21.5_-92.5"] = 2,
  ["-21.5_-95.5"] = 2,
  ["-21.5_-97.5"] = 2,
  ["-21.5_-99.5"] = 2,
  default_stage = 5
}