return {
  anchor = {
    x = -1,
    y = -4
  },
  entities = {
    {
      entity_number = 1,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -10.5
      }
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      entity_number = 4,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -8.5
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "inserter",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "inserter",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      entity_number = 9,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -6.5
      }
    },
    {
      entity_number = 10,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      entity_number = 13,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -4.5
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "inserter",
      position = {
        x = 1,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "inserter",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -2
      }
    },
    {
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      entity_number = 20,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -2.5
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      entity_number = 23,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = -0.5
      }
    },
    {
      direction = 6,
      entity_number = 24,
      name = "inserter",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 25,
      name = "inserter",
      position = {
        x = 1,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 1
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      entity_number = 29,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 1.5
      }
    },
    {
      entity_number = 30,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 4
      }
    },
    {
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      entity_number = 34,
      name = "stone-furnace",
      position = {
        x = 2.5,
        y = 3.5
      }
    },
    {
      direction = 6,
      entity_number = 35,
      name = "inserter",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "inserter",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      entity_number = 39,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 7
      }
    },
    {
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 10
      }
    },
    {
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      entity_number = 47,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 12
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap-3"
}