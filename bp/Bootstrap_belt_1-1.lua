return {
  anchor = {
    x = 8,
    y = -3
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -4,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -3,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -2,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -1,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 0,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 1,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 2,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 3,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 4,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 5,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 6,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 7,
        y = -21
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 8,
        y = -21
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 8,
        y = -22
      }
    },
    {
      entity_number = 15,
      name = "underground-belt",
      position = {
        x = -4,
        y = -19
      },
      type = "output"
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -4,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -6,
        y = -13
      }
    },
    {
      entity_number = 18,
      name = "underground-belt",
      position = {
        x = -4,
        y = -14
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -5,
        y = -13
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -4,
        y = -13
      }
    },
    {
      entity_number = 21,
      name = "underground-belt",
      position = {
        x = -6,
        y = -12
      },
      type = "output"
    },
    {
      entity_number = 22,
      name = "underground-belt",
      position = {
        x = -6,
        y = -7
      },
      type = "input"
    },
    {
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = -6,
        y = -5
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = -6,
        y = -6
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -6,
        y = -3
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -6,
        y = -4
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = -6,
        y = -1
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = -6,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 2
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -6,
        y = 0
      }
    },
    {
      entity_number = 32,
      name = "splitter",
      position = {
        x = -4.5,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = -5,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -2,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = -3,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = -3,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 39,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 2,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 43,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 2
      }
    },
    {
      entity_number = 44,
      name = "small-electric-pole",
      position = {
        x = -5,
        y = 3
      }
    },
    {
      entity_number = 45,
      name = "underground-belt",
      position = {
        x = -5,
        y = 2
      },
      type = "output"
    },
    {
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -4,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -3,
        y = 2
      }
    },
    {
      entity_number = 48,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = 3
      }
    },
    {
      entity_number = 49,
      name = "underground-belt",
      position = {
        x = 2,
        y = 2
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 50,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 5
      }
    },
    {
      entity_number = 51,
      name = "underground-belt",
      position = {
        x = -5,
        y = 5
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 52,
      name = "electric-mining-drill",
      position = {
        x = -3,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 53,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      entity_number = 54,
      name = "underground-belt",
      position = {
        x = 2,
        y = 5
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 55,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 56,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 8
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -5,
        y = 7
      }
    },
    {
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -5,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "electric-mining-drill",
      position = {
        x = -3,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 60,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 2,
        y = 7
      }
    },
    {
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 2,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      entity_number = 64,
      name = "small-electric-pole",
      position = {
        x = -5,
        y = 9
      }
    },
    {
      entity_number = 65,
      name = "underground-belt",
      position = {
        x = -5,
        y = 8
      },
      type = "output"
    },
    {
      entity_number = 66,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = 9
      }
    },
    {
      entity_number = 67,
      name = "underground-belt",
      position = {
        x = 2,
        y = 8
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 68,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 11
      }
    },
    {
      entity_number = 69,
      name = "underground-belt",
      position = {
        x = -5,
        y = 11
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 70,
      name = "electric-mining-drill",
      position = {
        x = -3,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 71,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 11
      }
    },
    {
      entity_number = 72,
      name = "underground-belt",
      position = {
        x = 2,
        y = 11
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 73,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 74,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 14
      }
    },
    {
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = -5,
        y = 13
      }
    },
    {
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = -5,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 77,
      name = "electric-mining-drill",
      position = {
        x = -3,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 78,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 2,
        y = 13
      }
    },
    {
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 2,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 81,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = -5,
        y = 14
      }
    },
    {
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = -5,
        y = 15
      }
    },
    {
      entity_number = 84,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = 15
      }
    },
    {
      entity_number = 85,
      name = "underground-belt",
      position = {
        x = 2,
        y = 14
      },
      type = "output"
    },
    {
      entity_number = 86,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = 16
      }
    },
    {
      direction = 6,
      entity_number = 87,
      name = "electric-mining-drill",
      position = {
        x = -3,
        y = 17
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = -5,
        y = 16
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -5,
        y = 17
      }
    },
    {
      direction = 2,
      entity_number = 90,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 17
      }
    },
    {
      entity_number = 91,
      name = "underground-belt",
      position = {
        x = 2,
        y = 17
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 92,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 17
      }
    },
    {
      direction = 2,
      entity_number = 93,
      name = "electric-mining-drill",
      position = {
        x = 0,
        y = 20
      }
    },
    {
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 2,
        y = 19
      }
    },
    {
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 2,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 96,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 20
      }
    },
    {
      entity_number = 97,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = 21
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 2,
        y = 20
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_1-1"
}