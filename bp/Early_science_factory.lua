return {
  anchor = {
    x = -10,
    y = 4
  },
  entities = {
    {
      entity_number = 1,
      name = "assembling-machine-1",
      position = {
        x = 3,
        y = -3
      },
      recipe = "science-pack-1"
    },
    {
      entity_number = 2,
      name = "assembling-machine-1",
      position = {
        x = 6,
        y = -3
      },
      recipe = "science-pack-1"
    },
    {
      entity_number = 3,
      name = "assembling-machine-1",
      position = {
        x = 9,
        y = -3
      },
      recipe = "science-pack-1"
    },
    {
      entity_number = 4,
      name = "assembling-machine-1",
      position = {
        x = 3,
        y = 1
      },
      recipe = "science-pack-1"
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = -1
      }
    },
    {
      entity_number = 6,
      name = "assembling-machine-1",
      position = {
        x = 6,
        y = 1
      },
      recipe = "science-pack-1"
    },
    {
      entity_number = 7,
      name = "assembling-machine-1",
      position = {
        x = 9,
        y = 1
      },
      recipe = "science-pack-1"
    }
  },
  icons = {
    {
      index = 2,
      signal = {
        name = "assembling-machine-1",
        type = "item"
      }
    }
  },
  name = "Early_science_factory"
}