return 
{
  anchor = {
    x = 10,
    y = 3
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = -10,
        y = -6
      }
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = -6,
        y = -6
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 1
      }
    },
    {
      entity_number = 5,
      name = "assembling-machine-1",
      position = {
        x = 9,
        y = 5
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 5
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "assembling-machine-1",
        type = "item"
      }
    }
  },
  name = "Bootstrap-copper-1"
}