return 
{
  anchor = {
    x = -4,
    y = -22
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -4,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -2,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -3,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 0,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -1,
        y = -26
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 1,
        y = -26
      }
    },
    {
      direction = 4,
      entity_number = 7,
      name = "splitter",
      position = {
        x = 1.5,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -2,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -3,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -3,
        y = -24
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 0,
        y = -24
      }
    },
    {
      direction = 6,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -1,
        y = -24
      }
    },
    {
      entity_number = 13,
      name = "splitter",
      position = {
        x = 1.5,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 2,
        y = -24
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 1,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 3,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 3,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -2,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -3,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -3,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = -1,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 2,
        y = -21
      }
    },
    {
      entity_number = 25,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -21
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 3,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 3,
        y = -22
      }
    },
    {
      entity_number = 28,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -19.5
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 0,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 0,
        y = -20
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "inserter",
      position = {
        x = -1,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 2,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 2,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 1,
        y = -19
      }
    },
    {
      entity_number = 35,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -19.5
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "inserter",
      position = {
        x = 3,
        y = -20
      }
    },
    {
      entity_number = 37,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -17.5
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 0,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 0,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "inserter",
      position = {
        x = -1,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 2,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 2,
        y = -18
      }
    },
    {
      entity_number = 43,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -17
      }
    },
    {
      entity_number = 44,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -17.5
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "inserter",
      position = {
        x = 3,
        y = -17
      }
    },
    {
      entity_number = 46,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -15.5
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 0,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 0,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 49,
      name = "inserter",
      position = {
        x = -1,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 2,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 2,
        y = -16
      }
    },
    {
      entity_number = 52,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -15.5
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "inserter",
      position = {
        x = 3,
        y = -16
      }
    },
    {
      entity_number = 54,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -13.5
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 0,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 0,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 57,
      name = "inserter",
      position = {
        x = -1,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 2,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 2,
        y = -14
      }
    },
    {
      entity_number = 60,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      entity_number = 61,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -13.5
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "inserter",
      position = {
        x = 3,
        y = -13
      }
    },
    {
      entity_number = 63,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -11.5
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = 0,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 0,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 66,
      name = "inserter",
      position = {
        x = -1,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 2,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 2,
        y = -12
      }
    },
    {
      entity_number = 69,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -11.5
      }
    },
    {
      direction = 6,
      entity_number = 70,
      name = "inserter",
      position = {
        x = 3,
        y = -12
      }
    },
    {
      entity_number = 71,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -9.5
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 74,
      name = "inserter",
      position = {
        x = -1,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 2,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 2,
        y = -10
      }
    },
    {
      entity_number = 77,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      entity_number = 78,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -9.5
      }
    },
    {
      direction = 6,
      entity_number = 79,
      name = "inserter",
      position = {
        x = 3,
        y = -9
      }
    },
    {
      entity_number = 80,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -7.5
      }
    },
    {
      direction = 4,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 83,
      name = "inserter",
      position = {
        x = -1,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 2,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = 2,
        y = -8
      }
    },
    {
      entity_number = 86,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -7.5
      }
    },
    {
      direction = 6,
      entity_number = 87,
      name = "inserter",
      position = {
        x = 3,
        y = -8
      }
    },
    {
      entity_number = 88,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -5.5
      }
    },
    {
      direction = 4,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "inserter",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 2,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 2,
        y = -6
      }
    },
    {
      entity_number = 94,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -5
      }
    },
    {
      entity_number = 95,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -5.5
      }
    },
    {
      direction = 6,
      entity_number = 96,
      name = "inserter",
      position = {
        x = 3,
        y = -5
      }
    },
    {
      entity_number = 97,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -3.5
      }
    },
    {
      direction = 4,
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 100,
      name = "inserter",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = 2,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = 2,
        y = -4
      }
    },
    {
      entity_number = 103,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -3.5
      }
    },
    {
      direction = 6,
      entity_number = 104,
      name = "inserter",
      position = {
        x = 3,
        y = -4
      }
    },
    {
      entity_number = 105,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = -1.5
      }
    },
    {
      direction = 4,
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 108,
      name = "inserter",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = 2,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = 2,
        y = -2
      }
    },
    {
      entity_number = 111,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      entity_number = 112,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = -1.5
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "inserter",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      entity_number = 114,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 0.5
      }
    },
    {
      direction = 4,
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 117,
      name = "inserter",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = 2,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      entity_number = 120,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 0.5
      }
    },
    {
      direction = 6,
      entity_number = 121,
      name = "inserter",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      entity_number = 122,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 2.5
      }
    },
    {
      direction = 4,
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 125,
      name = "inserter",
      position = {
        x = -1,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 126,
      name = "transport-belt",
      position = {
        x = 2,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 127,
      name = "transport-belt",
      position = {
        x = 2,
        y = 2
      }
    },
    {
      entity_number = 128,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      entity_number = 129,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 2.5
      }
    },
    {
      direction = 6,
      entity_number = 130,
      name = "inserter",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      entity_number = 131,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 4.5
      }
    },
    {
      direction = 4,
      entity_number = 132,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 133,
      name = "transport-belt",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 134,
      name = "inserter",
      position = {
        x = -1,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = 2,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 136,
      name = "transport-belt",
      position = {
        x = 2,
        y = 4
      }
    },
    {
      entity_number = 137,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 4.5
      }
    },
    {
      direction = 6,
      entity_number = 138,
      name = "inserter",
      position = {
        x = 3,
        y = 4
      }
    },
    {
      entity_number = 139,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 6.5
      }
    },
    {
      direction = 4,
      entity_number = 140,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 142,
      name = "inserter",
      position = {
        x = -1,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 143,
      name = "transport-belt",
      position = {
        x = 2,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 144,
      name = "transport-belt",
      position = {
        x = 2,
        y = 6
      }
    },
    {
      entity_number = 145,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 7
      }
    },
    {
      entity_number = 146,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 6.5
      }
    },
    {
      direction = 6,
      entity_number = 147,
      name = "inserter",
      position = {
        x = 3,
        y = 7
      }
    },
    {
      entity_number = 148,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 8.5
      }
    },
    {
      direction = 4,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 150,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 151,
      name = "inserter",
      position = {
        x = -1,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 152,
      name = "transport-belt",
      position = {
        x = 2,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 153,
      name = "transport-belt",
      position = {
        x = 2,
        y = 8
      }
    },
    {
      entity_number = 154,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 8.5
      }
    },
    {
      direction = 6,
      entity_number = 155,
      name = "inserter",
      position = {
        x = 3,
        y = 8
      }
    },
    {
      entity_number = 156,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 10.5
      }
    },
    {
      direction = 4,
      entity_number = 157,
      name = "transport-belt",
      position = {
        x = 0,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 158,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      direction = 2,
      entity_number = 159,
      name = "inserter",
      position = {
        x = -1,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 160,
      name = "transport-belt",
      position = {
        x = 2,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = 2,
        y = 10
      }
    },
    {
      entity_number = 162,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 11
      }
    },
    {
      entity_number = 163,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 10.5
      }
    },
    {
      direction = 6,
      entity_number = 164,
      name = "inserter",
      position = {
        x = 3,
        y = 11
      }
    },
    {
      entity_number = 165,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 12.5
      }
    },
    {
      direction = 4,
      entity_number = 166,
      name = "transport-belt",
      position = {
        x = 0,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 167,
      name = "transport-belt",
      position = {
        x = 0,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 168,
      name = "inserter",
      position = {
        x = -1,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 169,
      name = "transport-belt",
      position = {
        x = 2,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 170,
      name = "transport-belt",
      position = {
        x = 2,
        y = 12
      }
    },
    {
      entity_number = 171,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 12.5
      }
    },
    {
      direction = 6,
      entity_number = 172,
      name = "inserter",
      position = {
        x = 3,
        y = 12
      }
    },
    {
      entity_number = 173,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 14.5
      }
    },
    {
      direction = 4,
      entity_number = 174,
      name = "transport-belt",
      position = {
        x = 0,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 175,
      name = "transport-belt",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 176,
      name = "inserter",
      position = {
        x = -1,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 177,
      name = "transport-belt",
      position = {
        x = 2,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 178,
      name = "transport-belt",
      position = {
        x = 2,
        y = 14
      }
    },
    {
      entity_number = 179,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 15
      }
    },
    {
      entity_number = 180,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 14.5
      }
    },
    {
      direction = 6,
      entity_number = 181,
      name = "inserter",
      position = {
        x = 3,
        y = 15
      }
    },
    {
      entity_number = 182,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 16.5
      }
    },
    {
      direction = 4,
      entity_number = 183,
      name = "transport-belt",
      position = {
        x = 0,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 184,
      name = "transport-belt",
      position = {
        x = 0,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 185,
      name = "inserter",
      position = {
        x = -1,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 186,
      name = "transport-belt",
      position = {
        x = 2,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 187,
      name = "transport-belt",
      position = {
        x = 2,
        y = 16
      }
    },
    {
      entity_number = 188,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 16.5
      }
    },
    {
      direction = 6,
      entity_number = 189,
      name = "inserter",
      position = {
        x = 3,
        y = 16
      }
    },
    {
      entity_number = 190,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 18.5
      }
    },
    {
      direction = 4,
      entity_number = 191,
      name = "transport-belt",
      position = {
        x = 0,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 192,
      name = "transport-belt",
      position = {
        x = 0,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 193,
      name = "inserter",
      position = {
        x = -1,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 194,
      name = "transport-belt",
      position = {
        x = 2,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 195,
      name = "transport-belt",
      position = {
        x = 2,
        y = 18
      }
    },
    {
      entity_number = 196,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 19
      }
    },
    {
      entity_number = 197,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 18.5
      }
    },
    {
      direction = 6,
      entity_number = 198,
      name = "inserter",
      position = {
        x = 3,
        y = 19
      }
    },
    {
      entity_number = 199,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 20.5
      }
    },
    {
      direction = 4,
      entity_number = 200,
      name = "transport-belt",
      position = {
        x = 0,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 201,
      name = "transport-belt",
      position = {
        x = 0,
        y = 20
      }
    },
    {
      direction = 2,
      entity_number = 202,
      name = "inserter",
      position = {
        x = -1,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 203,
      name = "transport-belt",
      position = {
        x = 2,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 204,
      name = "transport-belt",
      position = {
        x = 2,
        y = 20
      }
    },
    {
      entity_number = 205,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 20.5
      }
    },
    {
      direction = 6,
      entity_number = 206,
      name = "inserter",
      position = {
        x = 3,
        y = 20
      }
    },
    {
      entity_number = 207,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 22.5
      }
    },
    {
      direction = 4,
      entity_number = 208,
      name = "transport-belt",
      position = {
        x = 0,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 209,
      name = "transport-belt",
      position = {
        x = 0,
        y = 22
      }
    },
    {
      direction = 2,
      entity_number = 210,
      name = "inserter",
      position = {
        x = -1,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 211,
      name = "transport-belt",
      position = {
        x = 2,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 212,
      name = "transport-belt",
      position = {
        x = 2,
        y = 22
      }
    },
    {
      entity_number = 213,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 23
      }
    },
    {
      entity_number = 214,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 22.5
      }
    },
    {
      direction = 6,
      entity_number = 215,
      name = "inserter",
      position = {
        x = 3,
        y = 23
      }
    },
    {
      entity_number = 216,
      name = "stone-furnace",
      position = {
        x = -2.5,
        y = 24.5
      }
    },
    {
      direction = 4,
      entity_number = 217,
      name = "transport-belt",
      position = {
        x = 0,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 218,
      name = "inserter",
      position = {
        x = -1,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 219,
      name = "transport-belt",
      position = {
        x = 2,
        y = 24
      }
    },
    {
      entity_number = 220,
      name = "stone-furnace",
      position = {
        x = 4.5,
        y = 24.5
      }
    },
    {
      direction = 6,
      entity_number = 221,
      name = "inserter",
      position = {
        x = 3,
        y = 24
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_1-furnaces-1"
}