return 
{
  anchor = {
    x = -7,
    y = -8
  },
  entities = {
    {
      direction = 4,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 6,
        y = -3
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 6,
        y = -1
      }
    },
    {
      entity_number = 6,
      name = "assembling-machine-2",
      position = {
        x = -1,
        y = 2
      },
      recipe = "electronic-circuit"
    },
    {
      entity_number = 7,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = 1
      },
      recipe = "copper-cable"
    },
    {
      direction = 2,
      entity_number = 8,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "fast-inserter",
      position = {
        x = 5,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 6,
        y = 1
      }
    },
    {
      entity_number = 12,
      name = "iron-chest",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "fast-inserter",
      position = {
        x = -3,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      entity_number = 16,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = 4
      },
      recipe = "copper-cable"
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 6,
        y = 3
      }
    },
    {
      entity_number = 19,
      name = "iron-chest",
      position = {
        x = -4,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "fast-inserter",
      position = {
        x = -3,
        y = 5
      }
    },
    {
      entity_number = 21,
      name = "assembling-machine-2",
      position = {
        x = -1,
        y = 6
      },
      recipe = "electronic-circuit"
    },
    {
      entity_number = 22,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "fast-inserter",
      position = {
        x = 5,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 6,
        y = 4
      }
    },
    {
      entity_number = 26,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 6,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 28,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 6
      }
    },
    {
      entity_number = 29,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = 7
      },
      recipe = "copper-cable"
    },
    {
      direction = 2,
      entity_number = 30,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 6,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "fast-inserter",
      position = {
        x = 5,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 6,
        y = 7
      }
    },
    {
      entity_number = 34,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 9
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-6"
}