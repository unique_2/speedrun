return 
{
  anchor = {
    x = -12,
    y = -23
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -14,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -12,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -13,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -10,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -11,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -9,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -8,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -7,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -6,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -5,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -4,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -3,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -2,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -1,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 0,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 2,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 1,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 4,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 3,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 6,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 5,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 8,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 7,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 10,
        y = -29
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 9,
        y = -29
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 11,
        y = -29
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = -14,
        y = -28
      }
    },
    {
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = -14,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 11,
        y = -28
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 11,
        y = -27
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -14,
        y = -26
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 8,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 7,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 10,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 9,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "splitter",
      position = {
        x = 11.5,
        y = -26
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 12,
        y = -25
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 11,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 13,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "underground-belt",
      position = {
        x = 6,
        y = -24
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 42,
      name = "underground-belt",
      position = {
        x = 8,
        y = -24
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 7,
        y = -24
      }
    },
    {
      entity_number = 44,
      name = "small-electric-pole",
      position = {
        x = 8,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 7,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 46,
      name = "splitter",
      position = {
        x = 9,
        y = -23.5
      }
    },
    {
      direction = 2,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 10,
        y = -23
      }
    },
    {
      entity_number = 48,
      name = "splitter",
      position = {
        x = 11.5,
        y = -24
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 11,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 13,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 13,
        y = -23
      }
    },
    {
      direction = 2,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 8,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 7,
        y = -22
      }
    },
    {
      entity_number = 54,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -20.5
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 10,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 9,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 10,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 58,
      name = "inserter",
      position = {
        x = 9,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 12,
        y = -22
      }
    },
    {
      entity_number = 60,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 12,
        y = -21
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 13,
        y = -22
      }
    },
    {
      entity_number = 63,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -20.5
      }
    },
    {
      direction = 6,
      entity_number = 64,
      name = "inserter",
      position = {
        x = 13,
        y = -21
      }
    },
    {
      entity_number = 65,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -18.5
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 10,
        y = -20
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 10,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 69,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 12,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 11,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 12,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 73,
      name = "underground-belt",
      position = {
        x = 11,
        y = -19
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 74,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -20
      }
    },
    {
      entity_number = 75,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -18.5
      }
    },
    {
      direction = 2,
      entity_number = 76,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -19
      }
    },
    {
      entity_number = 77,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -16.5
      }
    },
    {
      direction = 4,
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 10,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 79,
      name = "inserter",
      position = {
        x = 9,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 10,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 81,
      name = "inserter",
      position = {
        x = 9,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 12,
        y = -18
      }
    },
    {
      entity_number = 83,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 12,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 85,
      name = "inserter",
      position = {
        x = 13,
        y = -18
      }
    },
    {
      entity_number = 86,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -16.5
      }
    },
    {
      direction = 6,
      entity_number = 87,
      name = "inserter",
      position = {
        x = 13,
        y = -17
      }
    },
    {
      entity_number = 88,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -14.5
      }
    },
    {
      direction = 4,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 10,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 90,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 10,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 92,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 12,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 94,
      name = "underground-belt",
      position = {
        x = 11,
        y = -16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 12,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 96,
      name = "underground-belt",
      position = {
        x = 11,
        y = -15
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 97,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -16
      }
    },
    {
      entity_number = 98,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -14.5
      }
    },
    {
      direction = 2,
      entity_number = 99,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -15
      }
    },
    {
      entity_number = 100,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -12.5
      }
    },
    {
      direction = 4,
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = 10,
        y = -14
      }
    },
    {
      direction = 2,
      entity_number = 102,
      name = "inserter",
      position = {
        x = 9,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = 10,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 104,
      name = "inserter",
      position = {
        x = 9,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = 12,
        y = -14
      }
    },
    {
      entity_number = 106,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 12,
        y = -13
      }
    },
    {
      direction = 6,
      entity_number = 108,
      name = "inserter",
      position = {
        x = 13,
        y = -14
      }
    },
    {
      entity_number = 109,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -12.5
      }
    },
    {
      direction = 6,
      entity_number = 110,
      name = "inserter",
      position = {
        x = 13,
        y = -13
      }
    },
    {
      entity_number = 111,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -10.5
      }
    },
    {
      direction = 4,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 10,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = 10,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 115,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 12,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 117,
      name = "underground-belt",
      position = {
        x = 11,
        y = -12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = 12,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 119,
      name = "underground-belt",
      position = {
        x = 11,
        y = -11
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 120,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -12
      }
    },
    {
      entity_number = 121,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -10.5
      }
    },
    {
      direction = 2,
      entity_number = 122,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -11
      }
    },
    {
      entity_number = 123,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -8.5
      }
    },
    {
      direction = 4,
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 10,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 125,
      name = "inserter",
      position = {
        x = 9,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 126,
      name = "transport-belt",
      position = {
        x = 10,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 127,
      name = "inserter",
      position = {
        x = 9,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 128,
      name = "transport-belt",
      position = {
        x = 12,
        y = -10
      }
    },
    {
      entity_number = 129,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 130,
      name = "transport-belt",
      position = {
        x = 12,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 131,
      name = "inserter",
      position = {
        x = 13,
        y = -10
      }
    },
    {
      entity_number = 132,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -8.5
      }
    },
    {
      direction = 6,
      entity_number = 133,
      name = "inserter",
      position = {
        x = 13,
        y = -9
      }
    },
    {
      entity_number = 134,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -6.5
      }
    },
    {
      direction = 4,
      entity_number = 135,
      name = "transport-belt",
      position = {
        x = 10,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 136,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 137,
      name = "transport-belt",
      position = {
        x = 10,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 138,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 139,
      name = "transport-belt",
      position = {
        x = 12,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 140,
      name = "underground-belt",
      position = {
        x = 11,
        y = -8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = 12,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 142,
      name = "underground-belt",
      position = {
        x = 11,
        y = -7
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 143,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -8
      }
    },
    {
      entity_number = 144,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -6.5
      }
    },
    {
      direction = 2,
      entity_number = 145,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -7
      }
    },
    {
      entity_number = 146,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -4.5
      }
    },
    {
      direction = 4,
      entity_number = 147,
      name = "transport-belt",
      position = {
        x = 10,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 148,
      name = "inserter",
      position = {
        x = 9,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 149,
      name = "transport-belt",
      position = {
        x = 10,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 150,
      name = "inserter",
      position = {
        x = 9,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 151,
      name = "transport-belt",
      position = {
        x = 12,
        y = -6
      }
    },
    {
      entity_number = 152,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 153,
      name = "transport-belt",
      position = {
        x = 12,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 154,
      name = "inserter",
      position = {
        x = 13,
        y = -6
      }
    },
    {
      entity_number = 155,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -4.5
      }
    },
    {
      direction = 6,
      entity_number = 156,
      name = "inserter",
      position = {
        x = 13,
        y = -5
      }
    },
    {
      entity_number = 157,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -2.5
      }
    },
    {
      direction = 4,
      entity_number = 158,
      name = "transport-belt",
      position = {
        x = 10,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 159,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 160,
      name = "transport-belt",
      position = {
        x = 10,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 161,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 162,
      name = "transport-belt",
      position = {
        x = 12,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 163,
      name = "underground-belt",
      position = {
        x = 11,
        y = -4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 164,
      name = "transport-belt",
      position = {
        x = 12,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 165,
      name = "underground-belt",
      position = {
        x = 11,
        y = -3
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 166,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -4
      }
    },
    {
      entity_number = 167,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -2.5
      }
    },
    {
      direction = 2,
      entity_number = 168,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = -3
      }
    },
    {
      entity_number = 169,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = -0.5
      }
    },
    {
      direction = 4,
      entity_number = 170,
      name = "transport-belt",
      position = {
        x = 10,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 171,
      name = "inserter",
      position = {
        x = 9,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 172,
      name = "transport-belt",
      position = {
        x = 10,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 173,
      name = "inserter",
      position = {
        x = 9,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 174,
      name = "transport-belt",
      position = {
        x = 12,
        y = -2
      }
    },
    {
      entity_number = 175,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 176,
      name = "transport-belt",
      position = {
        x = 12,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 177,
      name = "inserter",
      position = {
        x = 13,
        y = -2
      }
    },
    {
      entity_number = 178,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = -0.5
      }
    },
    {
      direction = 6,
      entity_number = 179,
      name = "inserter",
      position = {
        x = 13,
        y = -1
      }
    },
    {
      entity_number = 180,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 1.5
      }
    },
    {
      direction = 4,
      entity_number = 181,
      name = "transport-belt",
      position = {
        x = 10,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 182,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 183,
      name = "transport-belt",
      position = {
        x = 10,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 184,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 185,
      name = "transport-belt",
      position = {
        x = 12,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 186,
      name = "underground-belt",
      position = {
        x = 11,
        y = 0
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 187,
      name = "transport-belt",
      position = {
        x = 12,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 188,
      name = "underground-belt",
      position = {
        x = 11,
        y = 1
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 189,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 0
      }
    },
    {
      entity_number = 190,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 1.5
      }
    },
    {
      direction = 2,
      entity_number = 191,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 1
      }
    },
    {
      entity_number = 192,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 3.5
      }
    },
    {
      direction = 4,
      entity_number = 193,
      name = "transport-belt",
      position = {
        x = 10,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 194,
      name = "inserter",
      position = {
        x = 9,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 195,
      name = "transport-belt",
      position = {
        x = 10,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 196,
      name = "inserter",
      position = {
        x = 9,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 197,
      name = "transport-belt",
      position = {
        x = 12,
        y = 2
      }
    },
    {
      entity_number = 198,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 199,
      name = "transport-belt",
      position = {
        x = 12,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 200,
      name = "inserter",
      position = {
        x = 13,
        y = 2
      }
    },
    {
      entity_number = 201,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 3.5
      }
    },
    {
      direction = 6,
      entity_number = 202,
      name = "inserter",
      position = {
        x = 13,
        y = 3
      }
    },
    {
      entity_number = 203,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 5.5
      }
    },
    {
      direction = 4,
      entity_number = 204,
      name = "transport-belt",
      position = {
        x = 10,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 205,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 206,
      name = "transport-belt",
      position = {
        x = 10,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 207,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 208,
      name = "transport-belt",
      position = {
        x = 12,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 209,
      name = "underground-belt",
      position = {
        x = 11,
        y = 4
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 210,
      name = "transport-belt",
      position = {
        x = 12,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 211,
      name = "underground-belt",
      position = {
        x = 11,
        y = 5
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 212,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 4
      }
    },
    {
      entity_number = 213,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 5.5
      }
    },
    {
      direction = 2,
      entity_number = 214,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 5
      }
    },
    {
      entity_number = 215,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 7.5
      }
    },
    {
      direction = 4,
      entity_number = 216,
      name = "transport-belt",
      position = {
        x = 10,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 217,
      name = "inserter",
      position = {
        x = 9,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 218,
      name = "transport-belt",
      position = {
        x = 10,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 219,
      name = "inserter",
      position = {
        x = 9,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 220,
      name = "transport-belt",
      position = {
        x = 12,
        y = 6
      }
    },
    {
      entity_number = 221,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 222,
      name = "transport-belt",
      position = {
        x = 12,
        y = 7
      }
    },
    {
      direction = 6,
      entity_number = 223,
      name = "inserter",
      position = {
        x = 13,
        y = 6
      }
    },
    {
      entity_number = 224,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 7.5
      }
    },
    {
      direction = 6,
      entity_number = 225,
      name = "inserter",
      position = {
        x = 13,
        y = 7
      }
    },
    {
      entity_number = 226,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 9.5
      }
    },
    {
      direction = 4,
      entity_number = 227,
      name = "transport-belt",
      position = {
        x = 10,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 228,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 229,
      name = "transport-belt",
      position = {
        x = 10,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 230,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 231,
      name = "transport-belt",
      position = {
        x = 12,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 232,
      name = "underground-belt",
      position = {
        x = 11,
        y = 8
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 233,
      name = "transport-belt",
      position = {
        x = 12,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 234,
      name = "underground-belt",
      position = {
        x = 11,
        y = 9
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 235,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 8
      }
    },
    {
      entity_number = 236,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 9.5
      }
    },
    {
      direction = 2,
      entity_number = 237,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 9
      }
    },
    {
      entity_number = 238,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 11.5
      }
    },
    {
      direction = 4,
      entity_number = 239,
      name = "transport-belt",
      position = {
        x = 10,
        y = 10
      }
    },
    {
      direction = 2,
      entity_number = 240,
      name = "inserter",
      position = {
        x = 9,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 241,
      name = "transport-belt",
      position = {
        x = 10,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 242,
      name = "inserter",
      position = {
        x = 9,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 243,
      name = "transport-belt",
      position = {
        x = 12,
        y = 10
      }
    },
    {
      entity_number = 244,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 245,
      name = "transport-belt",
      position = {
        x = 12,
        y = 11
      }
    },
    {
      direction = 6,
      entity_number = 246,
      name = "inserter",
      position = {
        x = 13,
        y = 10
      }
    },
    {
      entity_number = 247,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 11.5
      }
    },
    {
      direction = 6,
      entity_number = 248,
      name = "inserter",
      position = {
        x = 13,
        y = 11
      }
    },
    {
      entity_number = 249,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 13.5
      }
    },
    {
      direction = 4,
      entity_number = 250,
      name = "transport-belt",
      position = {
        x = 10,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 251,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 252,
      name = "transport-belt",
      position = {
        x = 10,
        y = 13
      }
    },
    {
      direction = 6,
      entity_number = 253,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 254,
      name = "transport-belt",
      position = {
        x = 12,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 255,
      name = "underground-belt",
      position = {
        x = 11,
        y = 12
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 256,
      name = "transport-belt",
      position = {
        x = 12,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 257,
      name = "underground-belt",
      position = {
        x = 11,
        y = 13
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 258,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 12
      }
    },
    {
      entity_number = 259,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 13.5
      }
    },
    {
      direction = 2,
      entity_number = 260,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 13
      }
    },
    {
      entity_number = 261,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 15.5
      }
    },
    {
      direction = 4,
      entity_number = 262,
      name = "transport-belt",
      position = {
        x = 10,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 263,
      name = "inserter",
      position = {
        x = 9,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 264,
      name = "transport-belt",
      position = {
        x = 10,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 265,
      name = "inserter",
      position = {
        x = 9,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 266,
      name = "transport-belt",
      position = {
        x = 12,
        y = 14
      }
    },
    {
      entity_number = 267,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 268,
      name = "transport-belt",
      position = {
        x = 12,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 269,
      name = "inserter",
      position = {
        x = 13,
        y = 14
      }
    },
    {
      entity_number = 270,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 15.5
      }
    },
    {
      direction = 6,
      entity_number = 271,
      name = "inserter",
      position = {
        x = 13,
        y = 15
      }
    },
    {
      entity_number = 272,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 17.5
      }
    },
    {
      direction = 4,
      entity_number = 273,
      name = "transport-belt",
      position = {
        x = 10,
        y = 16
      }
    },
    {
      direction = 6,
      entity_number = 274,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 275,
      name = "transport-belt",
      position = {
        x = 10,
        y = 17
      }
    },
    {
      direction = 6,
      entity_number = 276,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 277,
      name = "transport-belt",
      position = {
        x = 12,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 278,
      name = "underground-belt",
      position = {
        x = 11,
        y = 16
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 279,
      name = "transport-belt",
      position = {
        x = 12,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 280,
      name = "underground-belt",
      position = {
        x = 11,
        y = 17
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 281,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 16
      }
    },
    {
      entity_number = 282,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 17.5
      }
    },
    {
      direction = 2,
      entity_number = 283,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 17
      }
    },
    {
      entity_number = 284,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 19.5
      }
    },
    {
      direction = 4,
      entity_number = 285,
      name = "transport-belt",
      position = {
        x = 10,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 286,
      name = "inserter",
      position = {
        x = 9,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 287,
      name = "transport-belt",
      position = {
        x = 10,
        y = 19
      }
    },
    {
      direction = 2,
      entity_number = 288,
      name = "inserter",
      position = {
        x = 9,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 289,
      name = "transport-belt",
      position = {
        x = 12,
        y = 18
      }
    },
    {
      entity_number = 290,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 291,
      name = "transport-belt",
      position = {
        x = 12,
        y = 19
      }
    },
    {
      direction = 6,
      entity_number = 292,
      name = "inserter",
      position = {
        x = 13,
        y = 18
      }
    },
    {
      entity_number = 293,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 19.5
      }
    },
    {
      direction = 6,
      entity_number = 294,
      name = "inserter",
      position = {
        x = 13,
        y = 19
      }
    },
    {
      entity_number = 295,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 21.5
      }
    },
    {
      direction = 4,
      entity_number = 296,
      name = "transport-belt",
      position = {
        x = 10,
        y = 20
      }
    },
    {
      direction = 6,
      entity_number = 297,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 298,
      name = "transport-belt",
      position = {
        x = 10,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 299,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 300,
      name = "transport-belt",
      position = {
        x = 12,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 301,
      name = "underground-belt",
      position = {
        x = 11,
        y = 20
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 302,
      name = "transport-belt",
      position = {
        x = 12,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 303,
      name = "underground-belt",
      position = {
        x = 11,
        y = 21
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 304,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 20
      }
    },
    {
      entity_number = 305,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 21.5
      }
    },
    {
      direction = 2,
      entity_number = 306,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 21
      }
    },
    {
      entity_number = 307,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 23.5
      }
    },
    {
      direction = 4,
      entity_number = 308,
      name = "transport-belt",
      position = {
        x = 10,
        y = 22
      }
    },
    {
      direction = 2,
      entity_number = 309,
      name = "inserter",
      position = {
        x = 9,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 310,
      name = "transport-belt",
      position = {
        x = 10,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 311,
      name = "inserter",
      position = {
        x = 9,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 312,
      name = "transport-belt",
      position = {
        x = 12,
        y = 22
      }
    },
    {
      entity_number = 313,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 314,
      name = "transport-belt",
      position = {
        x = 12,
        y = 23
      }
    },
    {
      direction = 6,
      entity_number = 315,
      name = "inserter",
      position = {
        x = 13,
        y = 22
      }
    },
    {
      entity_number = 316,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 23.5
      }
    },
    {
      direction = 6,
      entity_number = 317,
      name = "inserter",
      position = {
        x = 13,
        y = 23
      }
    },
    {
      entity_number = 318,
      name = "stone-furnace",
      position = {
        x = 7.5,
        y = 25.5
      }
    },
    {
      direction = 4,
      entity_number = 319,
      name = "transport-belt",
      position = {
        x = 10,
        y = 24
      }
    },
    {
      direction = 6,
      entity_number = 320,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 321,
      name = "transport-belt",
      position = {
        x = 10,
        y = 25
      }
    },
    {
      direction = 6,
      entity_number = 322,
      name = "long-handed-inserter",
      position = {
        x = 9,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 323,
      name = "transport-belt",
      position = {
        x = 12,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 324,
      name = "underground-belt",
      position = {
        x = 11,
        y = 24
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 325,
      name = "transport-belt",
      position = {
        x = 12,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 326,
      name = "underground-belt",
      position = {
        x = 11,
        y = 25
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 327,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 24
      }
    },
    {
      entity_number = 328,
      name = "stone-furnace",
      position = {
        x = 14.5,
        y = 25.5
      }
    },
    {
      direction = 2,
      entity_number = 329,
      name = "long-handed-inserter",
      position = {
        x = 13,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 330,
      name = "transport-belt",
      position = {
        x = 10,
        y = 26
      }
    },
    {
      direction = 2,
      entity_number = 331,
      name = "inserter",
      position = {
        x = 9,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 332,
      name = "transport-belt",
      position = {
        x = 12,
        y = 26
      }
    },
    {
      entity_number = 333,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = 26
      }
    },
    {
      direction = 6,
      entity_number = 334,
      name = "inserter",
      position = {
        x = 13,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 335,
      name = "underground-belt",
      position = {
        x = 11,
        y = 29
      },
      type = "output"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "stone-furnace",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_6-furnaces"
}