return 
{
  anchor = {
    x = 16,
    y = 29
  },
  entities = {
    {
      direction = 4,
      entity_number = 1,
      name = "electric-mining-drill",
      position = {
        x = -16,
        y = -29
      }
    },
    {
      direction = 4,
      entity_number = 2,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = -29
      }
    },
    {
      entity_number = 3,
      name = "electric-mining-drill",
      position = {
        x = -16,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "underground-belt",
      position = {
        x = -16,
        y = -27
      },
      type = "input"
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "underground-belt",
      position = {
        x = -13,
        y = -27
      },
      type = "output"
    },
    {
      entity_number = 7,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = -25
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -12,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -11,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -10,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -9,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -8,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -7,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -6,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -5,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -4,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -3,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -2,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -1,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 0,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 1,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 2,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 3,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 4,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 5,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 6,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 7,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 8,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = 9,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 10,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 11,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 12,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 13,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 14,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 15,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 15,
        y = -26
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 15,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 15,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 15,
        y = -23
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 15,
        y = -22
      }
    },
    {
      entity_number = 41,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 15,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 15,
        y = -20
      }
    },
    {
      direction = 4,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 15,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 15,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 15,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 15,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 15,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 15,
        y = -14
      }
    },
    {
      entity_number = 50,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 15,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 15,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 15,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 15,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 15,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 15,
        y = -8
      }
    },
    {
      entity_number = 57,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 15,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 15,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = 15,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 15,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 15,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 15,
        y = -2
      }
    },
    {
      entity_number = 64,
      name = "small-electric-pole",
      position = {
        x = -12,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 15,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 15,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 15,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 15,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 15,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 15,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 15,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 15,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 15,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 15,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 15,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 15,
        y = 10
      }
    },
    {
      direction = 4,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 15,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 15,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 15,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 15,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 15,
        y = 15
      }
    },
    {
      direction = 4,
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 15,
        y = 16
      }
    },
    {
      direction = 4,
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 15,
        y = 17
      }
    },
    {
      direction = 4,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 15,
        y = 18
      }
    },
    {
      direction = 4,
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = 15,
        y = 19
      }
    },
    {
      direction = 4,
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = 15,
        y = 20
      }
    },
    {
      direction = 4,
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = 15,
        y = 21
      }
    },
    {
      direction = 4,
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 15,
        y = 22
      }
    },
    {
      direction = 4,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 15,
        y = 23
      }
    },
    {
      direction = 4,
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = 15,
        y = 24
      }
    },
    {
      direction = 4,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 15,
        y = 25
      }
    },
    {
      direction = 4,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 15,
        y = 26
      }
    },
    {
      direction = 4,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 15,
        y = 27
      }
    },
    {
      direction = 2,
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 15,
        y = 28
      }
    },
    {
      direction = 2,
      entity_number = 95,
      name = "underground-belt",
      position = {
        x = 16,
        y = 28
      },
      type = "input"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_coal_belt"
}