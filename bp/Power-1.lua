return {
  anchor = {
    x = 3,
    y = 30
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "steam-engine",
      position = {
        x = -11,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "steam-engine",
      position = {
        x = -6,
        y = -30
      }
    },
    {
      direction = 6,
      entity_number = 3,
      name = "boiler",
      position = {
        x = -2.5,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "inserter",
      position = {
        x = -1,
        y = -30
      }
    },
    {
      direction = 4,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 0,
        y = -30
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 0,
        y = -31
      }
    },
    {
      direction = 6,
      entity_number = 7,
      name = "inserter",
      position = {
        x = 1,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "boiler",
      position = {
        x = 2.5,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "steam-engine",
      position = {
        x = 6,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "steam-engine",
      position = {
        x = 11,
        y = -30
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "steam-engine",
      position = {
        x = -12,
        y = -27
      }
    },
    {
      entity_number = 12,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "steam-engine",
      position = {
        x = -6,
        y = -27
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "boiler",
      position = {
        x = -2.5,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 0,
        y = -28
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = -29
      }
    },
    {
      entity_number = 17,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "boiler",
      position = {
        x = 2.5,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "steam-engine",
      position = {
        x = 6,
        y = -27
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = -28
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "steam-engine",
      position = {
        x = 12,
        y = -27
      }
    },
    {
      entity_number = 22,
      name = "pipe",
      position = {
        x = -9,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "inserter",
      position = {
        x = -1,
        y = -27
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 0,
        y = -26
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 0,
        y = -27
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "inserter",
      position = {
        x = 1,
        y = -27
      }
    },
    {
      entity_number = 27,
      name = "pipe",
      position = {
        x = 9,
        y = -27
      }
    },
    {
      direction = 2,
      entity_number = 28,
      name = "steam-engine",
      position = {
        x = -11,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "steam-engine",
      position = {
        x = -6,
        y = -24
      }
    },
    {
      direction = 6,
      entity_number = 30,
      name = "boiler",
      position = {
        x = -2.5,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "inserter",
      position = {
        x = -1,
        y = -24
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 0,
        y = -25
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 0,
        y = -24
      }
    },
    {
      direction = 6,
      entity_number = 34,
      name = "inserter",
      position = {
        x = 1,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 35,
      name = "boiler",
      position = {
        x = 2.5,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "steam-engine",
      position = {
        x = 6,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "steam-engine",
      position = {
        x = 11,
        y = -24
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "steam-engine",
      position = {
        x = -12,
        y = -21
      }
    },
    {
      entity_number = 39,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "steam-engine",
      position = {
        x = -6,
        y = -21
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "boiler",
      position = {
        x = -2.5,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 0,
        y = -22
      }
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 0,
        y = -23
      }
    },
    {
      entity_number = 44,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "boiler",
      position = {
        x = 2.5,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 46,
      name = "steam-engine",
      position = {
        x = 6,
        y = -21
      }
    },
    {
      entity_number = 47,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = -22
      }
    },
    {
      direction = 2,
      entity_number = 48,
      name = "steam-engine",
      position = {
        x = 12,
        y = -21
      }
    },
    {
      entity_number = 49,
      name = "pipe",
      position = {
        x = -9,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 50,
      name = "inserter",
      position = {
        x = -1,
        y = -21
      }
    },
    {
      direction = 4,
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 0,
        y = -21
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 0,
        y = -20
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "inserter",
      position = {
        x = 1,
        y = -21
      }
    },
    {
      entity_number = 54,
      name = "pipe",
      position = {
        x = 9,
        y = -21
      }
    },
    {
      direction = 2,
      entity_number = 55,
      name = "steam-engine",
      position = {
        x = -11,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 56,
      name = "steam-engine",
      position = {
        x = -6,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "boiler",
      position = {
        x = -2.5,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 58,
      name = "inserter",
      position = {
        x = -1,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "inserter",
      position = {
        x = 1,
        y = -18
      }
    },
    {
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = 0,
        y = -18
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 0,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 62,
      name = "boiler",
      position = {
        x = 2.5,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 63,
      name = "steam-engine",
      position = {
        x = 6,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "steam-engine",
      position = {
        x = 11,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 65,
      name = "steam-engine",
      position = {
        x = -12,
        y = -15
      }
    },
    {
      entity_number = 66,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 67,
      name = "steam-engine",
      position = {
        x = -6,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 68,
      name = "boiler",
      position = {
        x = -2.5,
        y = -15
      }
    },
    {
      entity_number = 69,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -16
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 0,
        y = -16
      }
    },
    {
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 0,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 72,
      name = "boiler",
      position = {
        x = 2.5,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 73,
      name = "steam-engine",
      position = {
        x = 6,
        y = -15
      }
    },
    {
      entity_number = 74,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 75,
      name = "steam-engine",
      position = {
        x = 12,
        y = -15
      }
    },
    {
      entity_number = 76,
      name = "pipe",
      position = {
        x = -9,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 77,
      name = "inserter",
      position = {
        x = -1,
        y = -15
      }
    },
    {
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 0,
        y = -14
      }
    },
    {
      direction = 6,
      entity_number = 79,
      name = "inserter",
      position = {
        x = 1,
        y = -15
      }
    },
    {
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 0,
        y = -15
      }
    },
    {
      entity_number = 81,
      name = "pipe",
      position = {
        x = 9,
        y = -15
      }
    },
    {
      direction = 2,
      entity_number = 82,
      name = "steam-engine",
      position = {
        x = -11,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 83,
      name = "steam-engine",
      position = {
        x = -6,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 84,
      name = "boiler",
      position = {
        x = -2.5,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 85,
      name = "inserter",
      position = {
        x = -1,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 86,
      name = "inserter",
      position = {
        x = 1,
        y = -12
      }
    },
    {
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = 0,
        y = -12
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 0,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 89,
      name = "boiler",
      position = {
        x = 2.5,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 90,
      name = "steam-engine",
      position = {
        x = 6,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "steam-engine",
      position = {
        x = 11,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 92,
      name = "steam-engine",
      position = {
        x = -12,
        y = -9
      }
    },
    {
      entity_number = 93,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 94,
      name = "steam-engine",
      position = {
        x = -6,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 95,
      name = "boiler",
      position = {
        x = -2.5,
        y = -9
      }
    },
    {
      entity_number = 96,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      entity_number = 97,
      name = "transport-belt",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 0,
        y = -11
      }
    },
    {
      direction = 2,
      entity_number = 99,
      name = "boiler",
      position = {
        x = 2.5,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 100,
      name = "steam-engine",
      position = {
        x = 6,
        y = -9
      }
    },
    {
      entity_number = 101,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 102,
      name = "steam-engine",
      position = {
        x = 12,
        y = -9
      }
    },
    {
      entity_number = 103,
      name = "pipe",
      position = {
        x = -9,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 104,
      name = "inserter",
      position = {
        x = -1,
        y = -9
      }
    },
    {
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 106,
      name = "inserter",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      entity_number = 108,
      name = "pipe",
      position = {
        x = 9,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 109,
      name = "steam-engine",
      position = {
        x = -11,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 110,
      name = "steam-engine",
      position = {
        x = -6,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 111,
      name = "boiler",
      position = {
        x = -2.5,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 112,
      name = "inserter",
      position = {
        x = -1,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "inserter",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 116,
      name = "boiler",
      position = {
        x = 2.5,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 117,
      name = "steam-engine",
      position = {
        x = 6,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 118,
      name = "steam-engine",
      position = {
        x = 11,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 119,
      name = "steam-engine",
      position = {
        x = -12,
        y = -3
      }
    },
    {
      entity_number = 120,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 121,
      name = "steam-engine",
      position = {
        x = -6,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 122,
      name = "boiler",
      position = {
        x = -2.5,
        y = -3
      }
    },
    {
      entity_number = 123,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -4
      }
    },
    {
      entity_number = 124,
      name = "transport-belt",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      entity_number = 125,
      name = "transport-belt",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 126,
      name = "boiler",
      position = {
        x = 2.5,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 127,
      name = "steam-engine",
      position = {
        x = 6,
        y = -3
      }
    },
    {
      entity_number = 128,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 129,
      name = "steam-engine",
      position = {
        x = 12,
        y = -3
      }
    },
    {
      entity_number = 130,
      name = "pipe",
      position = {
        x = -9,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 131,
      name = "inserter",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      entity_number = 132,
      name = "transport-belt",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 133,
      name = "inserter",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      entity_number = 134,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      entity_number = 135,
      name = "pipe",
      position = {
        x = 9,
        y = -3
      }
    },
    {
      direction = 2,
      entity_number = 136,
      name = "steam-engine",
      position = {
        x = -11,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 137,
      name = "steam-engine",
      position = {
        x = -6,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 138,
      name = "boiler",
      position = {
        x = -2.5,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 139,
      name = "inserter",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 140,
      name = "inserter",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      entity_number = 141,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      entity_number = 142,
      name = "transport-belt",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 143,
      name = "boiler",
      position = {
        x = 2.5,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 144,
      name = "steam-engine",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 145,
      name = "steam-engine",
      position = {
        x = 11,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 146,
      name = "steam-engine",
      position = {
        x = -12,
        y = 3
      }
    },
    {
      entity_number = 147,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 148,
      name = "steam-engine",
      position = {
        x = -6,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 149,
      name = "boiler",
      position = {
        x = -2.5,
        y = 3
      }
    },
    {
      entity_number = 150,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      entity_number = 151,
      name = "transport-belt",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      entity_number = 152,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 153,
      name = "boiler",
      position = {
        x = 2.5,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 154,
      name = "steam-engine",
      position = {
        x = 6,
        y = 3
      }
    },
    {
      entity_number = 155,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 156,
      name = "steam-engine",
      position = {
        x = 12,
        y = 3
      }
    },
    {
      entity_number = 157,
      name = "pipe",
      position = {
        x = -9,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 158,
      name = "inserter",
      position = {
        x = -1,
        y = 3
      }
    },
    {
      entity_number = 159,
      name = "transport-belt",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 160,
      name = "inserter",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      entity_number = 161,
      name = "transport-belt",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      entity_number = 162,
      name = "pipe",
      position = {
        x = 9,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 163,
      name = "steam-engine",
      position = {
        x = -11,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 164,
      name = "steam-engine",
      position = {
        x = -6,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 165,
      name = "boiler",
      position = {
        x = -2.5,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 166,
      name = "inserter",
      position = {
        x = -1,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 167,
      name = "inserter",
      position = {
        x = 1,
        y = 6
      }
    },
    {
      entity_number = 168,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      entity_number = 169,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 170,
      name = "boiler",
      position = {
        x = 2.5,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 171,
      name = "steam-engine",
      position = {
        x = 6,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 172,
      name = "steam-engine",
      position = {
        x = 11,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 173,
      name = "steam-engine",
      position = {
        x = -12,
        y = 9
      }
    },
    {
      entity_number = 174,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 175,
      name = "steam-engine",
      position = {
        x = -6,
        y = 9
      }
    },
    {
      direction = 6,
      entity_number = 176,
      name = "boiler",
      position = {
        x = -2.5,
        y = 9
      }
    },
    {
      entity_number = 177,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 8
      }
    },
    {
      entity_number = 178,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      entity_number = 179,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 180,
      name = "boiler",
      position = {
        x = 2.5,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 181,
      name = "steam-engine",
      position = {
        x = 6,
        y = 9
      }
    },
    {
      entity_number = 182,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 183,
      name = "steam-engine",
      position = {
        x = 12,
        y = 9
      }
    },
    {
      entity_number = 184,
      name = "pipe",
      position = {
        x = -9,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 185,
      name = "inserter",
      position = {
        x = -1,
        y = 9
      }
    },
    {
      entity_number = 186,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 187,
      name = "inserter",
      position = {
        x = 1,
        y = 9
      }
    },
    {
      entity_number = 188,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      entity_number = 189,
      name = "pipe",
      position = {
        x = 9,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 190,
      name = "steam-engine",
      position = {
        x = -11,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 191,
      name = "steam-engine",
      position = {
        x = -6,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 192,
      name = "boiler",
      position = {
        x = -2.5,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 193,
      name = "inserter",
      position = {
        x = -1,
        y = 12
      }
    },
    {
      direction = 6,
      entity_number = 194,
      name = "inserter",
      position = {
        x = 1,
        y = 12
      }
    },
    {
      entity_number = 195,
      name = "transport-belt",
      position = {
        x = 0,
        y = 12
      }
    },
    {
      entity_number = 196,
      name = "transport-belt",
      position = {
        x = 0,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 197,
      name = "boiler",
      position = {
        x = 2.5,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 198,
      name = "steam-engine",
      position = {
        x = 6,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 199,
      name = "steam-engine",
      position = {
        x = 11,
        y = 12
      }
    },
    {
      direction = 2,
      entity_number = 200,
      name = "steam-engine",
      position = {
        x = -12,
        y = 15
      }
    },
    {
      entity_number = 201,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 202,
      name = "steam-engine",
      position = {
        x = -6,
        y = 15
      }
    },
    {
      direction = 6,
      entity_number = 203,
      name = "boiler",
      position = {
        x = -2.5,
        y = 15
      }
    },
    {
      entity_number = 204,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 14
      }
    },
    {
      entity_number = 205,
      name = "transport-belt",
      position = {
        x = 0,
        y = 14
      }
    },
    {
      entity_number = 206,
      name = "transport-belt",
      position = {
        x = 0,
        y = 13
      }
    },
    {
      direction = 2,
      entity_number = 207,
      name = "boiler",
      position = {
        x = 2.5,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 208,
      name = "steam-engine",
      position = {
        x = 6,
        y = 15
      }
    },
    {
      entity_number = 209,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 210,
      name = "steam-engine",
      position = {
        x = 12,
        y = 15
      }
    },
    {
      entity_number = 211,
      name = "pipe",
      position = {
        x = -9,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 212,
      name = "inserter",
      position = {
        x = -1,
        y = 15
      }
    },
    {
      entity_number = 213,
      name = "transport-belt",
      position = {
        x = 0,
        y = 16
      }
    },
    {
      direction = 6,
      entity_number = 214,
      name = "inserter",
      position = {
        x = 1,
        y = 15
      }
    },
    {
      entity_number = 215,
      name = "transport-belt",
      position = {
        x = 0,
        y = 15
      }
    },
    {
      entity_number = 216,
      name = "pipe",
      position = {
        x = 9,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 217,
      name = "steam-engine",
      position = {
        x = -11,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 218,
      name = "steam-engine",
      position = {
        x = -6,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 219,
      name = "boiler",
      position = {
        x = -2.5,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 220,
      name = "inserter",
      position = {
        x = -1,
        y = 18
      }
    },
    {
      direction = 6,
      entity_number = 221,
      name = "inserter",
      position = {
        x = 1,
        y = 18
      }
    },
    {
      entity_number = 222,
      name = "transport-belt",
      position = {
        x = 0,
        y = 18
      }
    },
    {
      entity_number = 223,
      name = "transport-belt",
      position = {
        x = 0,
        y = 17
      }
    },
    {
      direction = 2,
      entity_number = 224,
      name = "boiler",
      position = {
        x = 2.5,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 225,
      name = "steam-engine",
      position = {
        x = 6,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 226,
      name = "steam-engine",
      position = {
        x = 11,
        y = 18
      }
    },
    {
      direction = 2,
      entity_number = 227,
      name = "steam-engine",
      position = {
        x = -12,
        y = 21
      }
    },
    {
      entity_number = 228,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 20
      }
    },
    {
      direction = 2,
      entity_number = 229,
      name = "steam-engine",
      position = {
        x = -6,
        y = 21
      }
    },
    {
      direction = 6,
      entity_number = 230,
      name = "boiler",
      position = {
        x = -2.5,
        y = 21
      }
    },
    {
      entity_number = 231,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 20
      }
    },
    {
      entity_number = 232,
      name = "transport-belt",
      position = {
        x = 0,
        y = 20
      }
    },
    {
      entity_number = 233,
      name = "transport-belt",
      position = {
        x = 0,
        y = 19
      }
    },
    {
      direction = 2,
      entity_number = 234,
      name = "boiler",
      position = {
        x = 2.5,
        y = 21
      }
    },
    {
      direction = 2,
      entity_number = 235,
      name = "steam-engine",
      position = {
        x = 6,
        y = 21
      }
    },
    {
      entity_number = 236,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = 20
      }
    },
    {
      direction = 2,
      entity_number = 237,
      name = "steam-engine",
      position = {
        x = 12,
        y = 21
      }
    },
    {
      entity_number = 238,
      name = "pipe",
      position = {
        x = -9,
        y = 21
      }
    },
    {
      direction = 2,
      entity_number = 239,
      name = "inserter",
      position = {
        x = -1,
        y = 21
      }
    },
    {
      entity_number = 240,
      name = "transport-belt",
      position = {
        x = 0,
        y = 22
      }
    },
    {
      direction = 6,
      entity_number = 241,
      name = "inserter",
      position = {
        x = 1,
        y = 21
      }
    },
    {
      entity_number = 242,
      name = "transport-belt",
      position = {
        x = 0,
        y = 21
      }
    },
    {
      entity_number = 243,
      name = "pipe",
      position = {
        x = 9,
        y = 21
      }
    },
    {
      entity_number = 244,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 245,
      name = "steam-engine",
      position = {
        x = -11,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 246,
      name = "steam-engine",
      position = {
        x = -6,
        y = 24
      }
    },
    {
      direction = 6,
      entity_number = 247,
      name = "boiler",
      position = {
        x = -2.5,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 248,
      name = "inserter",
      position = {
        x = -1,
        y = 24
      }
    },
    {
      direction = 6,
      entity_number = 249,
      name = "inserter",
      position = {
        x = 1,
        y = 24
      }
    },
    {
      entity_number = 250,
      name = "transport-belt",
      position = {
        x = 0,
        y = 24
      }
    },
    {
      entity_number = 251,
      name = "transport-belt",
      position = {
        x = 0,
        y = 23
      }
    },
    {
      direction = 2,
      entity_number = 252,
      name = "boiler",
      position = {
        x = 2.5,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 253,
      name = "steam-engine",
      position = {
        x = 6,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 254,
      name = "steam-engine",
      position = {
        x = 11,
        y = 24
      }
    },
    {
      direction = 2,
      entity_number = 255,
      name = "steam-engine",
      position = {
        x = -12,
        y = 27
      }
    },
    {
      entity_number = 256,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 26
      }
    },
    {
      direction = 2,
      entity_number = 257,
      name = "steam-engine",
      position = {
        x = -6,
        y = 27
      }
    },
    {
      direction = 6,
      entity_number = 258,
      name = "boiler",
      position = {
        x = -2.5,
        y = 27
      }
    },
    {
      entity_number = 259,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 26
      }
    },
    {
      entity_number = 260,
      name = "transport-belt",
      position = {
        x = 0,
        y = 26
      }
    },
    {
      entity_number = 261,
      name = "transport-belt",
      position = {
        x = 0,
        y = 25
      }
    },
    {
      direction = 2,
      entity_number = 262,
      name = "boiler",
      position = {
        x = 2.5,
        y = 27
      }
    },
    {
      direction = 2,
      entity_number = 263,
      name = "steam-engine",
      position = {
        x = 6,
        y = 27
      }
    },
    {
      entity_number = 264,
      name = "small-electric-pole",
      position = {
        x = 9,
        y = 26
      }
    },
    {
      direction = 2,
      entity_number = 265,
      name = "steam-engine",
      position = {
        x = 12,
        y = 27
      }
    },
    {
      entity_number = 266,
      name = "pipe",
      position = {
        x = -9,
        y = 27
      }
    },
    {
      direction = 2,
      entity_number = 267,
      name = "inserter",
      position = {
        x = -1,
        y = 27
      }
    },
    {
      entity_number = 268,
      name = "transport-belt",
      position = {
        x = 0,
        y = 28
      }
    },
    {
      direction = 6,
      entity_number = 269,
      name = "inserter",
      position = {
        x = 1,
        y = 27
      }
    },
    {
      entity_number = 270,
      name = "transport-belt",
      position = {
        x = 0,
        y = 27
      }
    },
    {
      entity_number = 271,
      name = "pipe",
      position = {
        x = 9,
        y = 27
      }
    },
    {
      entity_number = 272,
      name = "transport-belt",
      position = {
        x = 0,
        y = 29
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "steam-engine",
        type = "item"
      }
    }
  },
  name = "Power-1"
}