return 
{
  anchor = {
    x = 0,
    y = 0
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 2,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 2,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 1,
        y = -8
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 2,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 10,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -10,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -11,
        y = -7
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -11,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -8,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -9,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -6,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -7,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -4,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -5,
        y = -7
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -4,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -3,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = -2,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = -1,
        y = -6
      }
    },
    {
      entity_number = 25,
      name = "splitter",
      position = {
        x = 1.5,
        y = -7
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 2,
        y = -6
      }
    },
    {
      entity_number = 28,
      name = "underground-belt",
      position = {
        x = -11,
        y = -5
      },
      type = "output"
    },
    {
      entity_number = 29,
      name = "small-electric-pole",
      position = {
        x = -11,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -4
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -4,
        y = -5
      }
    },
    {
      entity_number = 32,
      name = "underground-belt",
      position = {
        x = -4,
        y = -4
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 33,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 2,
        y = -5
      }
    },
    {
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 2,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 37,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = -2
      }
    },
    {
      entity_number = 38,
      name = "underground-belt",
      position = {
        x = -11,
        y = -2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 39,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -1
      }
    },
    {
      entity_number = 40,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 2,
        y = -3
      }
    },
    {
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 2,
        y = -2
      }
    },
    {
      direction = 2,
      entity_number = 44,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 1
      }
    },
    {
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -11,
        y = -1
      }
    },
    {
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -11,
        y = 0
      }
    },
    {
      entity_number = 48,
      name = "underground-belt",
      position = {
        x = -4,
        y = -1
      },
      type = "input"
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -4,
        y = 0
      }
    },
    {
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 2,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = 4,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 5,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 7,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 8,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = 9,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 10,
        y = 0
      }
    },
    {
      entity_number = 62,
      name = "underground-belt",
      position = {
        x = -11,
        y = 1
      },
      type = "output"
    },
    {
      entity_number = 63,
      name = "small-electric-pole",
      position = {
        x = -11,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 2
      }
    },
    {
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = -4,
        y = 1
      }
    },
    {
      entity_number = 66,
      name = "underground-belt",
      position = {
        x = -4,
        y = 2
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 67,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 68,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 69,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 2
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 3,
        y = 1
      }
    },
    {
      entity_number = 71,
      name = "underground-belt",
      position = {
        x = 3,
        y = 2
      },
      type = "output"
    },
    {
      direction = 2,
      entity_number = 72,
      name = "electric-mining-drill",
      position = {
        x = 8,
        y = 2
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 10,
        y = 1
      }
    },
    {
      entity_number = 74,
      name = "underground-belt",
      position = {
        x = 10,
        y = 2
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 75,
      name = "electric-mining-drill",
      position = {
        x = 12,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 76,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 77,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 4
      }
    },
    {
      entity_number = 78,
      name = "underground-belt",
      position = {
        x = -11,
        y = 4
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 79,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 5
      }
    },
    {
      entity_number = 80,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 81,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 82,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 6,
      entity_number = 83,
      name = "electric-mining-drill",
      position = {
        x = 5,
        y = 5
      }
    },
    {
      entity_number = 84,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 85,
      name = "electric-mining-drill",
      position = {
        x = 8,
        y = 5
      }
    },
    {
      entity_number = 86,
      name = "small-electric-pole",
      position = {
        x = 10,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 87,
      name = "electric-mining-drill",
      position = {
        x = 12,
        y = 5
      }
    },
    {
      entity_number = 88,
      name = "small-electric-pole",
      position = {
        x = 14,
        y = 3
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -11,
        y = 5
      }
    },
    {
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = -11,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = -10,
        y = 6
      }
    },
    {
      entity_number = 92,
      name = "small-electric-pole",
      position = {
        x = -8,
        y = 6
      }
    },
    {
      direction = 6,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = -9,
        y = 6
      }
    },
    {
      entity_number = 94,
      name = "underground-belt",
      position = {
        x = -4,
        y = 5
      },
      type = "input"
    },
    {
      entity_number = 95,
      name = "underground-belt",
      position = {
        x = 3,
        y = 5
      },
      type = "input"
    },
    {
      entity_number = 96,
      name = "underground-belt",
      position = {
        x = 10,
        y = 5
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 97,
      name = "electric-mining-drill",
      position = {
        x = -11,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 98,
      name = "electric-mining-drill",
      position = {
        x = -7,
        y = 8
      }
    },
    {
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = -9,
        y = 7
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = -9,
        y = 8
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_2"
}