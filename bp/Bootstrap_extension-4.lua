return 
{
  anchor = {
    x = -7,
    y = 18
  },
  entities = {
    {
      direction = 4,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 0,
        y = -18
      }
    },
    {
      entity_number = 2,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -17
      },
      recipe = "transport-belt"
    },
    {
      direction = 4,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 0,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 0,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -17
      }
    },
    {
      entity_number = 7,
      name = "iron-chest",
      position = {
        x = 6,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -16
      }
    },
    {
      entity_number = 9,
      name = "iron-chest",
      position = {
        x = 6,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 0,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 0,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "fast-inserter",
      position = {
        x = 2,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 13,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -14
      }
    },
    {
      entity_number = 14,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -13
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 15,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 0,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 18,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 0,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      entity_number = 21,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -10
      },
      recipe = "transport-belt"
    },
    {
      direction = 6,
      entity_number = 22,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 23,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -10
      }
    },
    {
      entity_number = 24,
      name = "iron-chest",
      position = {
        x = 6,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 0,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "fast-inserter",
      position = {
        x = 2,
        y = -8
      }
    },
    {
      entity_number = 28,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 29,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -9
      }
    },
    {
      entity_number = 30,
      name = "iron-chest",
      position = {
        x = 6,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 0,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      entity_number = 34,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -6
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 6,
      entity_number = 35,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 0,
        y = -4
      }
    },
    {
      entity_number = 38,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -3
      },
      recipe = "transport-belt"
    },
    {
      direction = 6,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 42,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 43,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -3
      }
    },
    {
      entity_number = 44,
      name = "iron-chest",
      position = {
        x = 6,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 45,
      name = "fast-inserter",
      position = {
        x = 5,
        y = -2
      }
    },
    {
      entity_number = 46,
      name = "iron-chest",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "fast-inserter",
      position = {
        x = 2,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 51,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      entity_number = 52,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = 1
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 53,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 55,
      name = "fast-inserter",
      position = {
        x = 1,
        y = 1
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-4"
}