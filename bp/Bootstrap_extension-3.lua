return 
{
  anchor = {
    x = 1,
    y = -2
  },
  entities = {
    {
      entity_number = 1,
      name = "assembling-machine-2",
      position = {
        x = 1,
        y = -7
      },
      recipe = "electric-mining-drill"
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -5
      }
    },
    {
      entity_number = 3,
      name = "assembling-machine-2",
      position = {
        x = 1,
        y = -4
      },
      recipe = "electric-mining-drill"
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -5
      }
    },
    {
      entity_number = 5,
      name = "iron-chest",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "fast-inserter",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "fast-inserter",
      position = {
        x = 3,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "fast-inserter",
      position = {
        x = 3,
        y = -3
      }
    },
    {
      entity_number = 10,
      name = "iron-chest",
      position = {
        x = -2,
        y = 1
      }
    },
    {
      entity_number = 11,
      name = "assembling-machine-2",
      position = {
        x = 1,
        y = 1
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 2,
      entity_number = 12,
      name = "fast-inserter",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 1
      }
    },
    {
      entity_number = 15,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      entity_number = 16,
      name = "assembling-machine-2",
      position = {
        x = 1,
        y = 4
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 2,
      entity_number = 17,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 3
      }
    },
    {
      entity_number = 18,
      name = "iron-chest",
      position = {
        x = -2,
        y = 4
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "fast-inserter",
      position = {
        x = -1,
        y = 4
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 4
      }
    },
    {
      entity_number = 22,
      name = "iron-chest",
      position = {
        x = -2,
        y = 7
      }
    },
    {
      entity_number = 23,
      name = "assembling-machine-2",
      position = {
        x = 1,
        y = 7
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 2,
      entity_number = 24,
      name = "fast-inserter",
      position = {
        x = -1,
        y = 7
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "fast-inserter",
      position = {
        x = 3,
        y = 7
      }
    },
    {
      entity_number = 27,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 8
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-3"
}