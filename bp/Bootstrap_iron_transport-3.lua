return 
{
  anchor = {
    x = -12,
    y = -17
  },
  entities = {
    {
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -14,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -14,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -12,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -13,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -13,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -12,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -10,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -11,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = -11,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -10,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -8,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -9,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -9,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -8,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -6,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -7,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -7,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -6,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -4,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -5,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -5,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = -4,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = -2,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = -3,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -3,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -2,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 0,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = -1,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = -1,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 0,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 2,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = 1,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 33,
      name = "transport-belt",
      position = {
        x = 1,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 2,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 3,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = 3,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = 4,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = 6,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = 5,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 5,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 8,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 7,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 10,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 9,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 12,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = 11,
        y = -19
      }
    },
    {
      direction = 2,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 13,
        y = -19
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -14,
        y = -17
      }
    },
    {
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = -14,
        y = -16
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = -13,
        y = -17
      }
    },
    {
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -13,
        y = -16
      }
    },
    {
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -14,
        y = -15
      }
    },
    {
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = -14,
        y = -14
      }
    },
    {
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = -13,
        y = -15
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -13,
        y = -14
      }
    },
    {
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -14,
        y = -13
      }
    },
    {
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -14,
        y = -12
      }
    },
    {
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = -13,
        y = -13
      }
    },
    {
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = -13,
        y = -12
      }
    },
    {
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = -14,
        y = -10
      }
    },
    {
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = -14,
        y = -11
      }
    },
    {
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = -13,
        y = -11
      }
    },
    {
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = -13,
        y = -10
      }
    },
    {
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = -14,
        y = -8
      }
    },
    {
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = -14,
        y = -9
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = -13,
        y = -9
      }
    },
    {
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = -13,
        y = -8
      }
    },
    {
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = -14,
        y = -6
      }
    },
    {
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = -14,
        y = -7
      }
    },
    {
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = -13,
        y = -7
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = -13,
        y = -6
      }
    },
    {
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = -14,
        y = -4
      }
    },
    {
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = -14,
        y = -5
      }
    },
    {
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = -13,
        y = -5
      }
    },
    {
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = -13,
        y = -4
      }
    },
    {
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = -14,
        y = -2
      }
    },
    {
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = -14,
        y = -3
      }
    },
    {
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = -13,
        y = -3
      }
    },
    {
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = -13,
        y = -2
      }
    },
    {
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = -14,
        y = 0
      }
    },
    {
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = -14,
        y = -1
      }
    },
    {
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = -13,
        y = -1
      }
    },
    {
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = -13,
        y = 0
      }
    },
    {
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = -14,
        y = 2
      }
    },
    {
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = -14,
        y = 1
      }
    },
    {
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = -13,
        y = 1
      }
    },
    {
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -13,
        y = 2
      }
    },
    {
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = -14,
        y = 4
      }
    },
    {
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = -14,
        y = 3
      }
    },
    {
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = -13,
        y = 3
      }
    },
    {
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = -13,
        y = 4
      }
    },
    {
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = -14,
        y = 6
      }
    },
    {
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = -14,
        y = 5
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = -13,
        y = 5
      }
    },
    {
      entity_number = 97,
      name = "transport-belt",
      position = {
        x = -13,
        y = 6
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = -14,
        y = 8
      }
    },
    {
      entity_number = 99,
      name = "transport-belt",
      position = {
        x = -14,
        y = 7
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = -13,
        y = 7
      }
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = -13,
        y = 8
      }
    },
    {
      entity_number = 102,
      name = "transport-belt",
      position = {
        x = -14,
        y = 9
      }
    },
    {
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = -14,
        y = 10
      }
    },
    {
      entity_number = 104,
      name = "transport-belt",
      position = {
        x = -13,
        y = 9
      }
    },
    {
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = -13,
        y = 10
      }
    },
    {
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = -14,
        y = 12
      }
    },
    {
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = -14,
        y = 11
      }
    },
    {
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = -13,
        y = 11
      }
    },
    {
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = -13,
        y = 12
      }
    },
    {
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = -14,
        y = 14
      }
    },
    {
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = -14,
        y = 13
      }
    },
    {
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = -13,
        y = 13
      }
    },
    {
      entity_number = 113,
      name = "transport-belt",
      position = {
        x = -13,
        y = 14
      }
    },
    {
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = -14,
        y = 16
      }
    },
    {
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = -14,
        y = 15
      }
    },
    {
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = -13,
        y = 15
      }
    },
    {
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = -13,
        y = 16
      }
    },
    {
      entity_number = 118,
      name = "transport-belt",
      position = {
        x = -14,
        y = 18
      }
    },
    {
      entity_number = 119,
      name = "transport-belt",
      position = {
        x = -14,
        y = 17
      }
    },
    {
      entity_number = 120,
      name = "transport-belt",
      position = {
        x = -13,
        y = 17
      }
    },
    {
      entity_number = 121,
      name = "transport-belt",
      position = {
        x = -13,
        y = 18
      }
    },
    {
      entity_number = 122,
      name = "transport-belt",
      position = {
        x = -14,
        y = 19
      }
    },
    {
      entity_number = 123,
      name = "transport-belt",
      position = {
        x = -13,
        y = 19
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_iron_transport-3"
}