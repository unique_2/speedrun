return 
{
  anchor = {
    x = -5,
    y = -12
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 3,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 4,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 5,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 6,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 7,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 8,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 9,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 10,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 11,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 12,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 13,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 14,
        y = -16
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 3,
        y = -15
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 3,
        y = -14
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -4,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -4,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -3,
        y = -13
      }
    },
    {
      entity_number = 19,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -2,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -1,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 2,
        y = -13
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 3,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 26,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -10
      }
    },
    {
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = -4,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 28,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -10
      }
    },
    {
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = -4,
        y = -10
      }
    },
    {
      direction = 2,
      entity_number = 30,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -7
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -4,
        y = -9
      }
    },
    {
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = -4,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -7
      }
    },
    {
      entity_number = 34,
      name = "underground-belt",
      position = {
        x = -4,
        y = -7
      },
      type = "output"
    },
    {
      entity_number = 35,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -6
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 37,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      entity_number = 38,
      name = "underground-belt",
      position = {
        x = -4,
        y = -4
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 39,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = -1
      }
    },
    {
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = -4,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 42,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      entity_number = 43,
      name = "underground-belt",
      position = {
        x = -4,
        y = -1
      },
      type = "output"
    },
    {
      entity_number = 44,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "underground-belt",
      position = {
        x = -11,
        y = 2
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 47,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 2
      }
    },
    {
      direction = 2,
      entity_number = 48,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 49,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 2
      }
    },
    {
      entity_number = 50,
      name = "underground-belt",
      position = {
        x = -4,
        y = 2
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 51,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 5
      }
    },
    {
      entity_number = 52,
      name = "small-electric-pole",
      position = {
        x = -11,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 5
      }
    },
    {
      direction = 2,
      entity_number = 54,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 5
      }
    },
    {
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = -4,
        y = 3
      }
    },
    {
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = -4,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "underground-belt",
      position = {
        x = -11,
        y = 5
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -11,
        y = 6
      }
    },
    {
      entity_number = 60,
      name = "underground-belt",
      position = {
        x = -4,
        y = 5
      },
      type = "output"
    },
    {
      entity_number = 61,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 6
      }
    },
    {
      direction = 2,
      entity_number = 62,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 8
      }
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = -11,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = -11,
        y = 8
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 65,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 8
      }
    },
    {
      direction = 2,
      entity_number = 66,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 8
      }
    },
    {
      entity_number = 68,
      name = "underground-belt",
      position = {
        x = -4,
        y = 8
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 69,
      name = "electric-mining-drill",
      position = {
        x = -13,
        y = 11
      }
    },
    {
      entity_number = 70,
      name = "small-electric-pole",
      position = {
        x = -11,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 71,
      name = "electric-mining-drill",
      position = {
        x = -9,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 72,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 11
      }
    },
    {
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = -4,
        y = 9
      }
    },
    {
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = -4,
        y = 10
      }
    },
    {
      direction = 6,
      entity_number = 75,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 11
      }
    },
    {
      direction = 4,
      entity_number = 76,
      name = "underground-belt",
      position = {
        x = -11,
        y = 11
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = -11,
        y = 12
      }
    },
    {
      entity_number = 78,
      name = "underground-belt",
      position = {
        x = -4,
        y = 11
      },
      type = "output"
    },
    {
      entity_number = 79,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = -11,
        y = 13
      }
    },
    {
      direction = 4,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = -11,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 82,
      name = "electric-mining-drill",
      position = {
        x = -6,
        y = 14
      }
    },
    {
      direction = 6,
      entity_number = 83,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 14
      }
    },
    {
      entity_number = 84,
      name = "underground-belt",
      position = {
        x = -4,
        y = 14
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = -11,
        y = 15
      }
    },
    {
      direction = 2,
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = -11,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = -10,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = -9,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = -8,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = -7,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = -6,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = -5,
        y = 16
      }
    },
    {
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = -4,
        y = 15
      }
    },
    {
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = -4,
        y = 16
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_belt_5"
}