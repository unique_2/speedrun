return 
{
  anchor = {
    x = -1,
    y = -6
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = -15
      }
    },
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = -16
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = -10,
        y = -14
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = -12
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = -10,
        y = -7
      }
    },
    {
      entity_number = 6,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -7
      }
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = -8
      }
    },
    {
      entity_number = 9,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = -4
      }
    },
    {
      entity_number = 10,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 0
      }
    },
    {
      entity_number = 11,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      entity_number = 12,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      entity_number = 13,
      name = "small-electric-pole",
      position = {
        x = 7,
        y = 11
      }
    },
    {
      entity_number = 14,
      name = "small-electric-pole",
      position = {
        x = 10,
        y = 16
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "small-electric-pole",
        type = "item"
      }
    }
  },
  name = "Bootstrap_iron_coal-1-power_connection"
}