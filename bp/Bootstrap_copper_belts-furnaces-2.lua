return 
{
  anchor = {
    x = -2,
    y = 17
  },
  entities = {
    {
      direction = 6,
      entity_number = 1,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -16
      }
    },
    {
      direction = 6,
      entity_number = 2,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = -1,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = -1,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 5,
      name = "underground-belt",
      position = {
        x = 0,
        y = -16
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 0,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 1,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 1,
        y = -17
      }
    },
    {
      direction = 2,
      entity_number = 9,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -16
      }
    },
    {
      direction = 2,
      entity_number = 10,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -1,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -1,
        y = -15
      }
    },
    {
      direction = 4,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 1,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 1,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -12
      }
    },
    {
      direction = 6,
      entity_number = 16,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = -1,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -1,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "underground-belt",
      position = {
        x = 0,
        y = -12
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 20,
      name = "underground-belt",
      position = {
        x = 0,
        y = -13
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 1,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 1,
        y = -13
      }
    },
    {
      direction = 2,
      entity_number = 23,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -12
      }
    },
    {
      direction = 2,
      entity_number = 24,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -13
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -1,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -1,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = 1,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = 1,
        y = -11
      }
    },
    {
      direction = 6,
      entity_number = 29,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -8
      }
    },
    {
      direction = 6,
      entity_number = 30,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -1,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = -1,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 33,
      name = "underground-belt",
      position = {
        x = 0,
        y = -8
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 34,
      name = "underground-belt",
      position = {
        x = 0,
        y = -9
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = 1,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = 1,
        y = -9
      }
    },
    {
      direction = 2,
      entity_number = 37,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -8
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -9
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = -1,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = -1,
        y = -7
      }
    },
    {
      direction = 4,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 1,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = 1,
        y = -7
      }
    },
    {
      direction = 6,
      entity_number = 43,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 44,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "underground-belt",
      position = {
        x = 0,
        y = -4
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 48,
      name = "underground-belt",
      position = {
        x = 0,
        y = -5
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 1,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 1,
        y = -5
      }
    },
    {
      direction = 2,
      entity_number = 51,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -4
      }
    },
    {
      direction = 2,
      entity_number = 52,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -5
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = 1,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 58,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 61,
      name = "underground-belt",
      position = {
        x = 0,
        y = 0
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 62,
      name = "underground-belt",
      position = {
        x = 0,
        y = -1
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 64,
      name = "transport-belt",
      position = {
        x = 1,
        y = -1
      }
    },
    {
      direction = 2,
      entity_number = 65,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      direction = 2,
      entity_number = 66,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = -1
      }
    },
    {
      direction = 4,
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      direction = 4,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 1,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 1,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 71,
      name = "long-handed-inserter",
      position = {
        x = -2,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = -1,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 73,
      name = "underground-belt",
      position = {
        x = 0,
        y = 4
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 74,
      name = "underground-belt",
      position = {
        x = 0,
        y = 3
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 1,
        y = 3
      }
    },
    {
      direction = 2,
      entity_number = 76,
      name = "long-handed-inserter",
      position = {
        x = 2,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 77,
      name = "underground-belt",
      position = {
        x = 0,
        y = 8
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 78,
      name = "underground-belt",
      position = {
        x = 0,
        y = 7
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 79,
      name = "underground-belt",
      position = {
        x = 0,
        y = 12
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 80,
      name = "underground-belt",
      position = {
        x = 0,
        y = 11
      },
      type = "output"
    },
    {
      direction = 4,
      entity_number = 82,
      name = "underground-belt",
      position = {
        x = 0,
        y = 17
      },
      type = "output"
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_copper_belts-furnaces-2"
}