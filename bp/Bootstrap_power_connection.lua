return 
{
  anchor = {
    x = 19,
    y = -45
  },
  entities = {
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = 18,
        y = -44
      }
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -43
      }
    },
    {
      entity_number = 4,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -39
      }
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -35
      }
    },
    {
      entity_number = 6,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -31
      }
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -27
      }
    },
    {
      entity_number = 8,
      name = "small-electric-pole",
      position = {
        x = 11,
        y = -23
      }
    },
    {
      entity_number = 9,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -21
      }
    },
    {
      entity_number = 10,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = -17
      }
    },
    {
      entity_number = 11,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -12
      }
    },
    {
      entity_number = 12,
      name = "small-electric-pole",
      position = {
        x = -2,
        y = -5
      }
    },
    {
      entity_number = 13,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      entity_number = 14,
      name = "small-electric-pole",
      position = {
        x = -8,
        y = 3
      }
    },
    {
      entity_number = 15,
      name = "small-electric-pole",
      position = {
        x = -12,
        y = 6
      }
    },
    {
      entity_number = 16,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = 10
      }
    },
    {
      entity_number = 17,
      name = "small-electric-pole",
      position = {
        x = -14,
        y = 15
      }
    },
    {
      entity_number = 18,
      name = "small-electric-pole",
      position = {
        x = -16,
        y = 22
      }
    },
    {
      entity_number = 19,
      name = "small-electric-pole",
      position = {
        x = -18,
        y = 28
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 35
      }
    },
    {
      entity_number = 21,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 39
      }
    },
    {
      entity_number = 22,
      name = "small-electric-pole",
      position = {
        x = -20,
        y = 46
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "small-electric-pole",
        type = "item"
      }
    }
  },
  name = "Bootstrap_power_connection"
}