return 
{
  anchor = {
    x = 0,
    y = -1
  },
  entities = {
    {
      entity_number = 2,
      name = "pipe",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      entity_number = 3,
      name = "pipe",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      entity_number = 4,
      name = "pipe",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      entity_number = 5,
      name = "pipe",
      position = {
        x = 1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 6,
      name = "offshore-pump",
      position = {
        x = 1,
        y = 1
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "pipe",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "offshore-pump",
        type = "item"
      }
    }
  },
  name = "Power-1-water-2"
}