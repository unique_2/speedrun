return 
{
  anchor = {
    x = -16,
    y = 19
  },
  entities = {
    {
      direction = 4,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = -1,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 0,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 1,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "transport-belt",
      position = {
        x = 2,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 3,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 4,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 5,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 6,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 7,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 8,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 9,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 10,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 11,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 12,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 13,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 14,
        y = -19
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 15,
        y = -19
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -1,
        y = -18
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -1,
        y = -17
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -17
      }
    },
    {
      entity_number = 21,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = -17
      },
      recipe = "underground-belt"
    },
    {
      direction = 6,
      entity_number = 22,
      name = "fast-inserter",
      position = {
        x = 4,
        y = -17
      }
    },
    {
      entity_number = 23,
      name = "iron-chest",
      position = {
        x = 5,
        y = -17
      }
    },
    {
      direction = 4,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = -1,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -1,
        y = -15
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -15
      }
    },
    {
      entity_number = 28,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = -15
      }
    },
    {
      entity_number = 29,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -16
      }
    },
    {
      direction = 4,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = -1,
        y = -14
      }
    },
    {
      direction = 4,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -1,
        y = -13
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -13
      }
    },
    {
      entity_number = 33,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = -13
      },
      recipe = "transport-belt"
    },
    {
      direction = 4,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -1,
        y = -12
      }
    },
    {
      direction = 4,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = -1,
        y = -11
      }
    },
    {
      entity_number = 36,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 37,
      name = "fast-inserter",
      position = {
        x = 2,
        y = -11
      }
    },
    {
      direction = 4,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = -1,
        y = -10
      }
    },
    {
      direction = 4,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = -1,
        y = -9
      }
    },
    {
      direction = 6,
      entity_number = 40,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -10
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -9
      }
    },
    {
      entity_number = 42,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = -9
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 4,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = -1,
        y = -8
      }
    },
    {
      direction = 4,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = -1,
        y = -7
      }
    },
    {
      entity_number = 45,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = -6
      },
      recipe = "underground-belt"
    },
    {
      direction = 4,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = -1,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -1,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 48,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -6
      }
    },
    {
      direction = 6,
      entity_number = 49,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 50,
      name = "fast-inserter",
      position = {
        x = 4,
        y = -6
      }
    },
    {
      entity_number = 51,
      name = "iron-chest",
      position = {
        x = 5,
        y = -6
      }
    },
    {
      direction = 4,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = -1,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "fast-inserter",
      position = {
        x = 1,
        y = -4
      }
    },
    {
      entity_number = 55,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = -2
      },
      recipe = "transport-belt"
    },
    {
      entity_number = 56,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = -4
      }
    },
    {
      direction = 4,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = -1,
        y = -1
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "fast-inserter",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      direction = 4,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = -1,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = -1,
        y = 1
      }
    },
    {
      entity_number = 62,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "fast-inserter",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      entity_number = 64,
      name = "assembling-machine-2",
      position = {
        x = 2,
        y = 2
      },
      recipe = "iron-gear-wheel"
    },
    {
      direction = 4,
      entity_number = 65,
      name = "fast-inserter",
      position = {
        x = 2,
        y = 0
      }
    },
    {
      direction = 4,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = -1,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "fast-inserter",
      position = {
        x = 0,
        y = 2
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-5"
}