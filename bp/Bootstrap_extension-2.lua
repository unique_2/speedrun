return 
{
  anchor = {
    x = -3,
    y = 1
  },
  entities = {
    {
      entity_number = 1,
      name = "small-electric-pole",
      position = {
        x = 5,
        y = -11
      }
    },
    {
      entity_number = 2,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -9
      },
      recipe = "electronic-circuit"
    },
    {
      entity_number = 3,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -7
      }
    },
    {
      entity_number = 4,
      name = "assembling-machine-2",
      position = {
        x = -3,
        y = -4
      },
      recipe = "electric-mining-drill"
    },
    {
      entity_number = 5,
      name = "assembling-machine-2",
      position = {
        x = 3,
        y = -5
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      entity_number = 7,
      name = "assembling-machine-2",
      position = {
        x = -3,
        y = -1
      },
      recipe = "electric-mining-drill"
    },
    {
      entity_number = 8,
      name = "small-electric-pole",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 0,
        y = -1
      }
    },
    {
      entity_number = 11,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = -2
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 0,
        y = 0
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 0,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "splitter",
      position = {
        x = 1,
        y = 1.5
      }
    },
    {
      direction = 4,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      direction = 4,
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 0,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 0,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 0,
        y = 5
      }
    },
    {
      entity_number = 20,
      name = "small-electric-pole",
      position = {
        x = 1,
        y = 5
      }
    },
    {
      direction = 4,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 0,
        y = 6
      }
    },
    {
      direction = 4,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 0,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 0,
        y = 8
      }
    },
    {
      direction = 6,
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      direction = 4,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 0,
        y = 10
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-2"
}