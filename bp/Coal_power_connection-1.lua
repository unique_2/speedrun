return {
  anchor = {
    x = -28,
    y = -4
  },
  entities = {
    {
      direction = 6,
      entity_number = 1,
      name = "underground-belt",
      position = {
        x = -30,
        y = -5
      },
      type = "output"
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = -31,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 4,
      name = "underground-belt",
      position = {
        x = -28,
        y = -5
      },
      type = "input"
    },
    {
      direction = 4,
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = -26,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = -26,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = -27,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = -25,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 9,
      name = "underground-belt",
      position = {
        x = -24,
        y = -4
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = -24,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = -25,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = -22,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = -23,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = -20,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = -21,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = -18,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 17,
      name = "underground-belt",
      position = {
        x = -19,
        y = -4
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = -18,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = -19,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = -17,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = -16,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = -16,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = -17,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 24,
      name = "underground-belt",
      position = {
        x = -14,
        y = -4
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = -15,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = -14,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 27,
      name = "transport-belt",
      position = {
        x = -15,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 28,
      name = "transport-belt",
      position = {
        x = -12,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 29,
      name = "transport-belt",
      position = {
        x = -13,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = -11,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = -10,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 32,
      name = "transport-belt",
      position = {
        x = -8,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 33,
      name = "underground-belt",
      position = {
        x = -9,
        y = -4
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = -8,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 35,
      name = "transport-belt",
      position = {
        x = -9,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 36,
      name = "transport-belt",
      position = {
        x = -7,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 37,
      name = "transport-belt",
      position = {
        x = -6,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 38,
      name = "transport-belt",
      position = {
        x = -7,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 39,
      name = "transport-belt",
      position = {
        x = -6,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 40,
      name = "transport-belt",
      position = {
        x = -5,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = -4,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = -5,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = -4,
        y = -5
      }
    },
    {
      direction = 6,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = -3,
        y = -5
      }
    },
    {
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = -3,
        y = -4
      }
    },
    {
      direction = 6,
      entity_number = 46,
      name = "underground-belt",
      position = {
        x = -28,
        y = -3
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 47,
      name = "transport-belt",
      position = {
        x = -26,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = -27,
        y = -3
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = -4,
        y = -3
      }
    },
    {
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = -4,
        y = -2
      }
    },
    {
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = -3,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = -3,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 53,
      name = "transport-belt",
      position = {
        x = -2,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 54,
      name = "transport-belt",
      position = {
        x = -2,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 55,
      name = "transport-belt",
      position = {
        x = -1,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 56,
      name = "transport-belt",
      position = {
        x = 0,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = -1,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 0,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 59,
      name = "transport-belt",
      position = {
        x = 1,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 60,
      name = "transport-belt",
      position = {
        x = 2,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 61,
      name = "transport-belt",
      position = {
        x = 1,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 2,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 63,
      name = "underground-belt",
      position = {
        x = 4,
        y = -3
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = 4,
        y = -2
      },
      type = "output"
    },
    {
      direction = 6,
      entity_number = 65,
      name = "transport-belt",
      position = {
        x = 3,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 66,
      name = "transport-belt",
      position = {
        x = 3,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 67,
      name = "underground-belt",
      position = {
        x = 9,
        y = -3
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 68,
      name = "underground-belt",
      position = {
        x = 9,
        y = -2
      },
      type = "input"
    },
    {
      direction = 6,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 10,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 70,
      name = "transport-belt",
      position = {
        x = 10,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 71,
      name = "transport-belt",
      position = {
        x = 11,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 72,
      name = "transport-belt",
      position = {
        x = 12,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 73,
      name = "transport-belt",
      position = {
        x = 11,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 74,
      name = "transport-belt",
      position = {
        x = 12,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 75,
      name = "transport-belt",
      position = {
        x = 14,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 76,
      name = "transport-belt",
      position = {
        x = 13,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 77,
      name = "transport-belt",
      position = {
        x = 13,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 78,
      name = "transport-belt",
      position = {
        x = 14,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 79,
      name = "transport-belt",
      position = {
        x = 16,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 80,
      name = "transport-belt",
      position = {
        x = 15,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 81,
      name = "transport-belt",
      position = {
        x = 15,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 82,
      name = "transport-belt",
      position = {
        x = 16,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 83,
      name = "transport-belt",
      position = {
        x = 18,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 84,
      name = "transport-belt",
      position = {
        x = 17,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 85,
      name = "transport-belt",
      position = {
        x = 17,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 86,
      name = "transport-belt",
      position = {
        x = 18,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 87,
      name = "transport-belt",
      position = {
        x = 20,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 88,
      name = "transport-belt",
      position = {
        x = 19,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 89,
      name = "transport-belt",
      position = {
        x = 19,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 90,
      name = "transport-belt",
      position = {
        x = 20,
        y = -2
      }
    },
    {
      entity_number = 91,
      name = "transport-belt",
      position = {
        x = 22,
        y = -2
      }
    },
    {
      direction = 6,
      entity_number = 92,
      name = "transport-belt",
      position = {
        x = 22,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 93,
      name = "transport-belt",
      position = {
        x = 21,
        y = -3
      }
    },
    {
      direction = 6,
      entity_number = 94,
      name = "transport-belt",
      position = {
        x = 21,
        y = -2
      }
    },
    {
      entity_number = 95,
      name = "transport-belt",
      position = {
        x = 22,
        y = -1
      }
    },
    {
      entity_number = 96,
      name = "transport-belt",
      position = {
        x = 22,
        y = 0
      }
    },
    {
      entity_number = 97,
      name = "transport-belt",
      position = {
        x = 21,
        y = -1
      }
    },
    {
      entity_number = 98,
      name = "transport-belt",
      position = {
        x = 21,
        y = 0
      }
    },
    {
      entity_number = 99,
      name = "splitter",
      position = {
        x = 21.5,
        y = 2
      }
    },
    {
      entity_number = 100,
      name = "transport-belt",
      position = {
        x = 22,
        y = 1
      }
    },
    {
      entity_number = 101,
      name = "transport-belt",
      position = {
        x = 21,
        y = 1
      }
    },
    {
      direction = 6,
      entity_number = 102,
      name = "splitter",
      position = {
        x = 25,
        y = 2.5
      }
    },
    {
      direction = 6,
      entity_number = 103,
      name = "transport-belt",
      position = {
        x = 26,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 104,
      name = "transport-belt",
      position = {
        x = 27,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 105,
      name = "transport-belt",
      position = {
        x = 28,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 106,
      name = "transport-belt",
      position = {
        x = 29,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 107,
      name = "transport-belt",
      position = {
        x = 30,
        y = 2
      }
    },
    {
      direction = 6,
      entity_number = 108,
      name = "transport-belt",
      position = {
        x = 31,
        y = 2
      }
    },
    {
      entity_number = 109,
      name = "transport-belt",
      position = {
        x = 22,
        y = 3
      }
    },
    {
      entity_number = 110,
      name = "transport-belt",
      position = {
        x = 21,
        y = 3
      }
    },
    {
      entity_number = 111,
      name = "transport-belt",
      position = {
        x = 21,
        y = 4
      }
    },
    {
      direction = 6,
      entity_number = 112,
      name = "transport-belt",
      position = {
        x = 23,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 113,
      name = "transport-belt",
      position = {
        x = 24,
        y = 3
      }
    },
    {
      direction = 6,
      entity_number = 114,
      name = "transport-belt",
      position = {
        x = 26,
        y = 3
      }
    },
    {
      entity_number = 115,
      name = "transport-belt",
      position = {
        x = 26,
        y = 4
      }
    },
    {
      entity_number = 116,
      name = "transport-belt",
      position = {
        x = 31,
        y = 3
      }
    },
    {
      entity_number = 117,
      name = "transport-belt",
      position = {
        x = 31,
        y = 4
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "transport-belt",
        type = "item"
      }
    }
  },
  name = "Coal_power_connection-1"
}