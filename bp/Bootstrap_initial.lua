return 
{
  anchor = {
    x = 0,
    y = -8
  },
  entities = {
    {
      entity_number = 2,
      name = "small-electric-pole",
      position = {
        x = -2,
        y = -1
      }
    },
    {
      entity_number = 3,
      name = "assembling-machine-2",
      position = {
        x = 0,
        y = 1
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 4,
      name = "assembling-machine-2",
      position = {
        x = 6,
        y = 2
      },
      recipe = "electronic-circuit"
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = 2,
        y = 2
      }
    },
    {
      entity_number = 6,
      name = "assembling-machine-2",
      position = {
        x = -7,
        y = 6
      },
      recipe = "inserter"
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = -7,
        y = 4
      }
    },
    {
      entity_number = 8,
      name = "small-electric-pole",
      position = {
        x = 6,
        y = 4
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_initial"
}