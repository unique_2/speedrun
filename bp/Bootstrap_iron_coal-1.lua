return 
{
  anchor = {
    x = 6,
    y = 17
  },
  entities = {
    {
      direction = 2,
      entity_number = 1,
      name = "transport-belt",
      position = {
        x = 5,
        y = -19
      }
    },
    {
      entity_number = 2,
      name = "transport-belt",
      position = {
        x = 5,
        y = -18
      }
    },
    {
      direction = 6,
      entity_number = 3,
      name = "transport-belt",
      position = {
        x = 6,
        y = -18
      }
    },
    {
      direction = 2,
      entity_number = 4,
      name = "underground-belt",
      position = {
        x = 6,
        y = -19
      },
      type = "input"
    },
    {
      entity_number = 5,
      name = "transport-belt",
      position = {
        x = 6,
        y = -16
      }
    },
    {
      entity_number = 6,
      name = "transport-belt",
      position = {
        x = 6,
        y = -17
      }
    },
    {
      entity_number = 7,
      name = "transport-belt",
      position = {
        x = 6,
        y = -14
      }
    },
    {
      entity_number = 8,
      name = "transport-belt",
      position = {
        x = 6,
        y = -15
      }
    },
    {
      entity_number = 9,
      name = "transport-belt",
      position = {
        x = 6,
        y = -12
      }
    },
    {
      entity_number = 10,
      name = "transport-belt",
      position = {
        x = 6,
        y = -13
      }
    },
    {
      entity_number = 11,
      name = "transport-belt",
      position = {
        x = 6,
        y = -10
      }
    },
    {
      entity_number = 12,
      name = "transport-belt",
      position = {
        x = 6,
        y = -11
      }
    },
    {
      entity_number = 13,
      name = "transport-belt",
      position = {
        x = 6,
        y = -8
      }
    },
    {
      entity_number = 14,
      name = "transport-belt",
      position = {
        x = 6,
        y = -9
      }
    },
    {
      entity_number = 15,
      name = "transport-belt",
      position = {
        x = 6,
        y = -6
      }
    },
    {
      entity_number = 16,
      name = "transport-belt",
      position = {
        x = 6,
        y = -7
      }
    },
    {
      entity_number = 17,
      name = "transport-belt",
      position = {
        x = 6,
        y = -4
      }
    },
    {
      entity_number = 18,
      name = "transport-belt",
      position = {
        x = 6,
        y = -5
      }
    },
    {
      entity_number = 19,
      name = "transport-belt",
      position = {
        x = 6,
        y = -2
      }
    },
    {
      entity_number = 20,
      name = "transport-belt",
      position = {
        x = 6,
        y = -3
      }
    },
    {
      entity_number = 21,
      name = "transport-belt",
      position = {
        x = 6,
        y = 0
      }
    },
    {
      entity_number = 22,
      name = "transport-belt",
      position = {
        x = 6,
        y = -1
      }
    },
    {
      entity_number = 23,
      name = "transport-belt",
      position = {
        x = 6,
        y = 2
      }
    },
    {
      entity_number = 24,
      name = "transport-belt",
      position = {
        x = 6,
        y = 1
      }
    },
    {
      entity_number = 25,
      name = "transport-belt",
      position = {
        x = 6,
        y = 4
      }
    },
    {
      entity_number = 26,
      name = "transport-belt",
      position = {
        x = 6,
        y = 3
      }
    },
    {
      direction = 4,
      entity_number = 27,
      name = "electric-mining-drill",
      position = {
        x = -5,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 28,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 7
      }
    },
    {
      direction = 4,
      entity_number = 29,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 7
      }
    },
    {
      entity_number = 30,
      name = "transport-belt",
      position = {
        x = 6,
        y = 6
      }
    },
    {
      entity_number = 31,
      name = "transport-belt",
      position = {
        x = 6,
        y = 5
      }
    },
    {
      entity_number = 32,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 8
      }
    },
    {
      entity_number = 33,
      name = "splitter",
      position = {
        x = 5.5,
        y = 8
      }
    },
    {
      entity_number = 34,
      name = "transport-belt",
      position = {
        x = 6,
        y = 7
      }
    },
    {
      entity_number = 35,
      name = "electric-mining-drill",
      position = {
        x = -5,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 36,
      name = "underground-belt",
      position = {
        x = -5,
        y = 9
      },
      type = "input"
    },
    {
      entity_number = 37,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 38,
      name = "underground-belt",
      position = {
        x = -2,
        y = 9
      },
      type = "output"
    },
    {
      entity_number = 39,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 9
      }
    },
    {
      entity_number = 40,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 11
      }
    },
    {
      direction = 2,
      entity_number = 41,
      name = "transport-belt",
      position = {
        x = 0,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 42,
      name = "transport-belt",
      position = {
        x = -1,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 43,
      name = "transport-belt",
      position = {
        x = 2,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 44,
      name = "transport-belt",
      position = {
        x = 1,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 45,
      name = "transport-belt",
      position = {
        x = 4,
        y = 9
      }
    },
    {
      direction = 2,
      entity_number = 46,
      name = "transport-belt",
      position = {
        x = 3,
        y = 9
      }
    },
    {
      entity_number = 47,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 11
      }
    },
    {
      entity_number = 48,
      name = "transport-belt",
      position = {
        x = 6,
        y = 9
      }
    },
    {
      entity_number = 49,
      name = "transport-belt",
      position = {
        x = 6,
        y = 10
      }
    },
    {
      entity_number = 50,
      name = "transport-belt",
      position = {
        x = 5,
        y = 9
      }
    },
    {
      entity_number = 51,
      name = "transport-belt",
      position = {
        x = 6,
        y = 11
      }
    },
    {
      entity_number = 52,
      name = "transport-belt",
      position = {
        x = 6,
        y = 12
      }
    },
    {
      direction = 4,
      entity_number = 53,
      name = "electric-mining-drill",
      position = {
        x = -5,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 54,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 55,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 14
      }
    },
    {
      direction = 4,
      entity_number = 56,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 14
      }
    },
    {
      entity_number = 57,
      name = "transport-belt",
      position = {
        x = 6,
        y = 13
      }
    },
    {
      entity_number = 58,
      name = "transport-belt",
      position = {
        x = 6,
        y = 14
      }
    },
    {
      direction = 2,
      entity_number = 59,
      name = "underground-belt",
      position = {
        x = -5,
        y = 16
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 60,
      name = "underground-belt",
      position = {
        x = -2,
        y = 16
      },
      type = "output"
    },
    {
      entity_number = 61,
      name = "small-electric-pole",
      position = {
        x = -3,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 62,
      name = "transport-belt",
      position = {
        x = 0,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 63,
      name = "transport-belt",
      position = {
        x = -1,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 64,
      name = "underground-belt",
      position = {
        x = 1,
        y = 16
      },
      type = "input"
    },
    {
      direction = 2,
      entity_number = 65,
      name = "underground-belt",
      position = {
        x = 4,
        y = 16
      },
      type = "output"
    },
    {
      entity_number = 66,
      name = "small-electric-pole",
      position = {
        x = 3,
        y = 16
      }
    },
    {
      entity_number = 67,
      name = "transport-belt",
      position = {
        x = 6,
        y = 15
      }
    },
    {
      entity_number = 68,
      name = "transport-belt",
      position = {
        x = 6,
        y = 16
      }
    },
    {
      direction = 2,
      entity_number = 69,
      name = "transport-belt",
      position = {
        x = 5,
        y = 16
      }
    },
    {
      entity_number = 70,
      name = "electric-mining-drill",
      position = {
        x = -5,
        y = 18
      }
    },
    {
      entity_number = 71,
      name = "electric-mining-drill",
      position = {
        x = -2,
        y = 18
      }
    },
    {
      entity_number = 72,
      name = "electric-mining-drill",
      position = {
        x = 1,
        y = 18
      }
    },
    {
      entity_number = 73,
      name = "electric-mining-drill",
      position = {
        x = 4,
        y = 18
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "electric-mining-drill",
        type = "item"
      }
    }
  },
  name = "Bootstrap_iron_coal-1"
}