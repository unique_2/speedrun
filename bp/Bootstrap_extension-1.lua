return 
{
  anchor = {
    x = -2,
    y = -8
  },
  entities = {
    {
      entity_number = 2,
      name = "assembling-machine-1",
      position = {
        x = -2,
        y = -5
      },
      recipe = "transport-belt"
    },
    {
      entity_number = 3,
      name = "assembling-machine-2",
      position = {
        x = -9,
        y = -2
      },
      recipe = "electric-mining-drill"
    },
    {
      entity_number = 4,
      name = "assembling-machine-1",
      position = {
        x = -2,
        y = -2
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 5,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -4
      }
    },
    {
      entity_number = 6,
      name = "small-electric-pole",
      position = {
        x = -4,
        y = -1
      }
    },
    {
      entity_number = 7,
      name = "small-electric-pole",
      position = {
        x = 8,
        y = -1
      }
    },
    {
      entity_number = 8,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 0
      }
    },
    {
      entity_number = 9,
      name = "assembling-machine-2",
      position = {
        x = -9,
        y = 2
      },
      recipe = "long-handed-inserter"
    },
    {
      entity_number = 10,
      name = "assembling-machine-2",
      position = {
        x = -2,
        y = 1
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 11,
      name = "assembling-machine-2",
      position = {
        x = 4,
        y = 2
      },
      recipe = "electronic-circuit"
    },
    {
      entity_number = 12,
      name = "assembling-machine-1",
      position = {
        x = 8,
        y = 1
      },
      recipe = "copper-cable"
    },
    {
      direction = 6,
      entity_number = 13,
      name = "fast-inserter",
      position = {
        x = -7,
        y = 2
      }
    },
    {
      entity_number = 14,
      name = "iron-chest",
      position = {
        x = -6,
        y = 2
      }
    },
    {
      entity_number = 15,
      name = "small-electric-pole",
      position = {
        x = 0,
        y = 2
      }
    },
    {
      entity_number = 16,
      name = "assembling-machine-1",
      position = {
        x = 8,
        y = 4
      },
      recipe = "copper-cable"
    },
    {
      entity_number = 17,
      name = "assembling-machine-2",
      position = {
        x = -9,
        y = 6
      },
      recipe = "inserter"
    },
    {
      entity_number = 18,
      name = "small-electric-pole",
      position = {
        x = -9,
        y = 4
      }
    },
    {
      direction = 4,
      entity_number = 19,
      name = "fast-inserter",
      position = {
        x = -8,
        y = 4
      }
    },
    {
      entity_number = 20,
      name = "assembling-machine-1",
      position = {
        x = 4,
        y = 6
      }
    },
    {
      entity_number = 21,
      name = "small-electric-pole",
      position = {
        x = 4,
        y = 4
      }
    },
    {
      entity_number = 22,
      name = "small-electric-pole",
      position = {
        x = 10,
        y = 5
      }
    },
    {
      entity_number = 23,
      name = "assembling-machine-2",
      position = {
        x = -2,
        y = 7
      },
      recipe = "transport-belt"
    },
    {
      entity_number = 24,
      name = "assembling-machine-1",
      position = {
        x = 8,
        y = 7
      },
      recipe = "iron-gear-wheel"
    },
    {
      entity_number = 25,
      name = "small-electric-pole",
      position = {
        x = -2,
        y = 9
      }
    }
  },
  icons = {
    {
      index = 1,
      signal = {
        name = "assembling-machine-1",
        type = "item"
      }
    },
    {
      index = 2,
      signal = {
        name = "assembling-machine-2",
        type = "item"
      }
    }
  },
  name = "Bootstrap_extension-1"
}