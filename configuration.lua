local config = {}

config.run_name = "BlueDarkas"
config.run_file = "blue_darkas_run"
config.autorun = true

return config
